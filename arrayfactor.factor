! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: annotations
arrayfactor.tests.parpi.test
arrayfactor.ui.console
arrayfactor.ui.console.instructions.private
arrayfactor.ui.console.private
kernel math.parser namespaces sequences vocabs ;
IN: arrayfactor

"arrayfactor.ui.chip.nodes" require

TUPLE: arrayfactor ;

SYMBOL: Arrayfactor

: <arrayfactor> ( -- arrayfactor )
    arrayfactor boa
    dup Arrayfactor set ;


: &table ( seq -- )
   !TODO console.private should not be available like this.
   check-for-current-node
   [ >hex source-push "," source-push ] each ;

! USE: arrayfactor
