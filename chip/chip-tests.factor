! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: kernel literals namespaces quotations
sequences tools.test vectors
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.port
arrayfactor.chips
arrayfactor.core.utils ;
IN: arrayfactor.chip.tests

<ga144> <chip> drop

[ 8 2 ]
[ l 0 9 correct-direction l ]
unit-test

[ { 0 37 74 37 } ]
[ { ${ u 0 } ${ d 0 } ${ u 18 } ${ d 18 } }
  [ dup first swap second 18 get-prt ] map
] unit-test

[ { 28 27 28 29 } ]
[ { ${ l 9 } ${ r 9 } ${ l 10 } ${ r 10 } }
  [ dup first swap second 18 get-prt ] map
] unit-test

[ { 21 39 20 2 } ]
[ ${ r d l u } [ 2 18 get-prt ] map ]
unit-test

[ t ]
[ 0 0 <port> r 2 set-port
  r 2 get-port port?
] unit-test

#! Generating portnumbers for all neighbours of nd 20.
[ { 373 469 277 325 } ]
[ 20 { 19 21 2 38 } [ over connect ] map nip ] unit-test

[ 1 3 connect ] must-fail

[   0 t f f
   17 t f f
    1 t f f
  127 t f f
]
[   1 nd>left over nd>left
   16 nd>right over nd>right
   19 nd>up over nd>up
  109 nd>down over nd>down
] unit-test

[ { 18 1 20 37 } ]
[ ${ r d l u } [ 19 swap nd-in-direction drop ] map ] unit-test

[ { 5 22 3 f } ]
[ ${ r d l u } [ 4 swap nd-in-direction drop ] map ] unit-test

[ { { 19 18 } } ]
[ 18 HEX: 1d5 port#>nd-pairs ] unit-test

[ 18 -dlu port#>nd-pairs ] must-fail

remove-chip

#! "arrayfactor.chip" test
