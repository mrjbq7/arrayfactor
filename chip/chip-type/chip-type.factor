#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors kernel math quotations ;
IN: arrayfactor.chip.chip-type

TUPLE: chip-type
     { rows integer }
     { cols integer }
     { compilation-constructor quotation }
     { node-class } ;
     
: get-rows ( chip-type -- n ) rows>> ;
: get-cols ( chip-type -- n ) cols>> ;
