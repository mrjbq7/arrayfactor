#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators
kernel locals math namespaces sequences words words.symbol parser
arrayfactor.chip
arrayfactor.chip.port
arrayfactor.core.utils ;
FROM: arrayfactor.core.utils => ?first ;
IN: arrayfactor.chip.connection

: CONNECTION-CODES ( -- seq )
{   "cn"         #! HEX: 00
    "cnp"        #! HEX: 01
    "cnp1"       #! HEX: 02
    "cnn"        #! HEX: 03
    "cnn1"       #! HEX: 04
    "north"      #! HEX: 05
    "south"      #! HEX: 06
    "east"       #! HEX: 07
    "west"       #! HEX: 08
} <enum> seq>> ;

: text>connection
    ( text -- connection-code/f ) CONNECTION-CODES index ;

TUPLE: connection
    { cnd integer }
    { pnd integer }
    { pnd1 integer }
    { nnd integer }
    { nnd1 integer }
    { tos integer } ;

SYMBOL: Connection

: <connection> ( node# -- connection )
    node#>nd       #! cnd
    -1             #! pnd
    -1             #! pnd1
    -1             #! nnd
    -1             #! nnd1
    -1             #! tos
    connection boa
    dup Connection set ;

: prvnd ( connection p -- )
    node#>nd >>pnd drop ;

: prvnd1 ( connection p -- )
    node#>nd >>pnd1 drop ;

: nxtnd ( connection n -- )
    node#>nd >>nnd drop ;

: nxtnd1 ( connection n -- )
    node#>nd >>nnd1 drop ;

: get-pnd ( connection -- pnd )
    pnd>> ;

: get-nnd ( connection -- pnd )
    nnd>> ;

ERROR: connection-port-error ;

: check-port ( port/f -- port )
    dup [ connection-port-error ] unless ;

: ,cn ( connection -- port )
    [ cnd>> ] [ tos>> ] bi node#>nd connect check-port ;

: ,cnp ( connection -- port )
    [ pnd>> ] [ cnd>> ] bi connect check-port ;

: ,cnp1 ( connection -- port )
    [ pnd1>> ] [ cnd>> ] bi connect check-port ;

: ,cnn ( connection -- port )
    [ nnd>> ] [ cnd>> ] bi connect check-port ;

: ,cnn1 ( connection -- port )
    [ nnd1>> ] [ cnd>> ] bi connect check-port ;

: ,north ( connection -- port )
    cnd>> dup nd>node# 100 + node#>nd connect check-port ;

: ,south ( connection -- port )
    cnd>> dup nd>node# 100 - node#>nd connect check-port ;

: ,east ( connection -- port )
    cnd>> dup nd>node# 1 + node#>nd connect check-port ;

: ,west ( connection -- port )
    cnd>> dup nd>node# 1 - node#>nd connect check-port ;

: CONNECT ( -- seq )
{
    [ ,cn    ]
    [ ,cnp   ]
    [ ,cnp1  ]
    [ ,cnn   ]
    [ ,cnn1  ]
    [ ,north ]
    [ ,south ]
    [ ,east  ]
    [ ,west  ]
} ; inline

#! FETCH

: connection@ ( -- connection )
    Connection get ;
