#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.core.utils assocs hashtables
kernel math ;
IN: arrayfactor.chip.debug-info

TUPLE: debug-info
    { hash hashtable } ;

: <debug-info> ( -- debug-info )
    H{ } clone   #! hash
    debug-info boa ;

: get-debug-info ( key debug-info -- value ? )
    hash>> at* ; inline

: set-debug-info ( value key debug-info -- )
    hash>> set-at ; inline

ERROR: inc-debug-info-error ;

: inc-debug-info ( key debug-info -- )
    2dup get-debug-info
    [ 1 + -rot* set-debug-info ]
    [ 3drop inc-debug-info-error ]
    if ; inline

ERROR: dec-debug-info-error ;

: dec-debug-info ( key debug-info -- )
    2dup get-debug-info
    [ 1 - -rot* set-debug-info ]
    [ 3drop inc-debug-info-error ]
    if ; inline

! USE: arrayfactor.chip.debug-info
