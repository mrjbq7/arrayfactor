! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays assocs fry grouping kernel locals
math.parser namespaces sequences sets splitting vectors ;
FROM: namespaces => set ;
IN: arrayfactor.chip.node.codes

: CODES ( -- seq )
{
    ";"        #! HEX: 00
    "ex"       #! HEX: 01
    "jmp"      #! HEX: 02
    "call"     #! HEX: 03
    "unext"    #! HEX: 04
    "next"     #! HEX: 05
    "if"       #! HEX: 06
    "-if"      #! HEX: 07
    "@p"       #! HEX: 08
    "@+"       #! HEX: 09
    "@b"       #! HEX: 0a
    "@"        #! HEX: 0b
    "!p"       #! HEX: 0c
    "!+"       #! HEX: 0d
    "!b"       #! HEX: 0e
    "!"        #! HEX: 0f
    "+*"       #! HEX: 10
    "2*"       #! HEX: 11
    "2/"       #! HEX: 12
    "-"        #! HEX: 13
    "+"        #! HEX: 14
    "and"      #! HEX: 15
    "or"       #! HEX: 16
    "drop"     #! HEX: 17
    "dup"      #! HEX: 18
    "pop"      #! HEX: 19
    "over"     #! HEX: 1a
    "a"        #! HEX: 1b
    "."        #! HEX: 1c
    "push"     #! HEX: 1d
    "b!"       #! HEX: 1e
    "a!"       #! HEX: 1f

    #! LITERALS

    ","        #! HEX: 20
    "lit"      #! HEX: 21

    #! MISC

    "org"      #! HEX: 22
    ":"        #! HEX: 23
    "]"        #! HEX: 24
    "["        #! HEX: 25

    #! CONTROL STRUCTURES

    "then"     #! HEX: 26
    "for"      #! HEX: 27
    "begin"    #! HEX: 28
    "while"    #! HEX: 29
    "-while"   #! HEX: 30
    "end"      #! HEX: 31
    "until"    #! HEX: 32
    "-until"   #! HEX: 33

    #! ADDITIONAL

    "<break>"   #! HEX: 34
    "nm>t"      #! HEX: 35
    "nm>s"      #! HEX: 36
    "nm>a"      #! HEX: 37
    "nm>b"      #! HEX: 38
    "nm>r"      #! HEX: 39

} <enum> seq>> ;

#! Ugly patch for -while added.
CONSTANT: ALIASES
H{
    { "xor"    "or" }
    { "@a"     "@"  }
    { "@a+"    "@+"  }
    { "!a"     "!"  }
    { "!a+"    "!+"  }
    { "a@"     "a"  }
    { "-while" "-if"  }
}

CONSTANT: CONTROL-WORDS
H{
    #! CONTROL STRUCTURES

    #! Delimiters

    { "then"   ""      }
    { "for"    ""      }
    { "begin"  ""      }

    #! Branching

    { "while"    "if"  }
    { "-while"   "-if" }
    { "end"   ""    } #! jmp
    { "until"    "if"  }
    { "-until"   "-if" }
}

#!
#! SOURCE>CODES
#!

: text>opcode ( text -- opcode/f ) CODES index ;

! : text>opcode ( text -- opcode )
!    dup CODES index
!    dup [ nip ] [ drop ALIASES at* drop ] if ;

ALIAS: op> text>opcode

: opcode>text ( opcode -- text ) CODES nth ; inline

ALIAS: txt> opcode>text

: translate-aliases ( text -- text' )
    dup ALIASES at* [ nip ] [ drop ] if ;

: source-translate-aliases ( vector -- vector' )
    [ translate-aliases ] map ;

: translate-control-words ( text -- text' )
    dup CONTROL-WORDS at* [ nip ] [ drop ] if ;

: opcode-filter ( seq -- seq' ) ; #! [ 0 >= ] filter ;

#! splits only on spaces, don't start or end with spaces!
: line>opcodes ( source-line -- seq )
    " " split [ text>opcode ] map opcode-filter ; inline

: source>opcodes ( source-lines-seq -- seq )
    [ line>opcodes ] map concat ; inline

: maybe-number ( x -- x' )
    dup string>number dup [ nip ] [ drop ] if ;

:: (source-vector>code) ( source-clumps jmp-seq -- seq )
    #! one of jmp-seq opcodes second in source-clumps with number as first is ok
    #!   not a number as first means first is ok, and second is also ok (put f before it)
    #! one of jmp-seq opcodes not second means first is ok
    V{ } clone :> seq
    source-clumps
    [ dup second jmp-seq in?
      [ dup first string>number dup
        [ swap second 2array >vector seq push ]
        [ drop
          [ first 1vector seq push ]
          [ f swap second 2array >vector seq push ]
          bi
        ]
        if
      ]
      [ first maybe-number 1vector seq push ]
      if
    ] each seq ;

SYMBOLS: The-Source The-Clumps The-Code ;

: source-vector>code ( vector -- array )
    dup The-Source set
    [ translate-aliases ] map >vector
    f over push
    2 clump
    dup The-Clumps set
    { "jmp" "call" "," "lit" "for" "org" }
    #! above opcodes not first in clump
    [  '[ first _ in? not ] filter ] keep
    (source-vector>code)
    dup The-Code set ;

: source>code ( string -- array ) " " split source-vector>code ;

: (source-without-names) ( seq' seq -- seq' seq )
    dup empty?
    [ dup pop dup "nm>t" =
      [ drop dup empty? [ dup pop drop ] unless ]
      [ pick push ]
      if (source-without-names)
    ] unless ; recursive

: source-without-names ( vector -- vector' )
    V{ } clone swap reverse (source-without-names) drop ; inline

: source-without-lit ( vector -- vector' )
    [ "lit" = not ] filter ; inline

