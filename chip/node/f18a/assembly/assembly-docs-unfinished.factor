! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: help.markup help.syntax calendar math quotations sequences ;
IN: arrayfactor.chip.node.f18a.assembly

HELP: assembly
{ $class-description "Assembly of the code in one node" } ;

HELP: resolve-addr
{ $values { "addr" integer } { "dest" integer } }
{ $description "Insert addr into branch sequence at dest in code." } ;


ARTICLE: "arrayfactor.chip.node.f18a.assembly" "arrayfactor.chip.node.f18a.assembly"
"The " { $vocab-link "arrayfactor.chip.node.f18a.assembly" } " assembles arrayforth code. "
"Related vocabularies: " { $vocab-link "arrayfactor.chip.node.f18a.assembly.private" } "."
{ $subsection assembly }
{ $subsection resolve-addr }
"---" ;

ABOUT: "arrayfactor.chip.node.f18a.assembly"

#! help-lint-all
