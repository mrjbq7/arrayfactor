


[ V{ "dup" "or" "!b" "." } V{ "!b" "@p" "!b" "dup" } ]
[ <start-memory> drop
  { "dup or !b" 
    "!b @p !b dup" }
  source>opcodes [ assembly@ >ram ] each
  assembly@ end-assembly
  ram@ code-trimmed>
  [ first ] [ second ] bi
]
unit-test

[ V{ V{ "dup" "or" "!b" "." } V{ "!b" "@p" "!b" "dup" } } ]
[ <start-memory> drop
  "dup or !b !b @p !b dup"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed>
]
unit-test

[ V{ V{ "drop" "." "." "." } } ]
[ <start-memory> drop
  "drop"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed>
]
unit-test

[ V{ V{ "over" V{ 5 "jmp" } } V{ "dup" V{ 6 "call" } } V{ 12 } V{ "dup" "." "." "." } } ]
[ <start-memory> drop
  "over 5 jmp dup 6 call 12 , dup"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed>
]
unit-test

[ V{ V{ "over" V{ 5 "jmp" } } V{ "dup" V{ 4 "call" } } V{ 12 } V{ "dup" "." "." "." } } ]
[ <start-memory> drop
  "over 5 jmp dup -1 call 12 , dup"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  4 1 assembly@ resolve-addr
  ram@ code-trimmed> ]
unit-test

[ V{
    V{ "dup" "@p" "push" "." }
    V{ 4 }
    V{ "dup" "drop" "over" "dup" }
    V{ "drop" "." "+" "." }
    V{ "+" "." V{ 2 "next" } }
    V{ 12 }
    V{ "dup" "." "." "." }
} ]
[ <start-memory> drop
  "dup 4 for dup drop over dup drop . + . + . next 12 , dup"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed> ]
unit-test

[ V{
    V{ "dup" "." "." "." }
    V{ -1 }
    V{ V{ 6 "if" } }
    V{ "dup" "drop" "over" "dup" }
    V{ "drop" "." "+" "." }
    V{ "+" "." "." "." }
    V{ 12 }
    V{ "dup" "." "." "." }
} ]
[ <start-memory> drop
  "dup -1 , if dup drop over dup drop . + . + then 12 , dup"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed> ]
unit-test

[ V{
    V{ "dup" "." "." "." }
    V{ "dup" "drop" "over" "." }
    V{ "!" "drop" "." "+" }
    V{ V{ 1 "-until" } }
} ]
[ <start-memory> drop
  "dup begin dup drop over ! drop . + -until"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed> ]
unit-test

[ V{
    V{ "dup" "." "." "." }
    V{ "dup" V{ 5 "while" } }
    V{ "drop" "over" "!" "." }
    V{ "drop" "." "+" "." }
    V{ V{ 1 "jmp" } }
} ]
[ <start-memory> drop
  "dup begin dup while drop over ! drop . + end"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed> ]
unit-test

[ V{
    V{ 0 }
    V{ 1 }
    V{ 2 }
    V{ 3 }
    V{ 4 }
    V{ 0 }
    V{ "a!" "." "." "." }
    V{ 5 }
    V{ "!+" "." "." "." }
    V{ 6 }
    V{ "!+" "." "." "." }
    V{ 7 }
    V{ "!+" "dup" "dup" "." }
    V{ "or" "!+" "." "." }
} ]
[ <start-memory> drop
  "0 , 1 , 2 , 3 , 4 , 0 , a! 5 , !+ 6 , !+ 7 , !+ dup dup or !+"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed> ]
unit-test

[ V{
    V{ "dup" V{ 5 "if" } }
    V{ 2 }
    V{ "." "+" "dup" "." }
    V{ 3 }
    V{ "2*" "." "." "." }
  }
  V{ 152325 2 180626 3 223666 79017 79017 79017 } ]
[ <start-memory> drop
  "dup if 2 , . + dup 3 , 2* then"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ code-trimmed>
  assembly@ get-ram
  dup clear-binary
  dup code>binary
  binary> 8 head ]
unit-test

[ H{ { "test4" 1 } { "double" 0 } }
  V{
    V{ "2*" ";" "." "." }
    V{ "@p" V{ 0 "call" } }
    V{ 4 }
    V{ ";" "." "." "." }
  } ]
[ <start-memory> drop
  "0 org : double 2* ; : test4 4 double ;"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  assembly@ get-ram get-names
  ram@ code-trimmed> ]
unit-test

#!
#! LIT
#!

[ V{ V{ HEX: 134a9 } V{ 0 } V{ "@p" "@p" "." "." } V{ 23 } V{ 24 } V{ 1 } } ]
[ <start-memory> drop
  "1 org 0 , 23 24 lit 1 ,"
  assembly@ assemble-source
  assembly@ end-assembly
  ram@ code-trimmed> ]
unit-test

#!
#! TIMED-CODE
#!

[ V{
    V{ f "@p" f V{ 3 "-if" } }
    V{ 3 }
    V{ "dup" "." "+" "." }
} ]
[ <start-memory> drop
  "3 lit -if dup . + then"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  ram@ get-timed-code
] unit-test
