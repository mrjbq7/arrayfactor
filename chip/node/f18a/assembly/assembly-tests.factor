! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrays assocs
hashtables kernel make math namespaces
quotations sequences splitting tools.test vectors
arrayfactor.chip arrayfactor.chip.chip-type
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.assembly.private
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.ram.private
arrayfactor.chip.node.f18a.memory.rom
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.f18a.assembly.tests

<ga144> <chip> drop

[ V{ "jmp" } ]
[ <start-memory> <assembly> drop
  "jmp" assembly@ instruction<push
  assembly@ instruction->
] unit-test

[ 4 5 5 6 ]
[ <start-memory> <assembly> drop
  4 assembly@ (org) assembly@ i-masked-> assembly@ here-masked->
  assembly@ next-i assembly@ i-masked-> assembly@ here-masked->
] unit-test

[ V{ 3 "jmp" } V{ V{ 169 "call" } } ]
[ <start-memory> <assembly> drop
  3 assembly@ instruction<push
  "jmp" op> txt> assembly@ instruction<push
  assembly@ instruction->
  dup assembly@ set-memory-instruction ram@ coding> first
]
unit-test

[ 10 ]
[ V{ 10 } code2binary ]
unit-test

[ V{ 24 2 } ]
[ V{ "dup" V{ 5 "jmp" } } get-opcodes ]
unit-test

[ 197120 ]
[ V{ 24 2 } shift-or ]
unit-test

[ HEX: 13400 ]
[ V{ V{ HEX: a9 "call" } } get-opcodes shift-or HEX: 15555 bitxor
  BIN: 111111110000000000 bitand ]
unit-test

[ HEX: 134a9 ]
[ HEX: a9 HEX: 13400 0 addr>ram ]
unit-test

[ HEX: 134a9 ]
[ V{ V{ HEX: a9 "call" } } HEX: 13400 maybe-include-addr ]
unit-test

[ HEX: 134a9 ]
[ V{ V{ HEX: a9 "call" } } code2binary ]
unit-test

[ HEX: 134a9 ]
[ V{ HEX: 134a9 } code2binary ]
unit-test

#!
#! NAMES
#!

[ H{ { "tst2" 3 } { "tst1" 0 } } ]
[ <start-memory> <assembly> drop
  0 "tst1" 3 "tst2" [ assembly@ get-ram set-name ] 2bi@
  assembly@ get-ram get-names ]
unit-test

[ 3 t ]
[ <start-memory> <assembly> drop
  0 "tst1" 3 "tst2" [ assembly@ get-ram set-name ] 2bi@
  "tst2" assembly@ get-ram name>addr ]
unit-test

[ "tst2" t ]
[ <start-memory> <assembly> drop
  0 "tst1" 3 "tst2" [ assembly@ get-ram set-name ] 2bi@
  3 assembly@ get-ram addr>name ]
unit-test

[ H{ { "tst3" 0 } } ]
[ <start-memory> <assembly> drop
  "0 org : tst3 2* ;" " " split
  assembly@ assemble-source
  assembly@ end-assembly
  assembly@ get-ram get-names ]
unit-test

[ V{
    V{
        f
        "@p"
        T{ integer-name { register "t" } { the-name "limit" } }
        "dup"
        f
        "@p"
    }
    V{ 10000 }
    V{ 999 }
    V{ f ";" }
} ]
[ <start-memory> <assembly> drop
  "0 org : tst4 10000 nm>t limit dup 999 ;" " " split
  assembly@ assemble-source
  assembly@ end-assembly
  assembly@ get-ram get-timed-code*
]
unit-test

#!
#! CHANGE A9 CODE
#!

[ V{ f V{ 7 "jmp" } } ]
[ <start-memory> <assembly> drop
  "169 org 7 jmp" " " split
  assembly@ assemble-source
  assembly@ end-assembly
  41 rom@ get-timed-code* nth
] unit-test

[ V{ V{ "dup" "or" "!b" "." } V{ "!b" "@p" "!b" "dup" } } ]
[ <start-memory> <assembly> drop
  "dup or !b !b @p !b dup"
  source>code [ assembly@ opcode>memory ] each
  assembly@ end-assembly
  assembly@ get-ram code-trimmed>
]
unit-test

remove-chip

#! "arrayfactor.chip.node.f18a.assembly" test
