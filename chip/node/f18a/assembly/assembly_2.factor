#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip
arrayfactor.chip.node.codes arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.debug
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.smart-integer arrayfactor.core.utils assocs
classes combinators grouping kernel locals math math.order
math.parser namespaces sequences strings vectors ;
IN: arrayfactor.chip.node.f18a.assembly

TUPLE: assembly
    { memory memory }
    { here integer }
    { i integer }
    { instruction vector }
    { unresolved vector }
    { no-next-semicolon-cnt integer }
    { remove-last-semicolon boolean }
    { expect-word boolean }
    { expect-name-for string }
    { annotation-count integer } ;

SYMBOL: Assembly

: <assembly> ( memory -- assembly )
                        #! memory
     1                  #! here
     0                  #! i
     4 <vector>         #! instruction
    10 <vector>         #! unresolved
     0                  #! no-next-semicolon-cnt
     t                  #! remove-last-semicolon
     f                  #! expect-word
     ""                 #! expect-name-for
     0                  #! annotation-count
    assembly boa
    dup Assembly set ;

: get-ram ( assembly -- ram )
    memory>> ram>> ; inline

: get-rom ( assembly -- rom )
    memory>> rom>> ; inline

: here-> ( assembly -- addr )
    here>> ;

: here-masked-> ( assembly -- addr )
    !TODO Check if this word is used in the right places.
    here-> mask-addr ;

: here<- ( addr assembly -- )
    here<< ; inline

: ->here ( assembly addr -- assembly )
    >>here ; inline

: here-inc ( assembly -- )
    [ addr-inc ] change-here drop ;

: i-> ( assembly -- addr )
    i>> ; inline

: i-masked-> ( assembly -- addr )
    i-> mask-addr ; inline

: i<- ( addr assembly -- )
    i<< ; inline

: ->i ( assembly addr -- assembly )
    >>i ; inline

: new-instruction ( assembly -- )
    4 <vector> >>instruction
    0 >>annotation-count drop ;

: i>name ( name assembly -- )
    [ i-> ] [ get-ram names> ] bi set-at ;

: ram|rom-set-name ( n name assembly -- ? )
    memory>>
    pick 64 < [ ram>> ] [ rom>> ] if
    [ mask-addr ] 2dip
    3dup check-name
    [ 3drop f ] [ set-names-model t ] if ;

#!
#! I
#!

: next-i ( assembly -- )
    dup [ here-> ] [ i-> addr-inc ] bi max ->i
    here-inc ;

: (org) ( addr assembly -- )
    [ ram|rom-addr ] dip
    [ i<- ] [ swap addr-inc ->here drop ] 2bi ;

: set-memory-instruction ( seq assembly -- )
   !TODO Shouldn't mask-addr be done first?
   [ i-> ] [ memory>> over swap memory-coding> ] bi
   [ mask-addr ] dip set-nth ;

: set-memory-here ( seq assembly -- )
   [ [ here-> ] [ memory>> over swap memory-coding> ] bi
   [ mask-addr ] dip set-nth ]
   [ here-inc ] bi ;

#! Insert addr into branch sequence
#!   into code at dest.
:: resolve-addr ( addr dest assembly -- ) 
    dest assembly memory>> memory-coding-item last
    dup pop over pop*
    addr pick push over push drop ;
  
: ->unresolved ( assembly -- )
    [ i-> ] [ unresolved>> ] bi push ;

ERROR: unresolved-stack-empty-error ;

: unresolved-> ( assembly -- addr )
    unresolved>> dup empty?
    [ drop unresolved-stack-empty-error ] [ pop ] if ;


#! 
#! INSTRUCTION
#!

: instruction-> ( assembly -- instruction )
    instruction>> ; inline

: instruction<push ( x assembly -- )
    instruction-> push ; inline

#!
#! SLOT
#!

: slot-> ( assembly -- n )
    instruction-> length ;

<PRIVATE

: fourth-slot-opcode? ( opcode -- ? )
    3 [ bitand ] [ bitxor ] [ = ] tri ;

#!
#! UTILS
#!

: branch-or-lit? ( seq -- ? )
    last [ vector? ] [ integer? ] [ { ";" "ex" } member? ] tri
    or or ; inline

#! Add extra nops if necessary.
:: fill-out ( assembly -- vector' )
    assembly instruction->
    dup branch-or-lit? [
        [ dup length assembly annotation-count>> 4 + < ]
        [ "." over push ] while
    ] unless ;

: next-instruction ( assembly -- )
   { [ fill-out ]
     [ set-memory-instruction ]
     [ next-i ]
     [ new-instruction ] } cleave ;
   
: highest-slot ( slot assembly -- )
    [ slot-> < ] keep
    [ next-instruction ] curry when ;

: check-branch-slot ( highest-slot addr assembly --
                      highest-slot' )
    i-> [ BIN: 11111000 bitand ] bi@ = [ 1 + ] when ;

#!
#! COMPILE INTO RAM
#!

CONSTANT: highest-branch-slot 1

:: >ram ( opcode assembly -- )
    assembly slot-> 3 =
    [ opcode fourth-slot-opcode? [ assembly next-instruction ]
    unless ] when
    opcode txt> assembly instruction<push
    3 assembly highest-slot ;
    
: 1>ram ( seq assembly -- )
    [ first op> ] dip >ram ;

: 2>ram ( seq assembly -- )
    highest-branch-slot swap
    [ highest-slot ]
    [ instruction<push ]
    [ next-instruction ] tri ;

: branch>ram ( seq assembly -- )
    highest-branch-slot swap
    { [ i-> ]
      [ check-branch-slot ] 
      [ highest-slot ]
      [ instruction<push ]
      [ next-instruction ]
    } cleave ;

: first>ram ( seq assembly -- )
    [ first ] dip 2>ram ;

#!
#! EXTRA INFO TO RAM
#!

:: integer-name>ram ( name assembly -- )
    assembly expect-name-for>>
    dup { "t" "s" "a" "b" "r" } member?
    [ name <integer-name>
      assembly
      [ 1 + ] change-annotation-count
      "" >>expect-name-for
      instruction<push
    ] [ drop ] if ;

: break>ram ( seq assembly -- )
    [ drop <break> ] dip instruction<push ;

#!
#! CODES TO RAM
#!

#! Pop addr from return-stack, and jump to it.
:: ;>ram ( seq assembly -- )
    assembly no-next-semicolon-cnt>> 0 >
    [ 0 assembly no-next-semicolon-cnt<< ]
    [ seq first assembly instruction<push
      assembly next-instruction
    ] if ;

#! Exchange p and ram, increment new p.
:: ,>ram ( seq assembly -- )
    seq first 
    0 assembly highest-slot
    assembly instruction<push
    assembly next-instruction ;

: lit>ram ( seq assembly -- )
    [ 1 head ] dip
    "@p" over instruction<push
    dup instruction-> length 4 =
    [ { [ instruction-> ]
        [ set-memory-instruction ]
        [ new-instruction ]
        [ set-memory-here ]
        [ next-i ] } cleave ]
    [ set-memory-here ] if ;

:: lit>ram? ( seq assembly -- ? )
    seq last number? dup
    [ seq assembly lit>ram ] when ;

:: unext>ram ( seq assembly -- )
    "unext" "Unresolved-Word" dbg-set
    assembly unresolved-> drop seq assembly 1>ram
    "" "Unresolved-Word" dbg-set ;

:: next>ram ( seq assembly -- )
    highest-branch-slot assembly highest-slot
    "next" "Unresolved-Word" dbg-set
    assembly unresolved-> 1vector seq append
    assembly instruction<push
    "" "Unresolved-Word" dbg-set
    assembly next-instruction ;

:: _if>ram ( seq assembly -- )
    highest-branch-slot assembly highest-slot
    f seq first 2vector assembly instruction<push
    assembly ->unresolved
    assembly next-instruction ;

: while>ram ( seq assembly -- ) nip
  { [ highest-branch-slot swap highest-slot ]
    [ f "while" 2vector swap instruction<push ]
    [ ->unresolved ]
    [ next-instruction ] } cleave ;

:: -while>ram ( seq assembly -- )
    highest-branch-slot assembly highest-slot
    f "-if" 2vector assembly instruction<push
!   f "-while" 2vector assembly instruction<push
    assembly ->unresolved
    assembly next-instruction ;

: for>ram ( seq assembly -- )
    over first [ 2dup lit>ram ] when
    "push" op> over >ram
    nip 0 over highest-slot
    ->unresolved ;

: begin>ram ( seq assembly -- ) nip
    0 over highest-slot
    ->unresolved ;

: resolve ( seq assembly -- ) nip
    { [ 0 swap highest-slot ]
      [ i-> ] [ unresolved-> ] [ resolve-addr ]
    } cleave ; inline

: then ( seq assembly -- )
    "then" "Unresolved-Word" dbg-set
    resolve
    "" "Unresolved-Word" dbg-set ;

:: until>ram ( seq assembly -- )
    highest-branch-slot assembly highest-slot
    "until" "Unresolved-Word" dbg-set
    assembly unresolved-> "until" 2vector assembly instruction<push
    "" "Unresolved-Word" dbg-set
    assembly next-instruction ;

:: -until>ram ( seq assembly -- )
    highest-branch-slot assembly highest-slot
    "-until" "Unresolved-Word" dbg-set
    assembly unresolved-> "-until" 2vector assembly instruction<push
    "" "Unresolved-Word" dbg-set
    assembly next-instruction ;
    
:: end>ram ( seq assembly -- )
    assembly unresolved-> 
    "end" "Unresolved-Word" dbg-set
    highest-branch-slot
    assembly unresolved-> 
    [ assembly [ check-branch-slot ] [ highest-slot ] bi ]
    [ "jmp" 2vector ] bi
    assembly instruction<push
    "" "Unresolved-Word" dbg-set
    assembly next-instruction
    0 assembly highest-slot
    assembly i-> swap assembly resolve-addr ;

#!
#! MISC
#!

: org-when-not-first ( addr assembly -- )
  { [ fill-out ]
    [ new-instruction ]
    [ set-memory-instruction ]
    [ (org) ]
    [ new-instruction ]
  } cleave ; inline

: handle-org? ( seq assembly -- ? )
    over last "org" =
    [ [ first ] dip
      dup instruction-> empty?
      [ (org) ] [ org-when-not-first ] if
      t
    ] [ 2drop f ] if ;

: :>ram ( seq assembly -- )
    t >>expect-word 2drop ;

#! ADDITIONAL

: break ( seq assembly -- )
    2drop ;

: nm>t ( seq assembly -- )
    "t" >>expect-name-for 2drop ;

: nm>s ( seq assembly -- )
    "s" >>expect-name-for 2drop ;

: nm>a ( seq assembly -- )
    "a" >>expect-name-for 2drop ;

: nm>b ( seq assembly -- )
    "b" >>expect-name-for 2drop ;

: nm>r ( seq assembly -- )
    "r" >>expect-name-for 2drop ;

: ASSEMBLY ( -- seq )
{
    [ ( seq assembly -- ) ;>ram ]         #! ";"
    [ ( seq assembly -- ) first>ram ]     #! "ex"
    [ ( seq assembly -- ) 2>ram ]         #! V{ addr "jmp" }
    [ ( seq assembly -- ) 2>ram ]         #! V{ addr "call" }
    [ ( seq assembly -- ) unext>ram ]     #! "unext" was: 1>ram
    [ ( seq assembly -- ) next>ram ]      #! V{ addr "next" }
    [ ( seq assembly -- ) _if>ram ]       #! V{ addr "if" }  
    [ ( seq assembly -- ) _if>ram ]       #! V{ addr "-if" }
    [ ( seq assembly -- ) 1>ram ]         #! "@p"   
    [ ( seq assembly -- ) 1>ram ]         #! "@+"   
    [ ( seq assembly -- ) 1>ram ]         #! "@b"   
    [ ( seq assembly -- ) 1>ram ]         #! "@"    
    [ ( seq assembly -- ) 1>ram ]         #! "!p"   
    [ ( seq assembly -- ) 1>ram ]         #! "!+"   
    [ ( seq assembly -- ) 1>ram ]         #! "!b"   
    [ ( seq assembly -- ) 1>ram ]         #! "!"    
    [ ( seq assembly -- ) 1>ram ]         #! "+*"   
    [ ( seq assembly -- ) 1>ram ]         #! "2*"   
    [ ( seq assembly -- ) 1>ram ]         #! "2/"   
    [ ( seq assembly -- ) 1>ram ]         #! "-"    
    [ ( seq assembly -- ) 1>ram ]         #! "+"    
    [ ( seq assembly -- ) 1>ram ]         #! "and"  
    [ ( seq assembly -- ) 1>ram ]         #! "or"   
    [ ( seq assembly -- ) 1>ram ]         #! "drop" 
    [ ( seq assembly -- ) 1>ram ]         #! "dup"  
    [ ( seq assembly -- ) 1>ram ]         #! "pop"  
    [ ( seq assembly -- ) 1>ram ]         #! "over"
    [ ( seq assembly -- ) 1>ram ]         #! "a"    
    [ ( seq assembly -- ) 1>ram ]         #! "."    
    [ ( seq assembly -- ) 1>ram ]         #! "push" 
    [ ( seq assembly -- ) 1>ram ]         #! "b!"   
    [ ( seq assembly -- ) 1>ram ]         #! "a!"
    
    #! LITERALS
    
    [ ( seq assembly -- ) ,>ram ]         #! ","   
    [ ( seq assembly -- ) lit>ram ]       #! "lit"

    #! MISC

    [ ( seq assembly -- ) 2drop ]         #! "org"   
    [ ( seq assembly -- ) :>ram ]         #! ":"
    [ ( seq assembly -- ) 2drop ]         #! "]"   
    [ ( seq assembly -- ) 2drop ]         #! "["
    
    #! CONTROL STRUCTURES

    [ ( seq assembly -- ) then ]          #! "then"   
    [ ( seq assembly -- ) for>ram ]       #! "for"    
    [ ( seq assembly -- ) begin>ram ]     #! "begin"  
    [ ( seq assembly -- ) while>ram ]     #! "while"  
    [ ( seq assembly -- ) -while>ram ]    #! "-while" 
    [ ( seq assembly -- ) end>ram ]       #! "end" 
    [ ( seq assembly -- ) until>ram ]     #! "until"  
    [ ( seq assembly -- ) -until>ram ]    #! "-until"
    
    #! PRO

    [ ( seq assembly -- ) break>ram ]     #! "<break>"        
    [ ( seq assembly -- ) nm>t ]          #! "nm>t"   
    [ ( seq assembly -- ) nm>s ]          #! "nm>s"   
    [ ( seq assembly -- ) nm>a ]          #! "nm>a"   
    [ ( seq assembly -- ) nm>b ]          #! "nm>b"   
    [ ( seq assembly -- ) nm>r ]          #! "nm>r"   

} ; inline

PRIVATE>

ERROR: invalid-wordname-error ;

: second-is-semicolon? ( seq/f -- ? )
    dup vector? [ second first ";" = ] [ drop f ] if ;

: ram|rom-name>addr ( maybe-name memory -- addr/f ? )
    2dup ram>> name>addr
    [ [ 2drop ] dip t ]
    [ drop rom>> name>addr [ 64 + t ] [ f ] if ] if ;

:: word-lookup ( maybe-name assembly _clump -- )
    maybe-name assembly memory>> ram|rom-name>addr
    [ _clump second-is-semicolon?
    [ 2 assembly no-next-semicolon-cnt<< "jmp" ] [ "call" ] if
      2vector assembly branch>ram ]
    [ drop maybe-name "Invalid-Wordname" dbg-set
      invalid-wordname-error ] if ;

ERROR: no-assembly-error1 ;
ERROR: no-assembly-error2 ;

: (maybe-create-word) ( word assembly -- ? )
    dup assembly instance? [ no-assembly-error1 ] unless
    [ i-> swap ]
    [ ram|rom-set-name ]
    [ f >>expect-word drop ] tri ; inline

: maybe-create-word ( word assembly -- ? )
    dup assembly instance? [ no-assembly-error2 ] unless
    dup expect-word>>
    [ (maybe-create-word) ] [ 2drop f ] if ;

: maybe-create-name ( name assembly -- ? )
    dup expect-name-for>> "" = not
    [ [ integer-name>ram ] [ 2drop ] if ] keep ;

: after-assembly ( assembly -- )
    [ 1 - ] change-no-next-semicolon-cnt drop ;

:: (opcode>memory) ( seq _clump assembly -- )
    seq assembly handle-org?
    [ seq assembly lit>ram?
      [ seq last assembly
        [ maybe-create-word ] [ maybe-create-name ] 2bi or
        [ seq last op> dup
          [ seq assembly rot* ASSEMBLY nth
            call( -seq- -assembly- -- )
            assembly after-assembly ]
          [ drop seq last assembly 
            _clump word-lookup
          ] if
        ] unless
      ] unless
    ] unless ;

: opcode>memory ( seq assembly -- )
    [ dup 2 clump { f } append ] dip (opcode>memory) ;

! : on-stack ( x y -- x y ) ;
! \ on-stack DEBUG add-input-logging

: no-last-semicolon? ( code c assembly -- code c ? )
    !TODO Remove last semicolon?
    over [ drop f ] [ remove-last-semicolon>>
    [ over first ";" = ] [ f ] if ] if ;

: inc-assemble-source-index ( -- )
    "Assemble-Source-Index" dbg-inc ;

:: end-assembly ( assembly -- )
    assembly slot-> 0 > [ assembly next-instruction ] when
    assembly memory>>
    [ ram>> ] [ rom>> ] bi 
    { [ [ names>addr-list ] bi@ ]
      [ [ coding>code ] bi@ ]
      [ [ code>binary ] bi@ ]
      [ [ code>timed-code ] bi@ ]
    } 2cleave ;

:: replace-a9-code ( addr assembly -- )
    "169 org " addr number>string append " jmp" append
    source>code [ assembly opcode>memory ] each
    assembly end-assembly ;

:: assemble-source ( source-vector assembly -- )
     0 "Assemble-Source-Index" dbg-set
     source-vector source-vector>code
     dup 2 clump { f } append
     dup "Assemble-Source-Clump" dbg-set
     [ inc-assemble-source-index assembly (opcode>memory) ]
     2each ;
     
#! Just get rid of the last semicolon in the source vector! 
#!    [ inc-assemble-source-index assembly no-last-semicolon?
#!  [ 2drop ] [ assembly (opcode>memory) ] if ] 2each ;

: assemble ( source-text assembly -- )
     [ assemble-source ] keep
     end-assembly ;

#! FETCH

: assembly@ ( -- assembly )
    Assembly get ;
