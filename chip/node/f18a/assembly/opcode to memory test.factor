<ga144> <chip> 
6 {node}

: seq ( -- seq ) "dup or !b !b @p !b dup" source>code ;

: _clump ( -- _clump ) seq 2 clump { f } append ;

: assembly ( -- assembly ) this-assembly ;

:: (opcode>memory) ( seq _clump assembly -- )
    seq assembly handle-org?
    [ seq assembly lit>ram?
      [ seq last assembly
        [ maybe-create-word ] [ maybe-create-name ] 2bi or
        [ seq last op> dup
          [ seq assembly rot* ASSEMBLY nth
            call( seq assembly -- )
            assembly after-assembly ]
          [ drop seq last assembly 
            _clump word-lookup
          ] if
        ] unless
      ] unless
    ] unless ;


6 {node}
  "dup or !b !b @p !b dup" source>code 
  [
    this-assembly opcode>memory
  ] each
  this-assembly end-assembly
  this-ram code-trimmed>


accessors annotations arrays assocs combinators
effects.parser eval fry generalizations grouping
hashtables io kernel locals logging macros make math
math.bitwise math.order math.parser namespaces
regexp sequences splitting strings words words.symbol
parser prettyprint shuffle unicode.normalize vectors