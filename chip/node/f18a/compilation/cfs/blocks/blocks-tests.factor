! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays assocs byte-arrays generalizations hashtables
kernel literals math math.parser namespaces pack quotations 
sequences strings tools.test vectors
arrayfactor.chip.node.f18a.compilation.cfs.blocks
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.f18a.compilation.cfs.blocks.tests

[ 5 4 ]
[ "a" get-cf-char
  [ first ] [ second ] bi
]
unit-test

[ "50000004" 4 ]
[ 4 "a" init-char-packed
  [ >hex ] dip
]
unit-test

[ "c6000004" 7 ]
[ 4 "b" init-char-packed
  [ >hex ] dip
]
unit-test

[ "5c600004" "1011100011000000000000000000100" 11 ]
[ 4 "a" init-char-packed
  "b" -rot* next-char-packed
  [ [ >hex ] [ >bin ] bi ] dip
]
unit-test

[ "5c720004" "1011100011100100000000000000100" 16 ]
[ 4 "a" init-char-packed
  "b" -rot* next-char-packed
  "c" -rot* next-char-packed
  [ [ >hex ] [ >bin ] bi ] dip
]
unit-test

[ "5c72c004" "1011100011100101100000000000100" 23 ]
[ 4 "a" init-char-packed
  "b" -rot* next-char-packed
  "c" -rot* next-char-packed
  "d" -rot* next-char-packed
  [ [ >hex ] [ >bin ] bi ] dip
]
unit-test

[ "5c72c084" "1011100011100101100000010000100" 27 ]
[ 4 "a" init-char-packed
  "b" -rot* next-char-packed
  "c" -rot* next-char-packed
  "d" -rot* next-char-packed
  "e" -rot* next-char-packed
  [ [ >hex ] [ >bin ] bi ] dip
]
unit-test

[ "5c72c084" "1011100011100101100000010000100" f ]
[ 4 "a" init-char-packed
  "b" -rot* next-char-packed
  "c" -rot* next-char-packed
  "d" -rot* next-char-packed
  "e" -rot* next-char-packed
  "f" -rot* next-char-packed
  [ [ >hex ] [ >bin ] bi ] dip
]
unit-test

[ "5c72c004" "1011100011100101100000000000100" f ]
[ 4 "a" init-char-packed
  "b" -rot* next-char-packed
  "c" -rot* next-char-packed
  "d" -rot* next-char-packed
  "f" -rot* next-char-packed
  [ [ >hex ] [ >bin ] bi ] dip
]
unit-test

[ V{ } { "5c72c084" "1011100011100101100000010000100" } f ]
[ "abcde" >vector
  [ 4 swap first 1string init-char-packed ]
  [ rest reverse -rot* ] bi
  pack-cf-word-part
  [ [ >hex ] [ >bin ] bi 2array ] dip
]
unit-test

[ V{ 102 } { "5c72c004" "1011100011100101100000000000100" } f ]
[ "abcdf" >vector
  [ 4 swap first 1string init-char-packed ]
  [ rest reverse -rot* ] bi
  pack-cf-word-part
  [ [ >hex ] [ >bin ] bi 2array ] dip
]
unit-test

[ V{ 1551024132 2952790016 } V{ } 2952790016 f
  { "5c72c004" "1011100011100101100000000000100" }
  { "b0000000" "10110000000000000000000000000000" }
]
[ V{ } clone
  "abcdf" >vector
  [ 4 swap first 1string init-char-packed ]
  [ rest reverse -rot* ] bi
  pack-cf-word-parts
  4 npick [ first ] [ second ] bi
  [ [ >hex ] [ >bin ] bi 2array ] bi@
]
unit-test

[ V{ 1551024132 2952790016 } ]
[ "abcdf" 4 pack-cf-word
]
unit-test

[ "f0000004" ]
[ ";" 4 pack-cf-word first >hex
]
unit-test

[ V{ B{ 4 192 114 92 } B{ 0 0 0 176 } } ]
[ V{ 1551024132 2952790016 } [ s32>byte-array ] map ]
unit-test

[ { 3208 } { "c88" } B{ 136 12 0 0 } ]
[ HEX: 64 0 $ yellow-number pack-cf-number
  dup [ >hex ] map
  over [ s32>byte-array ] map concat
]
unit-test

[ { 18 1073741823 } { "12" "3fffffff" } B{ 18 0 0 0 255 255 255 63 } ]
[ HEX: 3fffffff 1 $ yellow-number pack-cf-number
  dup [ >hex ] map
  over [ s32>byte-array ] map concat
]
unit-test

#! The next test is interesting, but colorforth is 32 bits.
#! So the first 32-bits word contains zero in the first 27 bits,
#! if the second word is used.
[ { 18 4294965948 }
  { "12" "fffffabc" }
  { "10010" "11111111111111111111101010111100" }
  B{ 18 0 0 0 188 250 255 255 }
]
[ HEX: 7fffffffabc 1 $ yellow-number pack-cf-number
  dup [ >hex ] map
  over [ >bin ] map
  pick [ s32>byte-array ] map concat
]
unit-test

[ { 18 2147483647 }
  { "12" "7fffffff" }
  { "10010" "1111111111111111111111111111111" }
  B{ 18 0 0 0 255 255 255 127 }
]
[ HEX: 7fffffff 1 $ yellow-number pack-cf-number
  dup [ >hex ] map
  over [ >bin ] map
  pick [ s32>byte-array ] map concat
]
unit-test

[ V{
    { "0" 8 }
    { "org" 1 }
    { "-swap" 3 }
    { "-" 4 }
    { "." 4 }
    { "+" 4 }
    { "-" 4 }
    { ";" 4 }
    { "gcnt" 3 }
    { "begin" 4 }
    { "@" 4 }
    { "while" 4 }
    { "drop" 4 }
    { "1" 6 }
    { "." 4 }
    { "+" 4 }
    { "end" 4 }
    { "drop" 4 }
    { ";" 4 }
    { "sv" 3 }
    { "over" 4 }
    { "over" 4 }
    { "or" 4 }
    { "if" 4 }
    { "drop" 4 }
    { "!b" 4 }
    { ";" 4 }
} ]
[ V{ "0" "org" ":" "-swap" "-" "." "+" "-" ";" ":" "gcnt" "begin" "@" "while" "drop" 
     "1" "." "+" "end" "drop" ";" ":" "sv" "over" "over" "or" "if" "drop" "!b" ";" }
  >colored-source
] unit-test

[ "ffffff00" 24 "i" ]
[ HEX: 7ffffff0 28 4bits-code-start
  [ >hex ] 2dip 1string
]
unit-test

[ "fffffe00" 23 "w" ]
[ HEX: bffffff0 28 5bits-code-start
  [ >hex ] 2dip 1string
]
unit-test

[ "fffff800" 21 "q" ]
[ HEX: cffffff0 28 7bits-code-start
  [ >hex ] 2dip 1string
]
unit-test

[ "fffff800" 21 "/" ]
[ HEX: effffff0 28 7bits-code-start
  [ >hex ] 2dip 1string
]
unit-test

[ "fffff800" 21 "?" ]
[ HEX: fffffff0 28 ((unpack-word))
  [ >hex ] 2dip 1string
]
unit-test

[ V{ "a" "b" "c" "d" } ]
[ HEX: 5c72c000 unpack-word [ 1string ] map
]
unit-test

[ { 8 0 100 } ]
[ HEX: c88 unpack-number-test
]
unit-test

[ { "6" "0" "7800000" } "7800000" ]
[ HEX: f0000006
  [ unpack-number-test [ >hex ] map ]
  [ -5 shift >hex ] bi
]
unit-test

[ 6 { "0" "7800000" } "7800000" ]
[ HEX: f0000006
  [ unpack-32bits [ >hex ] map ]
  [ -5 shift >hex ] bi
]
unit-test

[ 4 V{ "a" "b" "c" "d" } ]
[ HEX: 5c72c004
  unpack-32bits [ 1string ] map
]
unit-test

[ 5 f 5 { "7fffffff" } -1 ]
[ -1 Prev-Tag set
  HEX: 00000005 unpack-32bits
  HEX: 7fffffff unpack-32bits [ >hex ] map
  Prev-Tag get
]
unit-test

[ V{ "a" "b" "c" } V{ "a" "b" "c" } -1 ]
[ -1 Prev-Tag set
  HEX: 5c720004 unpack-word [ 1string ] map
  HEX: 5c72000 unpack-word [ 1string ] map
  Prev-Tag get
]
unit-test

#! "arrayfactor.chip.node.f18a.compilation.cfs.blocks" test
