! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip.node
arrayfactor.chip.node.codes arrayfactor.chips
arrayfactor.core.preferences arrayfactor.core.utils arrays
assocs combinators generalizations grouping io.binary
io.encodings.binary io.files io.pathnames kernel literals
locals math math.bitwise math.parser namespaces pack sequences
strings vectors ;
IN: arrayfactor.chip.node.f18a.compilation.cfs.blocks

#!
#! Reading blocks
#!

CONSTANT: Cf-Chars-To-Read
    H{
      {  0 " rtoeani" }
      {  2 "smcylgfw" }
      { 12 "dvpbhxuq" }
      { 13 "01234567" }
      { 14 "89j-k.z/" }
      { 15 ";'!+@*,?" }
    }

SYMBOL: Prev-Tag

: 4bits-code-start ( n length -- n' length' char/f )
    dup 4 >=
    [ over -31 shift 0 =
      [ 4 -
        over -28 shift 7 bitand
        0 Cf-Chars-To-Read at* drop nth
        [ HEX: 0fffffff bitand 4 shift ] 2dip
      ] [ f ] if
    ] [ f ] if ;

: 5bits-code-start ( n length -- n' length' char/f )
    dup 5 >=
    [ over -30 shift 2 =
      [ 5 -
        over -27 shift 7 bitand
        2 Cf-Chars-To-Read at* drop nth
        [ HEX: 07ffffff bitand 5 shift ] 2dip
      ] [ f ] if
    ] [ f ] if ;

:: (7bits-code-start) ( n length code-start -- n' length' char )
      n length 7 -
      over -25 shift 7 bitand
      code-start Cf-Chars-To-Read at* drop nth
      [ HEX: 01ffffff bitand 7 shift ] 2dip ;

: 7bits-code-start ( n length -- n' length' char/f )
    dup 7 >=
    [ over -28 shift 15 bitand
    dup { 12 13 14 15 } member?
    [ (7bits-code-start) ] [ drop f ] if ]
    [ f ] if ;

: ((unpack-word)) ( n length -- n' length' char/f )
    4bits-code-start dup
    [ drop 5bits-code-start dup
      [ drop 7bits-code-start ] unless
    ] unless ;

: (unpack-word) ( vector n length -- vector' n' length' )
     ((unpack-word)) dup
     [ 4 npick push (unpack-word) ] [ drop ] if
; recursive

#! For text only 3 bits are used for the tag!
: unpack-word ( n -- seq )
    HEX: fffffff8 bitand
    V{ } clone swap 29 (unpack-word)
    2drop [ 32 = not ] filter ;

: unpack-number ( n -- hex number )
   -4 shift
    [ 1 bitand ]
    [ -1 shift ] bi ;

: unpack-number-test ( n -- seq )
    dup HEX: f bitand swap
    unpack-number
    3array ;

! ERROR: unpack-32bits-error ;

#! TAGS
CONSTANT: yellow-word 1
CONSTANT: yellow-big-number 2
CONSTANT: red-word 3
CONSTANT: green-word 4
CONSTANT: green-big-number 5
CONSTANT: green-number 6
CONSTANT: yellow-number 8
CONSTANT: white-word 9
CONSTANT: white-number 15

: (unpack-32bits) ( n tag -- tag seq/f )
    swap over
    { { $ yellow-word [ unpack-word ] }
      { $ red-word [ unpack-word ] }
      { $ green-word [ unpack-word ] }
      { $ green-number [ unpack-number 2array ] }
      { $ yellow-number [ unpack-number 2array ] }
      { $ white-word [ unpack-word ] }
      { $ white-number [ unpack-number 2array ] }
      [ 2drop f ]
    } case ;

: big-number-tag? ( prev-tag -- ? )
    ${ yellow-big-number green-big-number } member? ; inline

: use-right-tag ( prev-tag tag -- right-tag )
      dup 0 = [ drop ] [ nip  ] if ;

: unpack-big-number ( n prev-tag -- prev-tag seq )
    swap 1array ; inline

: unpack-others ( n prev-tag -- tag seq/f )
    over HEX: f bitand
    use-right-tag (unpack-32bits) ;

: unpack-32bits ( n -- tag seq/f )
    Prev-Tag get dup big-number-tag?
    [ unpack-big-number -1 ]
    [ unpack-others over ] if
    Prev-Tag set ;

: (decode-unpacked) ( num-seq -- alpha-num-seq )
    [ first ]
    [ second over ${ green-number yellow-number } member?
      [ [ second ] [ first ] bi 0 = [ number>string ] [ >hex ] if 1array ]
      [ [ 1string ] map concat ] if
    ] bi 2array ; inline

: decodeable-tags-only ( alpha-num-seq -- alpha-num-seq' )
    [ last empty? not ] filter ; inline

: decode-unpacked ( num-seq -- alpha-num-seq )
    decodeable-tags-only
    [ (decode-unpacked) ] map ;

: binaries>numbers ( binary-seq -- numbers-seq )
    4 group [ le> ] map ;

: decode-block ( block-binary-data -- decoded-data )
    binaries>numbers [ unpack-32bits 2array ] map ;

#!
#! Writing blocks
#!

CONSTANT: Cf-Chars
    H{
      { " " {   0 4 } }
      { "r" {   1 4 } }
      { "t" {   2 4 } }
      { "o" {   3 4 } }
      { "e" {   4 4 } }
      { "a" {   5 4 } }
      { "n" {   6 4 } }
      { "i" {   7 4 } }

      { "s" {  16 5 } }
      { "m" {  17 5 } }
      { "c" {  18 5 } }
      { "y" {  19 5 } }
      { "l" {  20 5 } }
      { "g" {  21 5 } }
      { "f" {  22 5 } }
      { "w" {  23 5 } }

      { "d" {  96 7 } }
      { "v" {  97 7 } }
      { "p" {  98 7 } }
      { "b" {  99 7 } }
      { "h" { 100 7 } }
      { "x" { 101 7 } }
      { "u" { 102 7 } }
      { "q" { 103 7 } }

      { "0" { 104 7 } }
      { "1" { 105 7 } }
      { "2" { 106 7 } }
      { "3" { 107 7 } }
      { "4" { 108 7 } }
      { "5" { 109 7 } }
      { "6" { 110 7 } }
      { "7" { 111 7 } }

      { "8" { 112 7 } }
      { "9" { 113 7 } }
      { "j" { 114 7 } }
      { "-" { 115 7 } }
      { "k" { 116 7 } }
      { "." { 117 7 } }
      { "z" { 118 7 } }
      { "/" { 119 7 } }

      { ";" { 120 7 } }
      { "'" { 121 7 } }
      { "!" { 122 7 } }
      { "+" { 123 7 } }
      { "@" { 124 7 } }
      { "*" { 125 7 } }
      { "," { 126 7 } }
      { "?" { 127 7 } }
    }

ERROR: invalid-colorforth-char ;

SYMBOL: ColorForth-Char

: get-cf-char ( char -- cf-seq )
    dup ColorForth-Char set
    Cf-Chars at* [ invalid-colorforth-char ] unless ;

: init-char-packed ( function char -- packed' pos' )
    get-cf-char [ first ] [ second ] bi
    [ 32 swap - shift bitor ] keep ;

:: next-char-packed ( char packed pos -- packed' pos'/f )
    char get-cf-char [ first ] [ second ] bi
    pos + dup 27 <=
    [ [ 32 swap - shift packed bitor ] keep ]
    [ 2drop packed f ]
    if ;

 : pack-cf-word-part ( cf-chars-seq packed pos -- cf-chars-seq' packed pos'/f )
      pick empty?
      [ drop f ]
      [ pick last 1string -rot* next-char-packed
        dup [ pick pop drop pack-cf-word-part ] when ]
      if ; recursive

 : pack-cf-word-parts ( vector cf-chars-seq packed pos -- vector cf-chars-seq' packed pos'/f )
     pick empty?
     [ dup [ 2drop 0 0 ] unless
       pack-cf-word-part
       over 5 npick push
       pack-cf-word-parts
     ] unless
 ; recursive

 : pack-cf-word-rest ( packed pos'/f rest -- packed-vector )
     reverse -rot*
     V{ } clone 4 -nrot pack-cf-word-parts
     3drop ;

SYMBOL: Pack-Cf-Word-Args

: pack-cf-word ( word function -- packed-vector )
    2dup 2array Pack-Cf-Word-Args set
    swap >vector
    [ first 1string init-char-packed ] [ rest ] bi
    dup empty?
    [ 2drop 1array ] [ pack-cf-word-rest ] if ;

: (pack-cf-number) ( number hex function -- packed )
     [ 32 bits 5 shift ]
     [ 4 shift bitor ]
     [ bitor ]
     tri* ;

#! hex is 0 or 1.
: pack-cf-number ( number hex tag -- packed-array )
     [ 32 bits ] 2dip
     pick HEX: 7ffffff <=
     [ (pack-cf-number) 1array ]
     [ { { $ yellow-number [ $ yellow-big-number ] }
         { $ green-number  [ $ green-big-number ] }
         { $ yellow-big-number [ $ yellow-big-number ] }
         { $ green-big-number  [ $ green-big-number ] }
       } case
       0 -rot* (pack-cf-number) swap 2array
     ] if ;

#!
#! Source to packed sequence
#!

SYMBOL: Colored-Source-Input
SYMBOL: Colored-Source-Output

#! t means yellow
:: >colored-source ( seq -- clumps )
    seq Colored-Source-Input set
    t :> push-it!
    V{ } clone
    seq { f } append 2 clump
    [ push-it
      [
      dup last "org" = [ first $ yellow-number 2array t push-it! ]
      [ dup first "org" =  [ first $ yellow-word 2array t push-it! ]
        [ dup first ":" =  [ second $ red-word 2array f push-it! ]
            [ dup first string>number [ first $ green-number 2array t push-it! ]
                [ first $ green-word 2array t push-it! ] if
            ] if
        ] if
      ] if over push
      ] [ drop t push-it! ] if
    ] each
    dup Colored-Source-Output set ;

SYMBOL: Node>Array-Node

: node>array ( node -- array-of-seq )
    dup Node>Array-Node set
    compilation>> source>>
    source-translate-aliases
    source-without-names
    source-without-lit dup
    [ >colored-source
      [ dup first string>number dup
        [ 0 rot* second pack-cf-number ]
        [ drop [ first ] [ second ] bi pack-cf-word ]
        if
      ] map
    ] when ;

#!
#! Blocks
#!

: extract-block ( binary-data n -- block-binary-data )
    1024 * dup 1024 + rot* subseq ;

#! Read

CONSTANT: Retained-Blocks 1440
ERROR: block-file-not-retained-error ;

: read-block-file ( path -- file-byte-array )
    binary file-contents
    dup length Retained-Blocks 1024 * =
    [ block-file-not-retained-error ] unless ;

: block-file-contents ( path -- file-byte-array )
    fdir prepend-path read-block-file ;

: read-block ( file-byte-array block -- seq )
    -1 Prev-Tag set
    extract-block decode-block decode-unpacked ;

! Write

: write-block-file ( file-byte-array path -- )
    binary set-file-contents ;

: write-block-file-contents ( file-byte-array path -- )
    fdir prepend-path write-block-file ;

: write-block-header ( node -- packed-array )
    node#>> 0 $ yellow-number pack-cf-number
    "reclaim" $ yellow-word pack-cf-word prepend
    "node" $ yellow-word pack-cf-word append ;

: packed>bytes ( seq -- bytearray )
    [ s32>byte-array ] map concat ;

: create-block ( packed-vector -- byte-array )
    packed>bytes 1024 0 [ append ] padding ;

: replace-block ( file-byte-array byte-array block -- file-byte-array' )
     1024 * -rot*
     [ 4 npick + pick set-nth ] each-index nip ;

: write-block ( file-byte-array packed-vector block -- file-byte-array' )
    [ create-block ] dip replace-block ;

: node>packed-vector ( node -- packed-vector )
    [ write-block-header ] [ node>array concat ] bi append ; inline

: (write-blocks) ( node index file-byte-array start-block -- )
    4 nrot 4 nrot
    [ node>packed-vector ]
    [ 2 * rot* + write-block drop ] bi* ;

: write-blocks ( file-byte-array start-block -- file-byte-array' )
    this-chip nodes>> sift
    [ 4 npick 4 npick (write-blocks) ] each-index drop ;

#! USE: arrayfactor.chip.node.f18a.compilation.cfs.blocks
