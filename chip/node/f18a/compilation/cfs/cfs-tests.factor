! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: tools.test namespaces sequences kernel quotations vectors
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation.cfs ;
IN: arrayfactor.chip.node.f18a.compilation.cfs.tests

[ V{ "dup" ":" "wrd" "dup" "[" "1" "2" "+" "]" "10" "255" ";" "[" "169" } ]
[ 143 <cfs> drop 
  cfs@ "[dup :wr +d (-- ]dup [1 [2 [+ ]#10 ]$ff ]; $a9" cfs>new-source
  cfs@ get-cfs-new-source
]
unit-test

[ { "[#0" "[org"
    "]:" "]-swap" "]-" "]." "]+" "]-" "];"
    "]:" "]*," "]a" "]push" "]dup" "]a!" "]dup" "]or" "]#17" "]for" "]." "]+*" "]unext" "]drop" "]drop" "]a" "]pop" "]a!" "];"
    "]:" "]gcnt" "]begin" "]@" "]while" "]drop" "]#1" "]." "]+" "]end" "]drop" "];"
    "]:" "]sv" "]over" "]over" "]or" "]if" "]drop" "]!b" "];" "]then" "]drop" "]drop" "];"
    "]:" "]sieve" "]begin" "]@" "]while" "]begin" "]over" "]-" "]over" "]+" "]-"
         "]-while" "]drop" "]push" "]over" "]." "]+" "]pop" "]end" "]drop" "]sv" "]end" "];"
    "]:" "]svcnt" "]push" "]-if" "]drop" "]over" "]#1" "]." "]+" "]!b" "]pop" "]sieve" "];" "]then"
    "]pop" "]drop" "]drop" "]drop" "]#1" "]." "]+" "]gcnt" "]!b" "];"
} ]
[ V{ "0" "org"
     ":" "-swap" "-" "." "+" "-" ";"
     ":" "*," "a" "push" "dup" "a!" "dup" "or" "17" "for" "." "+*" "unext" "drop" "drop" "a" "pop" "a!" ";"
     ":" "gcnt" "begin" "@" "while" "drop" "1" "." "+" "end" "drop" ";"
     ":" "sv" "over" "over" "or" "if" "drop" "!b" ";" "then" "drop" "drop" ";"
     ":" "sieve" "begin" "@" "while" "begin" "over" "-" "over" "+" "-"
         "-while" "drop" "push" "over" "." "+" "pop" "end" "drop" "sv" "end" ";"
     ":" "svcnt" "push" "-if" "drop" "over" "1" "." "+" "!b" "pop" "sieve" ";" "then"
         "pop" "drop" "drop" "drop" "1" "." "+" "gcnt" "!b" ";"
} source>cfs ]
unit-test

! [ { "]dup ]drop" "]over ]. ]+" } ]
! [ "chip/node/f18a/cfs/test1.cfs" read-file ]
! unit-test

#! fdir "preferences.xml" append-path file>xml

#! "arrayfactor.chip.node.f18a.compilation.cfs" test
