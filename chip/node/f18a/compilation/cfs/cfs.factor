! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.core.files
arrayfactor.core.preferences combinators grouping
io.encodings.utf8 io.files io.pathnames kernel locals math
math.parser namespaces sequences splitting vectors ;
IN: arrayfactor.chip.node.f18a.compilation.cfs

TUPLE: cfs
    { node# integer }
    { source vector }
    { new-source vector }
    { binary vector }
    { executing boolean } ;

SYMBOL: Cfs

: <cfs> ( node# -- cfs )
                   #! node#
    V{ } clone     #! source
    V{ } clone     #! new-source
    V{ } clone     #! binary
    t              #! executing
    cfs boa
    dup Cfs set ;

: get-cfs-executing ( cfs -- ? )
    executing>> ;

: set-cfs-executing ( cfs ? -- )
    >>executing drop ;

: get-cfs-source ( cfs -- vector )
    source>> ;

: get-cfs-new-source ( cfs -- vector )
    new-source>> ;

#!
#! CONVERT CFS TO FORTH
#!

SYMBOL: Cfs-Mode

: pass ( code cfs -- )
    new-source>> push ;

:: pass+ ( code cfs -- )
    Cfs-Mode get "(" =
    [ cfs new-source>> dup empty?
      [ drop ] [ [ pop code rest append ] [ push ] bi ] if
    ] unless ;

: pass[ ( cfs -- cfs )
    dup executing>> [ "[" over pass t over executing<< ] unless ;

: pass] ( cfs -- cfs )
    dup executing>> [ "]" over pass f over executing<< ] when ;

:: cfs$ ( code cfs -- )
    code rest hex> dup
    [ cfs pass[ drop number>string ] [ drop code rest ] if
    cfs pass ;

:: cfs]$ ( code cfs -- )
    !TODO make previous ] explicit
    code rest hex> dup
    [ number>string ] [ drop code rest ] if
    cfs pass ;

: cfs# ( code cfs -- )
    [ rest ] dip pass ;

:: maybe-pass-number ( code-rest cfs -- )
    code-rest 1 head
    {
      { "$" [ code-rest cfs cfs]$ ] }
      { "#" [ code-rest cfs cfs# ] }
      [ drop code-rest cfs pass ]
    } case ;

:: maybe-cfs]; ( code cfs -- ? )
    code "];" = dup [
        #! cfs pass] t set-cfs-executing
        ";" cfs pass
    ] when ;
    
: cfs: ( code cfs -- )
    [ f set-cfs-executing ]
    [ ":" swap pass ]
    [ [ rest ] dip pass ] tri ;

: cfs[ ( code cfs -- )
    pass[ [ rest ] dip maybe-pass-number ;

: cfs] ( code cfs -- )
    pass] [ rest ] dip maybe-pass-number ;

: no-cfs-mode ( -- )
    "" Cfs-Mode set ;

:: prefix-actions ( code cfs -- )
    code cfs maybe-cfs]; [
      code empty? [
        code 1 head
        {
          { "(" [ "(" Cfs-Mode set ] }
          { "." [ no-cfs-mode ] }
          { "," [ no-cfs-mode ] }
          { "$" [ no-cfs-mode code cfs cfs$ ] }
          { "#" [ no-cfs-mode code cfs cfs# ] }
          { ":" [ no-cfs-mode code cfs cfs: ] }
          { "[" [ no-cfs-mode code cfs cfs[ ] }
          { "]" [ no-cfs-mode code cfs cfs] ] }
          { "+" [ code cfs pass+ ] }
          [ no-cfs-mode drop code cfs pass ]
        } case
      ] unless
    ] unless ;

:: cfs-vector>new-source ( cfs source -- )
     V{ } clone cfs new-source<<
     source [ cfs prefix-actions ] each ;

: cfs>new-source ( cfs source -- )
    " " split cfs-vector>new-source ;

: translate-source ( cfs -- )
    dup source>> " " join cfs>new-source ;

: write-new-source ( cfs relative-path -- )
    [ new-source>> " " join ] dip fdir prepend-path utf8 set-file-contents ;

#!
#! CONVERT FORTH TO CFS
#!

ERROR: not-a-number-before-org-error ;

: check-for-number ( string -- )
    string>number [ not-a-number-before-org-error ] unless ; inline

: translate-number-org ( vector -- [n/f ? )
    dup second "org" = dup [ [ first dup check-for-number "[#" prepend ] dip ] when ; inline

: translate-org ( vector -- [org/f ? )
    dup first "org" = dup [ [ first "[" prepend ] dip ] when ; inline

: translate-other ( vector -- [.. )
    first dup string>number [ "]#" ] [ "]" ] if prepend ; inline

: source>cfs ( vector -- string )
    { f } append 2 clump
    [ translate-number-org
      [ translate-org
        [ translate-other ] unless
      ] unless
    ] map ;

#!
#! READ SOURCE FROM CFS FILE
#!

: remove-comment ( string -- string )
    " " split "\\" over index [ head ] when*
    dup empty? [ drop "" ] [ " " join [ 32 <= ] trim ] if ;

: read-source ( cfs relative-path -- )
    read-file [ remove-comment ] map
    [ "" = not ] filter >vector >>source drop ;

: read-hex ( cfs relative-path -- )
    read-file [ remove-comment ] map
    [ "" = not ] filter >vector
    >>binary drop ;

#! FETCH

: cfs@ ( -- cfs )
    Cfs get ;

#! USE: arrayfactor.chip.node.f18a.compilation.cfs
