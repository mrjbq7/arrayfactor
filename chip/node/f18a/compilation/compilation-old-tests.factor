#! Compilation and assembly

[ H{ { "2+" 0 } }
  V{ V{ "@p" "+" ";" "." } V{ 2 } } ]
[ this-chip init-arrays
  143 {node}
  f this-compilation executing<<
  this-compilation  ": 2+ [ 1 dup + ] lit + ;" compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram get-names
  this-ram code-trimmed>
] unit-test

[ H{ { "2+" 0 } }
  V{ V{ f "@p" "+" f ";" "." } V{ 2 } V{ f V{ 0 "call" } } }
  V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 6 }
  V{ } ]
[ this-chip init-arrays
  143 {node}
  f this-compilation executing<<
  this-compilation ": 2+ [ 1 dup + ] lit + ; 4 2+" compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram get-names
  this-ram get-timed-code*
  0 this-assembly replace-a9-code 
  this-chip start-timed-executes
  5 [ this-chip timed-executes ] times
  143 node#>nd this-chip node!
  this-node get-stacks {ds}
  this-node instruction-stack->
] unit-test

#! cfs files

[ ]
[ this-chip init-arrays
  1 {node}
  cfs@ "temp/p2/node1/1.cfs" read-source
  ! cfs@ get-cfs-source
  cfs@ translate-source
  ! cfs@ get-cfs-new-source
  ! cfs@ "p2/node1/1.factor" write-new-source
] unit-test
