! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip arrayfactor.chip.chip-type
arrayfactor.chip.connection arrayfactor.chip.node
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.debug
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.rom
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.nodes
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.library.rom.macros
arrayfactor.core.macros
arrayfactor.core.utils
kernel logging math namespaces
quotations sequences tools.test vectors ;
FROM: arrayfactor.chip.node.f18a.memory.debug => break ;
IN: arrayfactor.chip.node.f18a.compilation.tests

: n#  ( n -- ) <smart-integer> this-node # ;
: n#> ( -- n ) this-node #> n> ;

<ga144> <chip> drop

#! Compile a number

[ f t V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 123 }
  f t V{ "123" } ]
[ 143 {node}
  t this-compilation executing<<
  "a" this-compilation compile-number?
  "123" this-compilation compile-number?
  this-node get-stacks {ds}
  f this-compilation executing<<
  "a" this-compilation compile-number?
  "123" this-compilation compile-number?
  this-compilation get-source
] unit-test

#! Switch executing

[ f t f t V{ ":" ";" } ]
[ this-chip init-arrays
  143 {node}
  t this-compilation executing<<
  ":" this-compilation compile-code
  this-compilation executing>>
  "[" this-compilation compile-code
  this-compilation executing>>
  "]" this-compilation compile-code
  this-compilation executing>>
  ";" this-compilation compile-code
  this-compilation executing>>
  this-compilation get-source
] unit-test

#! Executing or not executing

[ V{ 2 87381 87381 87381 87381 87381 87381 1 2 3 }
  V{ "1" "2" "over" "over" "+" } ]
[ this-chip init-arrays
  143 {node}
  t this-compilation executing<<
  this-compilation  "1 2 over over +" compile-source
  this-node get-stacks {ds}
  f this-compilation executing<<
  this-compilation  "1 2 over over +" compile-source
  this-compilation get-source
] unit-test

#! Compile word

[ V{ ":" "2+" "2" "lit" "+" ";" } ]
[ this-chip init-arrays
  143 {node}
  f this-compilation executing<<
  this-compilation ": 2+ [ 1 dup + ] lit + ;" compile-source
  this-compilation get-source
] unit-test

#!
#! if ... then
#!

[ V{ ":" "a" "-5" "87381" "lit" "if" "dup" "." "+" "then;" } ]
[ this-chip init-arrays
  4 {node}
  f this-compilation executing<<
  this-compilation ": a -5 lit if dup . + then;" compile-source
  this-compilation get-source
] unit-test

[ V{ "0" "87381" "lit" "if" "dup" "." "+" "then" } ]
[ 5 {node}
  f this-compilation executing<<
  this-compilation "0 lit if dup . + then" compile-source
  this-compilation get-source
] unit-test

[ V{ "-5" "87381" "lit" "-if" "dup" "." "+" "then" } ]
[ 6 {node}
  f this-compilation executing<<
  this-compilation "-5 lit -if dup . + then" compile-source
  this-compilation get-source
] unit-test

#! Compilation, assembly and execution

#! For ... unext

[ V{
    V{ f "@p" "push" "." "." }
    V{ 5 }
    V{ "dup" "2*" f "unext" f "@p" }
    V{ 999 }
    V{ f "@p" f ";" }
    V{ 888 }
    V{ f V{ 0 "call" } }
} V{ 87381 87381 999 1 2 4 8 16 32 64 } ]
[
  this-chip init-arrays
  13 {node} 999 n# 1 n#
  this-compilation "0 org : tst 5 for dup 2* unext 999 888 ; tst" compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  0 this-assembly replace-a9-code
  this-ram get-timed-code*

  start-stepping
  32 steps

  13 >current-node
  this-node get-stacks {ds}
]
unit-test

#!
#! MACROS
#!

[ H{ { ",double" ": ,double dup + ;" } } ]
[ this-chip init-arrays
  111 {node}
  ": ,double dup + ;" ",double" this-compilation get-macros add-macro
  this-compilation get-macros lib>>
] unit-test

[ H{ { ",double" "dup +" } }
  V{ "0" "org" ":" "double" "dup" "+" ";" ":" "init" "double" ";" "169" "org" "]" "init" ";" }
  V{ V{ "dup" "+" ";" } V{ V{ 0 "jmp" } } } ]
[ this-chip init-arrays
  112 {node}
  "dup +" ",double" this-compilation get-macros add-macro
  this-compilation get-macros lib>>
  this-compilation "0 org : double ,double ; : init double ; 169 org ] init ;" compile-source
  this-compilation expanded>>
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram code-trimmed>
! Assemble-Source-Clump get
] unit-test

#!
#! cn not implemented for now!
#!
! [ H{ { ",double" "dup +" } }
!   V{ "0" "org" ":" "double" "dup" "+" ";" ":" "init" "double" "[" "15" "cn" "]" "lit" ";" "169" "org" "]" "init" ";" }
!   V{
!     V{ "dup" "+" ";" }
!     V{ V{ 0 "call" } }
!     V{ "@p" ";" }
!     V{ 469 }
!   } ]
! [ this-chip init-arrays
!   14 {node}
!   "dup +" ",double" this-chip get-macros add-macro
!   this-chip get-macros lib>>
!   this-compilation "0 org : double ,double ; : init double [ 15 cn ] lit ; 169 org ] init ;" compile-source
!   this-compilation expanded>>
!   this-compilation get-source
!   this-assembly assemble-source
!   this-assembly end-assembly
!   this-ram code-trimmed>
! ] unit-test

#!
#! SMART-INTEGER
#!

[ V{ "0" "org" ":" "test" "1" "2" "nm>t" "acc" "+" ";" }
  V{ V{ "@p" "@p" T{ integer-name { register "t" } { the-name "acc" } } "+" "." } V{ 1 } V{ 2 } V{ ";" } }
  V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 3 }
  T{ smart-integer { n 3 } { name/f "acc" } { sequence# 1 } { base 16 } }
]
[ this-chip init-arrays
  114 {node}
  this-compilation "0 org : test 1 2 nm>t acc + ;" compile-source
  this-compilation expanded>>
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram code-trimmed>
  0 this-assembly replace-a9-code
  this-chip start-timed-executes
  10 [ this-chip timed-executes ] times
  this-node get-stacks {ds}
  this-node get-stacks t>
] unit-test

[ V{ "0" "org" ":" "test" "5" "nm>t" "invert" "dup" "-" "1" "." "+" ";" }
  V{ V{ "@p" T{ integer-name { register "t" } { the-name "invert" } } "dup" "." "." }
     V{ 5 } V{ "-" "@p" "." "+" } V{ 1 } V{ ";" } }
  V{ 5 87381 87381 87381 87381 87381 87381 87381 5 -5 }
  -5
]
[ this-chip init-arrays
  114 {node}
  this-compilation "0 org : test 5 nm>t invert dup - 1 . + ;" compile-source
  this-compilation expanded>>
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  0 this-assembly replace-a9-code
  this-ram code-trimmed>
  this-chip start-timed-executes
  15 [ this-chip timed-executes ] times
  this-node get-stacks {ds}
  this-node get-stacks t> n>
] unit-test

#!
#! A9
#!

[ V{ "4" "org" ":" "test" "1" "2" "+" ";" "169" "org" "test" ";" }
  V{ V{ 4 "jmp" } } ]
[ this-chip init-arrays
  114 {node}
  this-compilation "4 org : test 1 2 + ; 169 org test ] ;" compile-source
  this-compilation get-source dup
  this-assembly assemble-source
  this-assembly end-assembly
  41 this-rom code-trimmed> nth
] unit-test

#!
#! *+
#!

[ "0 org : *, a push a! dup dup or 17 for . +* unext drop drop a pop a! ; : tst 5 6 *, ; tst"
  V{
      V{ "a" "push" "a!" "dup" }
      V{ "dup" "or" "@p" "." }
      V{ 17 }
      V{ "push" "." "." "." }
      V{ "." "+*" "unext" "." }
      V{ "drop" "drop" "a" "." }
      V{ "pop" "a!" ";" }
      V{ "@p" "@p" V{ 0 "jmp" } }
      V{ 5 }
      V{ 6 }
      V{ V{ 7 "call" } }
  }
  V{ 87381 5 87381 87381 87381 87381 87381 87381 87381 30 }
]
[ this-chip init-arrays
  700 {node}
  this-compilation "0 org : *, a push a! dup dup or 17 for . +* unext drop drop a pop a! ; : tst 5 6 *, ; tst" compile-source
  this-compilation expanded>> " " join
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  "tst" this-ram name>addr drop this-assembly replace-a9-code
  this-ram code-trimmed>
  this-chip start-timed-executes
  140 steps
  this-node get-stacks {ds}
] unit-test

#!
#! !b
#!

[ V{
    V{ "@p" "b!" "@p" "." }
    V{ 63 }
    V{ 7 }
    V{ "!b" "@p" "a!" "." }
    V{ 63 }
    V{ "@" ";" }
    V{ V{ 0 "call" } }
} V{ 87381 87381 87381 87381 87381 87381 87381 87381 87381 7 }
]
[ this-chip init-arrays
  700 {node}
  this-compilation "0 org : tst 63 b! 7 !b 63 a! @a ; tst" compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  "tst" this-ram name>addr drop this-assembly replace-a9-code
  this-ram code-trimmed>
  this-chip start-timed-executes
  30 steps
  this-node get-stacks {ds}
] unit-test

#!
#! !a+
#!

[ V{
    V{ "@p" "a!" "@p" "." }
    V{ 10 }
    V{ 1 }
    V{ "!+" "@p" "!+" "@p" }
    V{ 2 }
    V{ 10 }
    V{ "a!" "@+" "@+" T{ break } ";" }
    V{ V{ 169 "call" } }
    V{ V{ 169 "call" } }
    V{ V{ 169 "call" } }
    V{ 1 }
    V{ 2 }
} V{ 87381 87381 87381 87381 87381 87381 87381 1 2 87381 }
]
[ this-chip init-arrays
  700 {node}
  this-compilation "0 org : tst 10 a! 1 !a+ 2 !+ 10 a! @a+ @+ <break> ; 169 org ] tst"
  execute-until-break
  this-ram code-trimmed>
  this-node get-stacks {ds}
] unit-test

#!
#! ROM
#!

[ V{ V{ "@p" "@p" V{ 112 "jmp" } } V{ 5 } V{ 6 } } ]
[ this-chip init-arrays
  115 {node}
  this-compilation "0 org : mult 5 6 *.17 ;" compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  this-ram code-trimmed>
] unit-test

#! Rom

! "compilation-test" [

[ 36 t 0 t ]
[ this-chip init-arrays
  109 {node}
  this-compilation "100 org : double dup + ; 0 org : init 5 double ; 169 org ] init" compile-source
  this-compilation get-source
  this-assembly assemble-source
  this-assembly end-assembly
  "double" this-rom name>addr
  "init" this-ram name>addr
] unit-test

! ] with-logging

#! Connections

[ 469 t ]
[ 104 {node}
  this-compilation connection>> 105 nxtnd
  "cnn" this-compilation execute-connection-code >boolean
]
unit-test

[ f ]
[ 104 {node}
  this-compilation connection>> 105 nxtnd
  "cnx" this-compilation execute-connection-code >boolean nip
]
unit-test

[ 325 t 277 t 469 t 373 t ]
[ ! <ga144> <chip> drop
  106 {node}
  105 n# 107 n# 6 n# 206 n#
  4 [ "cn" this-compilation execute-connection-code >boolean ] times
]
unit-test

[ 325 t 277 t 469 t 373 t ]
[ ! <ga144> <chip> drop
  106 {node}
  { "north" "south" "east" "west" } [ this-compilation execute-connection-code >boolean ] each
]
unit-test

[ 104 {node}
  this-compilation 106 nxtnd
  "cnn" this-compilation execute-connection-code >boolean
]
must-fail

remove-chip

#! "arrayfactor.chip.node.f18a.compilation" test
