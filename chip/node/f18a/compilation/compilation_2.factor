#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations combinators combinators.short-circuit
fry kernel locals math.parser namespaces sequences
shuffle splitting strings vectors words
arrayfactor.chip
arrayfactor.chip.connection
arrayfactor.chip.node.codes
arrayfactor.chip.node
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.execution.time
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.rom
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.nodes
arrayfactor.chips
arrayfactor.core.files
arrayfactor.core.macros
arrayfactor.chip.smart-integer
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.f18a.compilation

#! The purpose of compilation is to build a string,
#! that can be read by source>code, and fed to assembly.
#! The words of the string are accumulated in the
#!   vector "source".

TUPLE: compilation
    { node }
    { connection connection }
    { cfs cfs }
    { macros macros }
    { macro-source vector }
    { expanded vector }
    { source vector }
    { expect-word boolean }
    { executing boolean } ;

SYMBOL: Compilation

: <compilation> ( node# -- compilation )
    f swap             #! node
    dup <connection>   #! connection
    swap <cfs>         #! cfs
    <macros>           #! macros
    V{ } clone         #! macro-source
    V{ } clone         #! expanded
    V{ } clone         #! source
    f                  #! expect-word
    t                  #! executing
    compilation boa
    dup Compilation set ;

: get-cfs ( compilation -- cfs )
    cfs>> ;

: get-source ( compilation -- vector )
    source>> ; inline

: get-macros ( compilation -- macros )
    macros>> ;

: clear-source ( compilation -- )
    [ drop V{ } clone ] change-source drop ;

#!
#! COMPILE CODES
#!

: *pass ( code compilation -- )
    source>> push ;

: *maybe-execute ( code compilation -- )
    dup executing>>
    [ node>> swap execute-opcode ] [ *pass ] if ;

: *num-code ( code compilation -- )
    [ dup node>> #> n> number>string swap
      source>> push drop ]
    [ *pass ] 2bi ;

#! Pop addr from return.
: *: ( code compilation -- )
    f >>executing
    *pass ;

: *; ( code compilation -- )
    t >>executing *pass ;

#! Start assembly.
: *] ( code compilation -- )
    f >>executing 2drop ;

#! Start executing.
: *[ ( code compilation -- )
    t >>executing 2drop ;

: *lit ( code compilation -- )
    *num-code ;

: *org ( code compilation -- )
    *num-code ;

: *, ( code compilation -- )
    *num-code ;

: *then ( code compilation -- )
    f >>executing *pass ;

: COMPILATION ( -- seq )
{
    [ ( code compilation -- ) *; ]                #! ";"
    [ ( code compilation -- ) *pass ]             #! "ex"
    [ ( code compilation -- ) *pass ]             #! "jmp"
    [ ( code compilation -- ) *pass ]             #! "call"
    [ ( code compilation -- ) *pass ]             #! "unext"
    [ ( code compilation -- ) *pass ]             #! "next"
    [ ( code compilation -- ) *pass ]             #! "if"
    [ ( code compilation -- ) *pass ]             #! "-if"
    [ ( code compilation -- ) *pass ]             #! "@p"
    [ ( code compilation -- ) *pass ]             #! "@+"
    [ ( code compilation -- ) *pass ]             #! "@b"
    [ ( code compilation -- ) *pass ]             #! "@"
    [ ( code compilation -- ) *pass ]             #! "!p"
    [ ( code compilation -- ) *pass ]             #! "!+"
    [ ( code compilation -- ) *pass ]             #! "!b"
    [ ( code compilation -- ) *pass ]             #! "!"
    [ ( code compilation -- ) *pass ]             #! "+*"
    [ ( code compilation -- ) *pass ]             #! "2*"
    [ ( code compilation -- ) *pass ]             #! "2/"
    [ ( code compilation -- ) *maybe-execute ]    #! "-"
    [ ( code compilation -- ) *maybe-execute ]    #! "+"
    [ ( code compilation -- ) *maybe-execute ]    #! "and"
    [ ( code compilation -- ) *maybe-execute ]    #! "or"
    [ ( code compilation -- ) *maybe-execute ]    #! "drop"
    [ ( code compilation -- ) *maybe-execute ]    #! "dup"
    [ ( code compilation -- ) *maybe-execute ]    #! "pop"
    [ ( code compilation -- ) *maybe-execute ]    #! "over"
    [ ( code compilation -- ) *pass ]             #! "a"
    [ ( code compilation -- ) *pass ]             #! "."
    [ ( code compilation -- ) *maybe-execute ]    #! "push"
    [ ( code compilation -- ) *pass ]             #! "b!"
    [ ( code compilation -- ) *pass ]             #! "a!"

    #! LITERALS

    [ ( code compilation -- ) *, ]                #! ","
    [ ( code compilation -- ) *lit ]              #! "lit"

    #! MISC

    [ ( code compilation -- ) *org ]              #! "org"
    [ ( code compilation -- ) *: ]                #! ":"
    [ ( code compilation -- ) *] ]                #! "]"
    [ ( code compilation -- ) *[ ]                #! "["

    #! CONTROL STRUCTURES

    [ ( code compilation -- ) *then ]             #! "then"
    [ ( code compilation -- ) *pass ]             #! "for"
    [ ( code compilation -- ) *pass ]             #! "begin"
    [ ( code compilation -- ) *pass ]             #! "while"
    [ ( code compilation -- ) *pass ]             #! "-while"
    [ ( code compilation -- ) *pass ]             #! "end"
    [ ( code compilation -- ) *pass ]             #! "until"
    [ ( code compilation -- ) *pass ]             #! "-until"

    #! ADDITIONAL

    [ ( code compilation -- ) *pass ]             #! "<break>"
    [ ( code compilation -- ) *pass ]             #! "nm>t"
    [ ( code compilation -- ) *pass ]             #! "nm>s"
    [ ( code compilation -- ) *pass ]             #! "nm>a"
    [ ( code compilation -- ) *pass ]             #! "nm>b"
    [ ( code compilation -- ) *pass ]             #! "nm>r"
} ; inline

: push>expanded ( code compilation -- )
    expanded>> push ; inline

: push>expanded? ( code compilation ? -- ? )
    [ swap push>expanded t ] [ 2drop f ] if ; inline

: is-number? ( compilation code -- ? )
    dup string>number push>expanded? ;

: ram-word? ( compilation code -- ? )
    swap node>> assembly>> get-ram name>addr nip ;

: is-ram-word? ( compilation code -- ? )
    2dup ram-word? push>expanded? ;

: rom-word? ( compilation code -- ? )
    swap node>> assembly>> get-rom name>addr nip ;

: is-rom-word? ( compilation code -- ? )
    2dup rom-word? push>expanded? ;

: add-node-macro ( code name compilation -- )
    macros>> add-macro ; inline

: read-node-macro ( relative-path name compilation -- )
    [ read-file ] 2dip add-node-macro ;

: node-macro? ( compilation code -- macro-source ? )
    swap macros>> name>macro ;

: expand-node-macro? ( compilation code -- ? )
    2dup node-macro?
    [ nip " " split >vector >>macro-source drop t ]
    [ 3drop f ] if ;

: add-chip-macro? ( chip code -- macro-source ? )
    swap macros>> name>macro ;

: expand-chip-macro? ( compilation code -- ? )
    this-chip swap
    2dup add-chip-macro?
    [ nip " " split >vector nip >>macro-source drop t ]
    [ 4drop f ] if ;

: is-other? ( compilation code -- ? )
    swap push>expanded t ;

ERROR: word-name-also-macro-name-error ;

: macro-name-conflict? ( compilation code -- )
    dup "Compile-Word-Name" dbg-set
    [ nip this-chip swap add-chip-macro? nip ]
    [ node-macro? nip ]
    2bi or
    [ word-name-also-macro-name-error ] when ;

: is-colon? ( compilation code -- ? )
    dup ":" = [ swap t >>expect-word push>expanded t ] [ 2drop f ] if ;

: is-new-word? ( compilation code -- ? )
    over
    [ expect-word>>
      [ [ macro-name-conflict? ] [ 2drop ] if ]
      [ push>expanded? ]
      3bi ]
    [ f >>expect-word drop ] bi ;

: expand-code ( code compilation -- )
    swap
    { [ is-colon? ]
      [ is-new-word? ]
      [ is-number? ]
      [ is-ram-word? ]
      [ expand-node-macro? ]
      [ expand-chip-macro? ]
      [ is-rom-word? ]
      [ is-other? ]
    } 2|| drop ;

: expanded-init ( compilation -- )
    V{ } clone >>expanded drop ; inline

: expand-macro-source ( compilation -- )
    dup macro-source>> dup empty?
    [ 2drop ]
    [ [ over expand-code ] each
      V{ } clone >>macro-source drop
    ] if ;

: CHECK-expand-source ( compilation vector -- compilation vector )
    over node>> nd>> nd>node# 208 =
    [ dup "Expand-Source" dbg-set ] when ;

: expand-source ( compilation vector -- )
    CHECK-expand-source
    over expanded-init
    swap [ [ expand-code ] [ expand-macro-source ] bi ]
    curry each ;

: split-source ( source -- vector )
    "\n" split " " join
    " " split [ "" = not ] filter >vector ; inline

: compile-port? ( code compilation -- ? )
    !TODO Useful to set a-port here?
    over string>number dup
    [ over executing>>
        [ <smart-integer>
          t >>a-port
          16 >>base
          swap node>> stacks>> (#) drop ]
        [ drop *pass ]
        if t
    ] [ 2nip ] if ;

: get-tos ( code compilation -- )
   swap 0 =
   [ [ connection>> ] [ node>> ] bi #> n> >>tos drop ]
   [ drop ] if ;

: execute-connection-code ( connection-code-text compilation -- port ? )
    [ text>connection ] dip
    2dup get-tos
    connection>> swap
    dup [ dup [ CONNECT nth call( connection -- port ) ] dip ] when ;

: compile-connection-code? ( code compilation -- ? )
    [ execute-connection-code ] keep
   '[ number>string _ compile-port? drop t ]
    [ drop f ] if ;

: compile-number? ( code compilation -- ? )
    over string>number dup
    [ over executing>>
        [ <smart-integer>
          swap node>> stacks>> (#) drop ]
        [ drop *pass ]
        if t
    ] [ 2nip ] if ;

: compile-code ( code compilation -- )
    2dup compile-number?
    [ 2drop ]
    [ 2dup compile-connection-code?
      [ 2drop ]
      [ over op> dup
        [ COMPILATION nth call( code compilation -- ) ]
        [ drop *pass ] if
      ] if
    ] if ;

SYMBOLS: Compile-Source Compile-Source-Index ;

: CHECK-compile-source ( compilation vector -- compilation vector )
    over node>> nd>> nd>node# 208 =
    [ dup "Compile-Source" dbg-set ] when ;

: compile-source ( compilation source -- )
    over clear-source
    split-source dup Compile-Source set
    { [ expand-source ]
      [ drop dup expanded>>
        [ Compile-Source-Index set over compile-code ]
        each-index drop ]
    } 2cleave ;

: format-source ( source -- source' )
    ":" split "\n:" join ;

ERROR: execute-until-break-error ;

: check-for-break ( source -- )
    "<break>" swap member?
    [ execute-until-break-error ] unless ;

#! Puts a break after the source to make it stop.
: execute-until-break ( compilation source -- )
    2dup compile-source drop
    dup source>>
    [ nip check-for-break ]
    [ swap node>> assembly>> ] 2bi
    [ assemble-source ] [ nip end-assembly ] 2bi
    this-chip start-timed-executes
    100 steps ;

#! FETCH

: compilation@ ( -- compilation ) Compilation get ;
