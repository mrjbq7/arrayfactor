#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations
arrayfactor.chip
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.debug
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.node.f18a.status
arrayfactor.chip.port arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils
arrays combinators
kernel locals math math.bitwise models namespaces sequences
vectors vocabs.loader ;
FROM: sequences => change-nth ;
IN: arrayfactor.chip.node.f18a.execution

#!
#! PORTS
#!

:: copy-t-to-port ( directions node -- )
    directions [ node stacks>> t> swap node nd>> get-port p-value<- ] each ;

#!
#! BLOCKING
#!

:: blocking? ( node -- ? )
    node status>> blocking-port#>> port>rdlu :> directions
    node status>> blocking-> dup
    [ directions node nd>> any-port-accepted-request?
      [ node status>> unblock drop f ] when
    ] when ;

:: block?! ( opcode addr directions node -- )
    directions node nd>> change-active-nodes-in-port
    addr node status>>
    opcode { "@" "@+" "@b" } member?
    [ block-reading directions node nd>> request-read-from-port ]
    [ block-writing directions node nd>> request-write-to-port
      #! Write value to all ports, that request to write.
      directions node copy-t-to-port
    ] if ;

:: accept?! ( opcode directions node -- )
   directions node nd>> this-node-activated-port?
   [ node push-f-back ]
   [ directions node nd>>
     opcode { "@" "@+" "@b" } member?
     [ neighbour-writing? ]
     [ neighbour-reading? dup
       #! Write value to first reading port.
       [ directions first 1array node copy-t-to-port ] when
     ] if
     [ #! Accept request from first direction.
       directions first 1array node nd>> request-accepted-by-port
       directions node nd>> init-other-ports
     ] when
   ] if ;

:: block-or-accept?! ( node -- )
    node instruction-stack-> last dup :> opcode
    dup "Next-Opcode" dbg-set
    #! Does opcode possibly cause blocking or accepting a request?
    { "@" "!" "@+" "!+" "@b" "!b" } member?
    [ #! Get the port from register.
      node
      opcode { "!b" "@b" } member? [ b> n> ] [ a> n> ] if
      dup :> addr dup port#?
      [ port>rdlu dup empty? not
        [ dup :> directions
          node nd>> activated-port?
          #! Only an activated port might cause accepting a request.
          [ opcode directions node accept?! ]
          #! Only a not activated port might cause blocking.
          [ opcode addr directions node block?! ]
          if
        ] [ drop ] if  #! No direction
      ] [ drop ] if  #! Not a port
    ] when ;

#!
#! MOVING DATA
#!

:: data>port ( data addr node -- )
    data dup smart-integer? [ <smart-integer> ] unless
    addr port>rdlu dup empty?
    [ 2drop ] [  node nd>> write-to-port ] if ;

: port>data ( addr node -- data/f )
    swap port>rdlu dup empty?
    [ 2drop f ]
    [ swap nd>> [ read-from-port ] [ init-connected-ports ] 2bi ]
    if ;

: data>addr ( data addr node -- )
    over port#? [ data>port ] [ data>memory ] if ;

ERROR: addr>data-error ;

: addr>data ( addr node -- data/f )
    over dup "Addr>" dbg-set
    port#? [ port>data ] [ memory>data ] if
    dup [ dup smart-integer? [ <smart-integer> ] unless ] [ addr>data-error ] if ;

#!
#! P
#!

: code>vector ( code p node -- )
    memory>> (memory-code>)
    rot* [ nip ] curry change-nth ;

#! put code into code vector at p.
: >p! ( code node -- )
    dup p> dup port#?
    [ swap data>port ]
    [ swap [ code>vector ] [ p-inc ] bi ]
    if ;

#! get code from code vector at p
: p@> ( node -- code )
    dup p> dup port#?
    [ swap port>data ]
    [ swap [ memory>> memory-code-item ] [ p-inc ] bi ] if ;

#!
#! A
#!

#! Nop
: ,. ( node -- )
    drop ;

ALIAS: ,nop ,.

: #> ( node -- n )
    stacks>> ,#> ;

: # ( n/f node -- )
    stacks>> ,# ;

#! Store from data-stack into a.
: ,a! ( node -- )
    [  #> dup ]
    [ [ n> ] dip nd>> maybe-create-ports drop ]
    [ >a ] tri ;

#! Fetch from a to data-stack.
: ,a ( node -- )
    [ a> ] [ # ] bi ;

ALIAS: ,a@ ,a

#! Store from data-stack throu a.
: ,! ( node -- )
    [ [ #> ] [ a> n> ] bi ] keep data>addr ;

: no-port>inc ( smart-integer -- )
    dup n> port#?
    [ drop ] [ inc-smart-integer ] if ;

#! Store from data-stack throu a, increment a.
: ,!+ ( node -- )
    !TODO Should smart integers be stored in memory?
    [ [ #> ]
      [ a> [ n> ] [ no-port>inc ] bi ]
      bi
    ] keep data>addr ;

#! Fetch throu a to data-stack.
: ,@  ( node -- )
    [ a> n> ] [ addr>data ] [ # ] tri ;

#! Fetch throu a to data-stack, increment a.
:: ,@+ ( node -- )
    node dup a> n>
    [ node addr>data node # ] keep
    port#? [ drop ] [ a-inc?! ] if ;

ALIAS: ,!a ,!
ALIAS: ,@a ,@
ALIAS: ,!a+ ,!+
ALIAS: ,@a+ ,@+

#!
#! B
#!

#! Store from data-stack into b.
: ,b! ( node -- )
    [ #> dup ]
    [ [ n> ] dip nd>> maybe-create-ports drop ]
    [ >b ] tri ;

#! Store from data-stack thru b.
: ,!b ( node -- )
    !TODO b is only 9 bits!
    [ [ #> ]
      [ b> n> ] bi
    ] keep data>addr ;

#! Fetch throu b to data-stack.
: ,@b ( node -- )
    [ b> n> ] [ addr>data ] [ # ] tri ;

#!
#! P
#!

#! Store from data-stack thru p, increment p.
: ,!p ( node -- )
    { [ #> n> ] [ p> dup ] [ data>addr addr-inc ] [ >p ] } cleave ;

#! Fetch throu p to data-stack, increment p.
: ,@p ( node -- )
    { [ p> ] [ addr>data ] [ # ] [ p-inc?! ] } cleave ;

ALIAS: ,!p+ ,!p
ALIAS: ,@p+ ,@p

#!
#! ARITHMETIC
#!

: ##> ( node -- smart-integer n )
    #> dup n> ; inline

: ## ( node smart-integer n -- )
    over >n swap # ; inline

: #18bits# ( node smart-integer n -- )
    over 18bits>n swap # ; inline

: ,2* ( node -- )
    dup ##> 1 shift #18bits# ;

: ,2/ ( node -- )
    dup ##> -1 shift ## ;

: 2#> ( node -- smart-integer1 smart-integer2 )
    [ #> ] [ #> ] bi ; inline

: ,+ ( node -- )
    [ 2#> [ 2n-signed> + ] 2keep smart-integer-naming ]
    [ # ] bi ;

#! ,+*

: (,+*)1    ( node -- t:a )
    [ stacks>> s> n> >bignum ] [ stacks>> t> n> >bignum ] [ a> n> >bignum ] tri
    dup 1 bitand 0 =
    [ [ 18 shift ] dip bitor -1 shift nip ]
    [ [ + 18 shift ] dip bitor -1 shift ]
    if
; inline

: (,+*)split    ( t:a -- a t )
    [ HEX: 3ffff bitand ] [ -18 shift ] bi ;

: (,+*)2    ( t:a -- a-unsigned t-signed )
    dup 0 >=
    [ (,+*)split ] [ abs (,+*)split neg ] if
    [ abs HEX: 3ffff bitand f <<smart-integer>> ] [ <smart-integer> ] bi*
; inline

: (,+*)3    ( a t node -- )
    [ stacks>> t< ] [ >a ] bi ; inline

: ,+*    ( node -- )
    [ (,+*)1 (,+*)2 ] [ (,+*)3 ] bi ;

#!
#! LOGIC
#!

: ,- ( node -- )
    [ #> [ n> bitnot 18bits ] [ >named?-smart-integer ] bi ]
    [ # ] bi ;

: 2n>bitand ( smart-integer1 smart-integer2 -- integer )
    [ n> ] bi@ bitand ; inline

: ,and ( node -- )
    [ 2#> [ 2n>bitand 18bits ] 2keep smart-integer-naming ]
    [ # ] bi ;

: 2n>bitxor ( smart-integer1 smart-integer2 -- integer )
    [ n> ] bi@ bitxor ; inline

: ,xor ( node -- )
    [ 2#> [ 2n>bitxor 18bits ] 2keep smart-integer-naming ]
    [ # ] bi ;

ALIAS: ,not ,-
ALIAS: ,invert ,-

#!
#! BRANCHING
#!

:: p>instruction-stack ( node -- )
    node [ p> ] [ p-inc ] [ memory>> ] tri memory-get-timed-code-item
    reverse node instruction-stack<- ;

#! Return to addr in r. Increment p?
: ,; ( node -- )
    !TODO check if increment can't be handled here.
    [ stacks>> (,pop) n> ]
    [ >p ]
    [ p>instruction-stack ] tri ;

#! Coroutine
:: ,ex ( node -- )
    node
    [ p> <smart-integer> dup is-return-addr dup node stacks>> (#) node stacks>> (,pop) n> ]
    [ >p+ node stacks>> (,push) ] bi
    node p>instruction-stack ;

#! Jump to addr. (Increment p)
: ,jmp ( node -- )
    [ number>> ] [ >p ] [ p>instruction-stack ] tri ;

: p>smart-integer ( node -- smart-integer )
    [  p> dup ] [ memory>> ram>> ] bi addr>name
    [ <named-smart-integer> ] [ drop <smart-integer> dup is-return-addr ] if ;

#! Put p on return-stack. Jump to addr in ram. Increment p.
: ,call  ( node -- )
    { [ p>smart-integer dup ]
      [ stacks>> (#) ]
      [ stacks>> (,push) ]
      [ number>> ]
      [ >p ]
      [ p>instruction-stack ]
    } cleave ;

#!
#! CONTROL STRUCTURES
#!

: number>timed-code?! ( data node -- code )
    over number?
    [ p> binary2code >vector (code>timed-code) ]
    [ drop ] if ;

: vector>timed-code?! ( node data -- code )
    dup vector?
    [ dup first number? [ first swap number>timed-code?! ] [ nip ] if
    ] [ nip ] if ;

: ,unext ( node -- )
    dup stacks>> r> n> 0 <=
    [ stacks>> (,pop) drop ]
    [ [ clear-instruction-stack ] [ stacks>> r-- ] [ p-dec ] tri ] if ;

: ,next ( node -- )
    dup stacks>> r> n> 0 =
    [ stacks>> (,pop) drop ]
    [ [ stacks>> r-- ] [ number>> ] [ >p ] tri ] if ;

: ,if ( node -- )
    dup stacks>> t> n> 0 =
    [ [ number>> ] [ >p ] bi ] [ drop ] if ;

: ,-if ( node -- )
    dup stacks>> t> n> 17 bit?
    [ drop ] [ [ number>> ] [ >p ] bi ] if ;

: ,drop ( node -- ) stacks>> ,,drop ;
: ,dup ( node -- ) stacks>> ,,dup ;
: ,over ( node -- ) stacks>> ,,over ;
: ,push ( node -- ) stacks>> ,,push ;
: ,pop ( node -- ) stacks>> ,,pop ;

: ,then ( node -- ) drop ;
: ,for ( node -- ) ,push ;
: ,begin ( node -- ) drop ;
: ,while ( node -- ) ,if ;
: ,-while ( node -- ) ,-if ;
: ,end ( node -- ) drop ;
: ,until ( node -- ) ,if ;
: ,-until ( node -- ) ,-if ;

#!
#! EXECUTE CODES
#!

: EXECUTION ( -- seq )
{
    [ ,;      ]
    [ ,ex     ]
    [ ,jmp    ]
    [ ,call   ]
    [ ,unext  ]
    [ ,next   ]
    [ ,if     ]
    [ ,-if    ]
    [ ,@p     ]
    [ ,@+     ]
    [ ,@b     ]
    [ ,@a     ]
    [ ,!p     ]
    [ ,!+     ]
    [ ,!b     ]
    [ ,!a     ]
    [ ,+*     ]
    [ ,2*     ]
    [ ,2/     ]
    [ ,-      ]
    [ ,+      ]
    [ ,and    ]
    [ ,xor    ]
    [ ,drop   ]
    [ ,dup    ]
    [ ,pop    ]
    [ ,over   ]
    [ ,a@     ]
    [ ,nop    ]
    [ ,push   ]
    [ ,b!     ]
    [ ,a!     ]

    #! LITERALS

    [ drop    ] #! ","
    [ drop    ] #! "lit"

    #! MISC

    [ drop    ] #! "org"
    [ drop    ] #! ":"
    [ drop    ] #! "]"
    [ drop    ] #! "["

    #! CONTROL STRUCTURES

    [ ,then    ]
    [ ,for     ]
    [ ,begin   ]
    [ ,while   ]
    [ ,-while  ]
    [ ,end     ]
    [ ,until   ]
    [ ,-until  ]

    #! ADDITIONAL

    [ drop     ] #! "<break>"
    [ drop     ] #! "nm>t"
    [ drop     ] #! "nm>s"
    [ drop     ] #! "nm>a"
    [ drop     ] #! "nm>b"
    [ drop     ] #! "nm>r"

} ; inline

: execute-opcode ( node opcode-text -- )
    dup "Execution-Opcode" dbg-set
    op> EXECUTION nth call( node -- ) ;

: execute-vector ( node vector -- )
    [ first over number<< ]
    [ second execute-opcode ] bi ;

#!
#! NEXT-TIMED-INSTRUCTION
#!

ERROR: next-timed-instruction-error ;

: port-exec?! ( addr node -- timed-instruction/f )
      [ port>data ] keep over [ number>timed-code?! ] [ drop ] if ;

: (next-timed-instruction) ( addr node -- timed-instruction/f )
    [ [ memory>> memory-get-timed-code-item ] [ p-inc ] bi
      dup [ next-timed-instruction-error ] unless
    ] keep swap vector>timed-code?! ;

: next-timed-instruction ( node -- timed-instruction/f )
    !TODO Ram and Rom memory wrap
    dup p> dup "Next-P" dbg-set dup port#?
    [ swap port-exec?! ] [ swap (next-timed-instruction) ] if ;

: next-instruction-stack ( node -- instruction-stack )
    dup next-timed-instruction dup
    [ reverse dup rot* instruction-stack<- ] [ nip ] if ;

ERROR: empty-next-timed-instruction-error ;

: check-instruction-stack ( node -- )
    dup instruction-stack->
    dup empty? [ drop next-instruction-stack ] [ nip ] if
    empty? [ empty-next-timed-instruction-error ] when ;

: (execute-timed-opcode) ( node opcode -- )
    dup 
    [ dup number?
      [ >>number drop ]
      [ dup vector? [ execute-vector ] [ execute-opcode ] if ]
      if
    ] [ 2drop ] if ;

: smart-integer-action ( name smart-integer -- )
    over this-chip get-named-smart-integers name>smart-integer?
    [ 2nip inc-sequence# ]
    [ drop
      2dup >name
      swap this-chip get-named-smart-integers add>named-smart-integers ]
    if ;

: (do-integer-name) ( integer-name  smart-integer -- )
    [ the-name>> ] dip 
    dup [ smart-integer-action ] [ 2drop ] if ;

: do-integer-name ( integer-name  node -- )
    over register>>
    { { "t" [ stacks>> t> (do-integer-name) ] }
      { "s" [ stacks>> s> (do-integer-name) ] }
      { "a" [ a> (do-integer-name) ] }
      { "b" [ b> (do-integer-name) ] }
      { "r" [ stacks>> r> (do-integer-name) ] }
      [ 3drop ]
    } case ;

: pop-opcode ( node -- opcode )
    dup check-instruction-stack
    dup instruction-stack-model-> pop-model
    dup integer-name?
    [ over do-integer-name
      pop-opcode ]
    [ nip ] if ; recursive

: break?! ( opcode -- ? )
    break? dup [ this-chip breaking<< t ] when ;
    
: next-opcode ( node -- )
    dup [ pop-opcode ] [ break?! ] bi
    [ 2drop ] [ 1vector ->opcode drop ] if ;

: start-timed-execute ( node -- )
    [ HEX: a9 swap >p ] [ next-opcode ] bi ;


#! "arrayfactor.chip.nodes" require

! USE: arrayfactor.chip.node.f18a.execution
