! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: tools.test namespaces sequences kernel quotations vectors
arrayfactor.chip.node.f18a.execution.time  ;
IN: arrayfactor.chip.node.f18a.execution.time.tests

[ 10 ]
[ "over" code>execution-time ]
unit-test
