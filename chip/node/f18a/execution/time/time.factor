! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators fry grouping kernel math math.parser
sequences sets splitting unicode.normalize vectors
arrayfactor.chip.node.codes ;
IN: arrayfactor.chip.node.f18a.execution.time

: EXECUTION-TIME ( -- n )
{
    20 #! ";"
    20 #! "ex"
    20 #! "jmp"
    20 #! "call"
    20 #! "unext"
    20 #! "next"
    20 #! "if"
    20 #! "-if"
    20 #! "@p"
    20 #! "@+"
    20 #! "@b"
    20 #! "@"
    20 #! "!p"
    20 #! "!+"
    20 #! "!b"
    20 #! "!"
    10 #! "+*"
    10 #! "2*"
    10 #! "2/"
    10 #! "-"
    10 #! "+"
    10 #! "and"
    10 #! "or"
    10 #! "drop"
    10 #! "dup"
    10 #! "pop"
    10 #! "over"
    10 #! "a"
    10 #! "."
    10 #! "push"
    10 #! "b!"
    10 #! "a!"

    #! LITERALS

     0 #! ","
     0 #! "lit"

    #! MISC

     0 #! "org"
     0 #! ":"
     0 #! "]"
     0 #! "["

    #! CONTROL STRUCTURES

     0 #! "then"
     0 #! "for"
    20 #! "begin"
    20 #! "while"
    20 #! "-while"
     0 #! "end"
    20 #! "until"
    20 #! "-until"

    #! ADDITIONAL

     0 #! "<break>"
     0 #! "nm>t"
     0 #! "nm>s"
     0 #! "nm>a"
     0 #! "nm>b"
     0 #! "nm>r"

} ; inline

: code>execution-time ( opcode-text -- n )
    op> EXECUTION-TIME nth ;
