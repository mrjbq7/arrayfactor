! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations hashtables kernel math namespaces sequences vectors
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.rom
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.f18a.memory

TUPLE: memory
    { ram ram }
    { rom rom }
    { ram-hardcoded ram } ;

SYMBOL: Memory

: <memory> ( ram rom -- memory )
    <ram>   #! ram-hardcoded
    memory boa dup Memory set ;

: <start-memory> ( -- memory )
    <ram> 0 <rom> <memory> ;

: ram|rom-addr ( addr -- addr' )
    HEX: ff bitand ;

! ERROR: ram-or-rom-addr-error ;

! : ram|rom-addr ( addr -- addr' )
!    dup ram-addr?
!    [ mask-addr ]
!    [ dup rom-addr? [ rom-addr ] [ ram-or-rom-addr-error ] if
!    ] if ;

ERROR: addr-increment-error ;

: addr-inc ( addr -- addr' )
    dup ram-addr?
    [ ram-addr-inc ]
    [ dup rom-addr? [ rom-addr-inc ] [ addr-increment-error ] if
    ] if ;

: (ram|rom) ( addr memory -- addr ram/rom )
    over ram-addr? [ ram>> ] [ rom>> ] if
    [ mask-addr ] dip ; inline

: ram|rom ( addr memory -- ram/rom )
    (ram|rom) nip ; inline

: nd>memory ( nd -- memory )
    <rom> <ram> swap <memory> ;

#!
#! CODING
#!

: (memory-coding>) ( addr memory -- addr coding )
    (ram|rom) coding> ; inline

: memory-coding> ( addr memory -- coding )
    (memory-coding>) nip ;

: memory-coding-trimmed> ( addr memory -- coding )
    (memory-coding>) [ CA9-DISASM 1vector = ] trim-tail nip ;

: memory-coding-item ( addr memory -- coding-item )
    (memory-coding>) nth ; inline

#!
#! CODE
#!

: (memory-code>) ( addr memory -- addr code )
    (ram|rom) code> ; inline

: memory-code> ( addr memory -- code )
    (memory-code>) nip ;

: memory-code-trimmed> ( addr memory -- code )
    (memory-code>) [ CA9-DISASM 1vector = ] trim-tail nip ;

: memory-code-item ( addr memory -- code-item )
    (memory-code>) nth ; inline

#!
#! BINARY
#!

: (memory-get-binary) ( addr memory -- addr code )
    (ram|rom) binary> ; inline

: memory-get-binary-item ( addr memory -- binary-item )
    (memory-get-binary) nth ; inline

: memory-get-timed-code-item ( addr memory -- timed-code-item )
    memory-code-item (code>timed-code) ;
