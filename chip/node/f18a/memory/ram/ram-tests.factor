! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: tools.test namespaces sequences kernel quotations vectors
arrayfactor.chip.node.f18a.memory.ram  ;
IN: arrayfactor.chip.node.f18a.memory.ram.tests

[ 1 t ]
[ "if" code>extra-time ]
unit-test

[ 0 f ]
[ "dup" code>extra-time ]
unit-test

[ V{ f "@p" f V{ 3 "-if" } } ]
[ V{ "@p" V{ 3 "-if" } } (code>timed-code) ]
unit-test

[ { f t t t t f } ]
[ { -1 0 63 64 127 128 } [ ram-addr? ] map ]
unit-test

[ { f 0 63 0 63 f } ]
[ { -1 0 63 64 127 128 } [ addr>ram-addr ] map ]
unit-test

[ { 0 1 0 1 0 1 } ]
[ { -1 0 63 64 127 128 } [ ram-addr-inc ] map ]
unit-test

#! "arrayfactor.chip.node.f18a.memory.ram" test
