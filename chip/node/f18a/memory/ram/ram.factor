! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators
effects.parser eval fry hashtables io kernel
locals macros make math math.bitwise
math.order math.parser math.ranges models namespaces
sequences splitting words words.symbol parser
prettyprint shuffle unicode.normalize vectors
arrayfactor.chip
arrayfactor.chip.models
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a.execution.time
arrayfactor.chip.node.f18a.memory.debug
arrayfactor.chip.smart-integer
arrayfactor.core.utils ;
FROM: sequences => change-nth ;
QUALIFIED-WITH: arrayfactor.chip.node.f18a.memory.debug d
IN: arrayfactor.chip.node.f18a.memory.ram

TUPLE: ram
    { coding vector }
    { code model }
    { binary model }
    { timed-code model }
    { names model }
    { addr-list model }
    { debug debug }
    { notify-connections boolean } ;

CONSTANT: CA9-DISASM V{ 169 "call" }


SYMBOL: Ram

#!
#! GETTING THE CODE
#!

: coding> ( ram -- code )
    coding>> ; inline

: code-model> ( ram -- model )
    code>> ; inline

: code> ( ram -- code )
    code-model> value>> ; inline

: code-trimmed> ( ram -- code )
    code> [ CA9-DISASM 1vector = ] trim-tail ;

: code< ( code ram -- )
    dup code-model> set-model** ; inline

: >code ( ram code -- ram )
    over code< ; inline

: binary-model> ( ram -- model )
    binary>> ; inline

: binary> ( ram -- binary )
    binary-model> value>> ; inline

: binary-trimmed> ( ram -- binary )
    binary> [ CA9 = ] trim-tail ;

: binary< ( binary ram -- )
    dup binary-model> set-model** ; inline

: >binary ( ram binary -- ram )
    over binary< ;

CONSTANT: CA9-DISASM-TIMED V{ f V{ 169 "call" } }

: timed-code-model> ( ram -- model )
    timed-code>> ; inline

: get-timed-code ( ram -- timed-code )
    timed-code-model> value>> ; inline

ALIAS: timed-code> get-timed-code

: get-timed-code* ( ram -- timed-code )
    get-timed-code [ CA9-DISASM-TIMED = ] trim-tail ;

: timed-code< ( timed-code ram -- )
    dup timed-code-model> set-model** ; inline

: >timed-code ( ram timed-code -- ram )
    over timed-code< ; inline

: addr-list-model> ( ram -- model )
    addr-list>> ; inline

: get-addr-list ( ram -- addr-list )
    addr-list-model> value>> ; inline

: addr-list< ( addr-list ram -- )
    dup addr-list-model> set-model** ; inline

: >addr-list ( ram addr-list -- ram )
    over addr-list< ; inline

: names-model> ( ram -- model )
    names>> ; inline

: names> ( ram -- names )
    names-model> value>> ; inline

: clear-names ( ram -- )
    H{ } clone swap dup names-model> set-model** ;

: coding>code ( ram -- )
    [ coding>> clone ] [ code< ] bi ;

#!
#! ADDRESSING
#!

: mask-addr ( addr -- addr' )
    HEX: 3f bitand ; inline

#!
#! ADDRESSING RAM
#!

: ram-addr? ( addr -- ? )
    [ 0 >= ] [ HEX: 7f <= ] bi and ; inline

: addr>ram-addr ( addr -- ram-addr/f )
    dup ram-addr? [ mask-addr ] [ drop f ] if ;

: ram-addr-inc ( ram-addr -- ram-addr' )
    1 + mask-addr ;

#!
#! BINARY TO CODE
#!

: bin2slots ( bin -- array )
  HEX: 15555 bitxor
  {
      [ -13 shift BIN: 11111 bitand ]
      [ -8  shift BIN: 11111 bitand ]
      [ -3  shift BIN: 11111 bitand ]
      [ BIN: 111 bitand 2 shift ]
  } cleave
  4array ;

<PRIVATE

: shorten-branch-seq ( seq -- seq' )
    dup [ 8 < ] filter
    dup length 0 >
    [ first over index 1 + head ] [ drop ] if ;

: branch-with-addr-word? ( opcode -- ? )
    !TODO Check { "if" "-if" "next" }
    { "jmp" "call" "next" "if" "-if" } member? ;

PRIVATE>

:: ram>addr ( p binary-item slot -- addr )
    !TODO Warning for invalid addr in branch instruction.
    slot
    {
        { 0 [ BIN: 000000000111111111 binary-item bitand ] }
        { 1 [ BIN: 000000000011111111 binary-item bitand ] }
        { 2 [ BIN: 000000000000000111 binary-item bitand
              #! Get high 5 bits of addr from p
              BIN: 11111000 p bitand bitxor  ] }
              [ drop f ]
    } case ;

:: (binary2code) ( opcode p binary-item slot -- opcode-text )
    opcode CODES nth
    dup branch-with-addr-word?
    [ p binary-item slot ram>addr swap 2vector ] when ;

:: binary2code ( p binary-item -- code-item )
    binary-item bin2slots shorten-branch-seq
    [ p binary-item rot* (binary2code) ] map-index ;

: disassembly ( binary-seq -- code-seq )
    [ swap first binary2code >vector ] map-index ;

#!
#! CODING-ITEM
#!

:: coding-item ( p ram -- code-item )
    p ram coding> ?nth dup first number?
    [ p swap first binary2code >vector ] when ;

#!
#! CODE TO BINARY
#!

<PRIVATE

: shifting ( elt index -- elt' )
    { 13 8 3 -2 } nth shift ;

: shift-or ( seq -- n )
    [ shifting ] map-index 0 [ bitor ] reduce ;

PRIVATE>

: addr>ram ( addr binary-item slot -- binary-item' )
  { BIN: 111111110000000000
    BIN: 111111111100000000
    BIN: 111111111111111000 } nth
    bitand + ;

: maybe-include-addr ( code-item binary-item -- binary-item' )
    over last vector?
    [ [ [ last first ] [ length 1 - ] bi ] dip
      swap addr>ram ] [ nip ] if ;

: get-opcodes ( code-item -- seq )
    [ dup vector? [ last ] when op> ] map ;

: code2binary ( code-item -- binary-item )
    !TODO Check this word...
 !  dup "Code2Binary-Item" dbg-set
    dup first [ number? ] [ smart-integer? ] bi or
    [ first (smart)integer>number ]
    [ sift
      dup get-opcodes shift-or HEX: 15555 bitxor
      maybe-include-addr ]
    if ;

: clear-binary ( ram -- ) 64 <vector> >binary drop ;

: code>binary ( ram -- )
    dup code> [
!     "Code-Item" dbg-set
drop      
      [ [ integer-name? not ] [ break? not ] bi and ] filter
      code2binary
    ] map-index >vector
    >binary drop ;

#!
#! TIMED-CODE
#!

: code>extra-time ( code -- extra-time ? )
    dup vector? [ last ] when code>execution-time
    10 / 1 - 0 max
    dup 0 > ;

ERROR: code>timed-code-error ;

:: (code>timed-code) ( code-item -- timed-code-item )
    code-item first number?
    [ code-item ]
    [ V{ } clone code-item
      [ dup [ integer-name? ] [ break? ] bi or
        [ 1vector append! ]
        [ [ code>extra-time
          [ [ { f } append! ] times ] [ drop ] if ]
          [ 1vector append! ] bi
        ] if
      ] each
    ] if
    dup empty? [ code>timed-code-error ] when ;

: code-seq>timed-code ( code-seq -- timed-code )
    [ (code>timed-code) ] map >vector ;

: code>timed-code ( ram -- )
    dup code> code-seq>timed-code >timed-code drop ;

: maybe-add-space ( s x -- s' x )
    [ dup length 0 > [ " " append ] when ] dip ;

:: (code>text) ( code-item p -- text-item )
    code-item first number?
    [ code-item first >hex ]
    [ ""
      code-item
      [ dup [ integer-name? ] [ break? ] [ smart-integer? ]
        tri or or
          [ dup smart-integer?
            [ n> number>string append ] [ drop ] if ]
          [ dup vector?
            [ maybe-add-space
              [ first >hex append " " append ] keep
              second append ]
            [ maybe-add-space append ]
            if
          ] if
      ] each
    ] if ;

: code>text ( code-seq -- text-seq )
    [ (code>text) ] map-index ;

: get-timed-code-item ( p ram -- code-item )
    coding-item (code>timed-code) ; inline

: get-timed-code-item-only ( p ram -- code-item )
    get-timed-code-item [ [ integer-name? not ] [ break? not ]
    bi and ] filter ;

#!
#! NAMES AND ADDRESSES
#!

: get-names ( ram -- names )
    names> ; inline

: name>addr ( name ram|rom -- addr ? )
    names> at* ; inline

ERROR: name-not-unique-error ;

: name-in-use? ( addr name ram|rom -- ? )
    name>addr [ = not ] [ 2drop f ] if ; inline

: check-name ( addr name ram|rom -- ? )
   3dup name-in-use? dup
   [ 
!    pick "Not-Unique-Name" dbg-set
     name-not-unique-error ] when
   [ 3drop ] dip ;

: set-name ( addr name ram|rom -- )
    3dup check-name drop
    names> set-at ;
 
: set-names-model ( addr name ram|rom -- )
    dup names-model>
    [ nip value>> [ set-at ] [ clone ] bi ]
    [ set-model** ] 2bi ;

: addr>name ( addr ram -- name|rom ? )
    names> value-at* ;

:: (names>addr-list) ( ram|rom -- addr-list )
    0 :> addr!
    0 63 [a,b]
    [ dup ram|rom addr>name nip
      [ dup addr! ] [ drop addr ] if
    ] map ;

: names>addr-list ( ram|rom -- )
    [ (names>addr-list) ] keep addr-list< ;

: init-ram ( ram -- )
    { [ names>addr-list ]
      [ coding>code ]
      [ code>binary ]
      [ code>timed-code ]
    } cleave ;

: <ram> ( -- ram )
    64 CA9-DISASM 1vector
    <array> >vector            #! coding
    V{ } clone <model>         #! code
    V{ } clone <model>         #! binary
    V{ } clone <model>         #! timed-code
    H{ } clone <model>         #! names
    { } clone <model>          #! addr-list
    <debug>                    #! debug
    t                          #! notify-connections
    ram boa
    dup init-ram
    dup Ram set
    dup chip-contains-models ;

#!
#! DATA>RAM
#!

: data>code ( data addr ram -- )
   dup code-model>
   [ nip value>> rot* 1vector '[ drop _ ] change-nth ]
   [ notify-connections** ] 2bi ;

: data>binary ( data addr ram -- )
   dup binary-model>
   [ nip value>> rot* '[ drop _ ] change-nth ]
   [ notify-connections** ] 2bi ;

: data>timed-code ( data addr ram -- )
   dup timed-code-model>
   [ nip value>> rot* 1vector '[ drop _ ] change-nth ]
   [ notify-connections** ] 2bi ;

: data>ram ( data addr ram -- )
    [ ?n> ] 2dip
    { [ data>code ]
      [ data>binary ]
      [ data>timed-code ]
    } 3cleave ;

#!
#! BREAK
#!

: ((break)) ( addr seq -- )
    [ d:<break> prefix ] change-nth ;

: (break) ( addr r_m -- )
    [ code> ((break)) ] [ timed-code> ((break)) ] 2bi ;

: break ( addr r_m -- )
    [ (break) ] [ debug>> add-break-point ] 2bi ;

: ((remove-break)) ( addr seq -- )
    [ [ d:break? not ] filter ] change-nth ;

: (remove-break) ( addr r_m -- )
    [ code> ((remove-break)) ]
    [ timed-code> ((remove-break)) ] 2bi ;

: remove-break ( addr r_m -- )
    [ (remove-break) ] [ debug>> remove-break-point ] 2bi ;

#!
#! DISASSEMBLING FROM BINARY
#!

: <hex-ram> ( hex-string-seq -- ram )
    !TODO Use models!
    [ hex> ] map
    64 HEX: 134a9 pad-right
    [ 1vector ] map dup disassembly   #! coding
    dup clone                         #! code   
    rot*                              #! binary
    over code-seq>timed-code          #! timed-code
    H{ } clone                        #! names
    { } clone                         #! addr-list
    <debug>                           #! debug
    t                                 #! notify-connections
    ram boa
    dup Ram set
    dup chip-contains-models ;

:: hex>ram ( ram hex-string-seq -- )
    hex-string-seq [ hex> ] map
    64 HEX: 134a9 pad-right
    dup ram binary<
    [ 1vector ] map dup disassembly
    ram code<
    code-seq>timed-code
    ram timed-code< ;

#! FETCH

: ram@ ( -- type )
    Ram get ;
