! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: kernel grouping math.parser tools.test namespaces
sequences quotations vectors
arrayfactor.chip.node.f18a.memory.rom.basic
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.f18a.memory.rom.basic.tests

[ {
    V{ "26aba" "0ecb0" "270bd" "02a55" "248ba" "209b2" "311aa" "3e81b" }
    V{ "066b0" "32cab" "243b2" "351ba" "3acb0" "15555" "04f69" "10000" }
    V{ "3a9f5" "3a6b0" "33555" "24de3" "2c1ed" "136d3" "2bdba" "00011" }
    V{ "249f2" "2edb0" "24eb0" "1b6de" "3ac78" "249f5" "203e2" "270d8" }
    V{ "249f5" "26a1a" "2fc7c" "3b7a8" "26fbf" "236a1" "09b22" "07b72" }
    V{ "228ad" "115b5" "26aba" "06eb2" "2f6b7" "26a18" "230ac" "2f555" }
    V{ "2bdbb" "00010" "243b2" "351c9" "232b6" "3a6dd" "3a4cd" "134b0" }
    V{ "2246b" "3a6da" "33555" "3a455" "26aba" "07eba" "228b2" "134b0" }
} ]
[ BASIC-ROM [ first >hex 5 zeros-pad-numeric-string ] map 8 group ]
unit-test

#! "arrayfactor.chip.node.f18a.memory.rom.basic" test
