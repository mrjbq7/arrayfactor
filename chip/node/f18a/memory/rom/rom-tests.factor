! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: tools.test namespaces sequences kernel quotations vectors
arrayfactor.chip.node.f18a.memory.rom  ;
IN: arrayfactor.chip.node.f18a.memory.rom.tests

[ { f t t t t f } ]
[ { 127 128 191 192 255 256 } [ rom-addr? ] map ]
unit-test

[ { f 0 63 0 63 f } ]
[ { 127 128 191 192 255 256 } [ addr>rom-addr ] map ]
unit-test

[ { 128 129 128 129 128 129 } ]
[ { 127 128 191 192 255 256 } [ rom-addr-inc ] map ]
unit-test
