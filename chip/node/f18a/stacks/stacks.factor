#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays combinators fry kernel math
math.parser models models.arrow prettyprint sequences vectors
arrayfactor.chip
arrayfactor.chip.models
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils ;
FROM: sequences => change-nth ;
IN: arrayfactor.chip.node.f18a.stacks

TUPLE: stacks < notify-connections-flag
    { stack model }
    { stack-count model }
    { check-stack-count boolean }
    { return-stack model }
    { return-stack-count model }
    { check-return-stack-count boolean }
    { stopped boolean } ;

: stack-model-> ( stacks -- model )
    stack>> ; inline

: stack-> ( stacks -- vector  )
    stack-model-> value>> ; inline

: stack<- ( vector stacks -- )
    dup stack-model-> set-model** ; inline

: ->stack ( stacks vector -- stacks )
    over stack<- ; inline

: stack-count-model-> ( stacks -- model )
    stack-count>> ; inline

: stack-count-> ( stacks -- integer  )
    stack-count-model-> value>> ; inline

: stack-count<- ( integer stacks -- )
    dup stack-count-model-> set-model** ; inline

: ->stack-count ( stacks integer -- stacks )
    over stack-count<- ; inline

: return-stack-model-> ( stacks -- model )
    return-stack>> ; inline

: return-stack-> ( stacks -- vector  )
    return-stack-model-> value>> ; inline

: return-stack<- ( vector stacks -- )
    dup return-stack-model-> set-model** ; inline

: ->return-stack ( stacks vector -- stacks )
    swap [ return-stack<- ] keep ; inline

: return-stack-count-model-> ( stacks -- model )
    return-stack-count>> ; inline

: return-stack-count-> ( stacks -- integer  )
    return-stack-count-model-> value>> ; inline

: return-stack-count<- ( integer stacks -- )
    dup return-stack-count-model-> set-model** ; inline

: ->return-stack-count ( stacks integer -- stacks )
    over return-stack-count<- ; inline

ERROR: stack-underflow-error ;

: stack-count-dec ( stacks -- stacks )
    dup stack-count-> dup 0 >
    [ 1 - ->stack-count ]
    [ drop dup check-stack-count>>
      [ stack-underflow-error ] when ]
    if ;

: stack-count-inc ( stacks -- stacks )
    dup dup stack-count-model-> [ 1 + ] change-model** ;

: change-stack-bases ( base stacks -- )
    dup stack-model-> swap '[ [ _ >>base ] map ] change-model** ;

#! Stops when a return-stack underflow is encountered.
: return-stack-count-dec ( stacks -- stacks )
    dup return-stack-count-> dup 0 >
    [ 1 - ->return-stack-count ]
    [ drop dup check-return-stack-count>>
      [ t over stopped<< ] when ]
    if ;

: return-stack-count-inc ( stacks -- stacks )
    dup dup return-stack-count-model-> [ 1 + ] change-model** ;

: change-return-stack-bases ( base stacks -- )
    dup return-stack-model-> pick '[ [ _ >>base ] map ] change-model** drop ;

#!
#! Construction
#!

: init-a-stack ( length -- model )
    SV <smart-integer> <array> >vector <model> ;

: <stacks> ( -- stacks )
    t                             #! notify-connections
    10 init-a-stack               #! stack
    0 <model>                     #! stack-count
    f                             #! check-stack-count
    9 init-a-stack                #! return-stack
    0 <model>                     #! return-stack-count
    t                             #! check-return-stack-count
    f                             #! stopped
    stacks boa
    dup chip-contains-models ;

#!
#! T & S
#!

ERROR: stack-length-error ;

: check-stack-length ( stacks -- )
    stack-> length 10 > [ stack-length-error ] when ; inline

: t> ( stacks -- smart-integer  )
    stack-> last ; inline

: t< ( smart-integer stacks -- )
    [ stack-> clone but-last [ push ] keep ]
    [ dup stack-model-> set-model** ]
    bi ; inline

: >t ( stacks smart-integer -- stacks )
    over t< ; inline

: s> ( stacks -- smart-integer  )
    8 swap stack-> nth ; inline

: replace-second-from-last ( n seq -- seq' )
    dup pop [ but-last [ push ] keep ] dip
    1vector append ; inline

: s< ( smart-integer stacks -- )
    [ stack-> clone replace-second-from-last ]
    [ dup stack-model-> set-model** ]
    bi ; inline

: >s ( stacks smart-integer -- stacks )
    over s< ; inline

#!
#! R
#!

: r> ( stacks -- smart-integer  )
    return-stack-> last ; inline

: r< ( smart-integer stacks -- )
    [ return-stack-> clone but-last [ push ] keep ]
    [ dup return-stack-model-> set-model** ]
    bi ; inline

: >r ( stacks smart-integer -- stacks )
    over r< ; inline

: r-- ( stacks -- )
    dup return-stack-model->
    [ nip value>> last dec-smart-integer ]
    [ notify-connections** ] 2bi ;

#!
#! STACKS
#!

: .ds ( stacks -- )
    stack-> [ n> .. ] each
    "<-Top" . ;

: {ds} ( stacks -- seq )
    stack-> [ n> ] map ;

: .rs ( stacks -- )
    return-stack-> [ n> .. ] each
    "<-Top" . ;

: {rs} ( stacks -- seq )
    return-stack-> [ n> ] map ;

#!
#! STACK MANIPULATIONS
#!

: circulate-stack ( seq -- seq' )
    7 over nth 1vector prepend but-last ; inline

#! Removes t from data-stack.
: ,,drop ( stacks -- )
    [ stack-> clone circulate-stack ]
    [ dup stack-model-> set-model** ]
    [ stack-count-dec drop ]
    tri ; inline

#! Move smart-integer from regular factor stack to data-stack.
: (#) ( smart-integer stacks -- )
    [ stack-> clone rest [ push ] keep ]
    [ dup stack-model-> set-model** ]
    [ stack-count-inc drop ]
    tri ; inline

: ,# ( smart-integer/f stacks -- )
    over [ (#) ] [ 2drop ] if ;

#! Move smart-integer from data-stack to regular factor stack.
: ,#> ( stacks -- smart-integer )
    [ t> ] [ ,,drop ] bi ;

#! Duplicate t and put on data-stack.
: ,,dup ( stacks -- )
    [ t> clone ] [ ,# ] bi ;

#! Duplicate s and put on data-stack.
: ,,over ( stacks -- )
    [ s> clone ] [ ,# ] bi ;

#! Move smart-integer from regular factor stack to return-stack.
#!   but drop top of data-stack first!
: (,push) ( smart-integer stacks -- )
    { [ ,,drop ]
      [ return-stack-> clone rest [ push ] keep ]
      [ dup return-stack-model-> set-model** ]
      [ return-stack-count-inc drop ]
    } cleave ; inline

#! Move t on return-stack.
: ,,push ( stacks -- )
    [ t> ] [ (,push) ] bi ;

#! Removes r from data-stack.
: ,r>drop ( stacks -- )
    [ return-stack-> clone circulate-stack ]
    [ dup return-stack-model-> set-model** ]
    [ return-stack-count-dec drop ]
    tri ; inline

#! Pop x from return-stack.
: (,pop) ( stacks -- r )
    [ r> ] [ ,r>drop ] bi ;

#! Move r on data-stack.
: ,,pop ( stacks -- )
    [ (,pop) ] [ ,# ] bi ;

: change-stack-item-color ( color-name item stacks -- )
   dup stack-model->
   [ nip value>> rot* [ [ clone ] dip >>color-name/f ] curry change-nth ]
   [ notify-connections** ] 2bi ;

: change-return-stack-item-color ( color-name item stacks -- )
   dup return-stack-model->
   [ nip value>> rot* [ [ clone ] dip >>color-name/f ] curry change-nth ]
   [ notify-connections** ] 2bi ;

: change-stack-item-name ( name item stacks -- )
   dup stack-model->
   [ nip value>> rot* [ [ clone ] dip >>name/f ] curry change-nth ]
   [ notify-connections** ] 2bi ;

: change-return-stack-item-name ( name item stacks -- )
   dup return-stack-model->
   [ nip value>> rot* [ [ clone ] dip >>name/f ] curry change-nth ]
   [ notify-connections** ] 2bi ;

! USE: arrayfactor.chip.node.f18a.stacks
