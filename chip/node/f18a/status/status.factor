#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors kernel math models namespaces sequences ;
IN: arrayfactor.chip.node.f18a.status

TUPLE: status
    { blocking model }
    { reading boolean }
    { writing boolean }
    { blocking-port# integer }
    { (not-)blocking-count integer }
    { empty-return-stack-error boolean } ;

SYMBOL: Status

: <status> ( -- status )
    f <model>           #! blocking
    f                   #! reading
    f                   #! writing
    0                   #! blocking-port#
    0                   #! (not-)blocking-count
    f                   #! empty-return-stack-error
    status boa
    dup Status set ;

#!
#! BLOCKING
#!

: blocking-model-> ( status -- model )
    blocking>> ; inline

: blocking<- ( ? status -- )
    blocking-model-> set-model ; inline

: ->blocking ( status ? -- status )
    over blocking<- ; inline

: blocking-> ( status -- ? )
    blocking-model-> value>> ; inline

: blocking-port#-> ( status -- n )
    blocking-port#>> ;

: (not-)blocking-count-> ( status -- n )
    (not-)blocking-count>> ;

: init-(not-)blocking-count ( status -- )
    0 >>(not-)blocking-count drop ;

: inc-(not-)blocking-count ( status -- )
    [ 1 + ] change-(not-)blocking-count drop ;

: block-reading ( addr status -- )
    t ->blocking t >>reading 0 >>(not-)blocking-count blocking-port#<< ;

: block-writing ( addr status -- )
    t ->blocking t >>writing 0 >>(not-)blocking-count blocking-port#<< ;

: unblock ( status -- )
    0 >>blocking-port#
    0 >>(not-)blocking-count
    f [ ->blocking ] [ >>reading ] [ >>writing ] 2tri 3drop ;

: empty-return-stack-errors ( chip -- seq )
    nodes>> sift
    [ swap dup
      [ status>> empty-return-stack-error>> [ drop f ] unless ]
      [ nip ]
      if
    ] map-index sift ;

! USE: arrayfactor.chip.node.f18a.status
