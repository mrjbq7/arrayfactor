! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: tools.test namespaces sequences kernel quotations vectors
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chips
arrayfactor.core.utils ;
IN: arrayfactor.chip.node.tests

<ga144> <chip> drop

[ V{ V{ "dup" "or" "!b" "." } V{ "!b" "@p" "!b" "dup" } } ]
[ 6 {node}
  "dup or !b !b @p !b dup"
  source>code [ this-assembly opcode>memory ] each
  this-assembly end-assembly
  this-ram code-trimmed>
]
unit-test

remove-chip

#! "arrayfactor.chip.node" test
