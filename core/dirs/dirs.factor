#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip io.directories io.pathnames
kernel math math.parser math.ranges sequences ;
IN: arrayfactor.core.dirs

<PRIVATE

: (create-directories) ( n path -- )
    swap nd>node# number>string append-path
    "macros" append-path
    make-directories ;

: create-directories ( path chip -- )
    over make-directories
    type>> [ rows>> ] [ cols>> ] bi * 1 - 0 swap [a,b]
    [ over (create-directories) ] each drop ;

PRIVATE>

: create-chip&directories ( path chip-type -- )
    over file-name swap <<chip>>
    2dup path<<
    create-directories ;

! fdir "projects/testdirs" append-path dup <<chip>> create-directories
! fdir "projects/sha-2/chip" append-path <ga144> create-chip&directories

! USE: arrayfactor.core.dirs

