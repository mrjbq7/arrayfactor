! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.core.preferences io.encodings.utf8 io.files io.pathnames kernel ;
IN: arrayfactor.core.files

: read-source-file ( path -- seq )
    utf8 file-lines ; inline

: read-file ( relative-path -- seq )
    fdir prepend-path read-source-file ;


! USE: arrayfactor.core.files
