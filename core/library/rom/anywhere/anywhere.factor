! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip ;
IN: arrayfactor.core.library.rom.anywhere

: define-macros ( -- )
    "dup +" "double," add-chip-macro
    "over - over +" "ge," add-chip-macro
    "over - over + -" "lt," add-chip-macro
    "over over - +" "le," add-chip-macro
    "over over - + -" "gt," add-chip-macro
    "over over or" "ne," add-chip-macro
    "-" "not" add-chip-macro
    "dup dup xor" "0," add-chip-macro
    "dup dup xor" "false," add-chip-macro
    "dup dup xor -" "true," add-chip-macro
    "end" "repeat" add-chip-macro ;

! 1388 message passing tool
! routing: called with 'a relay'
! : relay ( a -- )
!     pop a! @+ push @+ zif
!     drop ahead [ swap ] then
!     pop over push @p+ a relay
!     !b !b !b begin @+ !b unext
! : done then a push a! ;

#! USE: arrayfactor.core.library.macros.anywhere
