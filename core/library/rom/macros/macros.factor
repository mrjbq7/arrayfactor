! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.chip arrayfactor.core.macros ;
IN: arrayfactor.core.library.rom.macros

: define-rom-macros ( -- )
! 1388 message passing tool
! routing: called with 'a relay'
": relay pop a! @+ push @+ if drop [ swap ] then pop over push @p+ a relay !b !b !b begin @+ !b unext : done then a push a! ;"
"relay,rom" add-chip-macro ;

#! USE: arrayfactor.core.library.rom.macros
