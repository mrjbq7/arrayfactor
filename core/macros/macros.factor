! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations assocs hashtables kernel namespaces ;
IN: arrayfactor.core.macros

TUPLE: macros { lib hashtable } ;

SYMBOL: Macros

: <macros> ( -- macros )
    !TODO make sub-macros possible.
    H{ } clone   #! lib
    macros boa
    dup Macros set ;

#!
#! NAMES AND MACROS
#!

! CONSTANT: MACROS H{ }

: add-macro ( code name macros -- ) lib>> set-at ;

: name>macro ( name macros -- code ? ) lib>> at* ; 
