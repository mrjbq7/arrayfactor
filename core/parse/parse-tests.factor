#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays combinators hashtables kernel math
namespaces prettyprint sequences tools.test words words.symbol
arrayfactor.chip.node
arrayfactor.core.parse ;
IN: arrayfactor.core.parse.tests

0 <node> drop

F: tst1 ( -- ) dup dup or ;

[ "dup dup or" ]
[ tst1 ACC get ]
unit-test

#!  "arrayfactor.core.parse" test
