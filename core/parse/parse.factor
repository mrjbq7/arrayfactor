#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip.node.codes
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.assembly.private combinators
effects.parser kernel lexer math namespaces parser quotations
sequences vectors words words.symbol ;
IN: arrayfactor.core.parse

#! Everything I do with ACC is completely ignored.

SYMBOL: ACC

: maybe-append-space ( str -- str' ) dup length 0 > [ " " append ] when ;

: acc ( str -- ) ACC get maybe-append-space swap append ACC set-global ;

: compile-now ( -- )
    ACC get source>code [ assembly@ opcode>memory ] each
    assembly@ end-assembly ;

#!
#! F:
#!

: parse--step ( accum end -- accum ? )
    (scan-token) {
        { [ dup ";" = ] [ 2drop f ] }
        { [ 2dup eq? ] [ 2drop f ] }
        { [ dup not ] [ drop unexpected-eof t ] }
        { [ dup delimiter? ] [ unexpected t ] }
        [ acc drop t ]
    } cond ;

: (parse--until) ( accum end -- accum )
    [ parse--step ] keep swap [ (parse--until) ] [ drop ] if ;

: parse--until ( end -- vec )
    100 <vector> swap (parse--until) ;

: parse--definition ( delimiter -- quot )
    "" ACC set-global
    parse--until >quotation
    compile-now ;

: [:] ( -- word def effect )
    scan-new-word
    word name>> get-ram assembly@ i>name
    scan-effect \ ; parse--definition swap ;

SYNTAX: F: [:] define-declared ;

#!
#! F[
#!

: parse---step ( accum end -- accum ? )
    (scan-token) {
        { [ dup "]" = ] [ 2drop f ] }
        { [ 2dup eq? ] [ 2drop f ] }
        { [ dup not ] [ drop unexpected-eof t ] }
        { [ dup delimiter? ] [ unexpected t ] }
        [ acc drop t ]
    } cond ;

: (parse---until) ( accum end -- accum )
    [ parse---step ] keep swap [ (parse---until) ] [ drop ] if ;

: parse---until ( end -- vec )
    100 <vector> swap (parse---until) ;

: parse---definition ( delimiter -- quot )
    "" ACC set-global
    parse---until >quotation
    compile-now ;

: ([) ( -- def ) \ ] parse---definition ;

SYNTAX: F[ ([) drop ;

#!
#! f:
#!
 
! : f; ( -- ) ; delimiter
DEFER: f; delimiter

! "f;" define-delimiter

: parse---f ( delimiter -- quot )
    ":" ACC set-global
    parse---until >quotation
    compile-now ;

: (f;) ( -- def )
    \ ] parse---f
    ACC get-global
    " ;" append
    ACC set-global ;

SYNTAX: f: (f;) drop ;

#! alike: C:\factor\core\parser\parser.factor

! USE: arrayfactor.core.parse
