! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators fry formatting
generalizations grouping io kernel math math.parser parser prettyprint
sequences sets splitting tools.test unicode.normalize vectors ;
IN: arrayfactor.core.utils

#!
#! VECTORS
#!

: ?first ( seq -- first/f )
    0 swap ?nth ;

: ?last ( seq -- last/f )
    dup empty? [ drop f ] [ last ] if ;

: 2vector ( x y -- vector )
    2array >vector ;

#!
#! PRINTING
#!

: .. ( n -- )
    pprint bl ;

: ... ( -- )
    "Not implemented yet" . ;

: print-memory ( seq -- )
    8 group [ [ first "%05X" printf bl ] each nl ] each ;

#!
#! PADDING
#!

: pad-string ( string n -- string' )
    32 [ append ] padding ;

: spaces-pad-numeric-string ( string n -- string' )
    [ "    " prepend ] dip tail* ;

: zeros-pad-numeric-string ( string n -- string' )
    [ "00000000" prepend ] dip tail* ;

: pad-right ( seq n x -- seq' )
    [ append ] padding ;

: node#>string ( n -- 00n )
    number>string 3 zeros-pad-numeric-string ;

#!
#! STRINGS
#!

: append-string ( s1 s2 -- s3 )
    2dup [ "" = ] bi@ or [ " " prepend ] unless append ;

: string-prepend?! ( string prefix -- string' )
    over length 0 > [ prepend ] [ drop ] if ;

#!
#! TEST
#!

: |chip ( -- )
    "arrayfactor.chip" test ;

#!
#! rot* & TUCK - in stead of deprecated words
#!

: rot* ( x y z -- y z x )
    3 nrot ; inline

: -rot* ( x y z -- z x y )
    3 -nrot ; inline

: tuck* ( x y -- y x y )
    swap over ; inline
    
#!
#! Binary and hex
#!

<<

: parse-base ( parsed base -- parsed ) scan-base suffix! ;

SYNTAX: BIN: 2 parse-base ;

SYNTAX: HEX: 16 parse-base ;


>>

CONSTANT: SV  HEX: 15555
CONSTANT: CA9 HEX: 134a9


