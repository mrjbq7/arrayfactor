INSTALLING FACTOR
-----------------

Get factor at: http://www.factorcode.org/

PATH TO ARRAYFACTOR
-------------------

The .factor-roots file must contain the path to the parent directory of arrayFactor.
See: http://docs.factorcode.org/content/article-add-vocabs-roots.html

Using the .factor-roots file:

In Factor type: home
You will see the directory, in which the .factor-roots file must be created.

For example:
In Windows the arrayfactor directory has the path:
C:/KS/factor/arrayFactor
Put in the following text in .factor-roots: C:/KS/factor

In core/preferences/preferences.factor the field "main-path" is set in this way:
vocab-roots get last "arrayfactor" append-path

This only works correctly if the last line of the .factor-roots file is set as described above. An alternative is to edit the code in preferences.factor.

USING ARRAYFACTOR
-----------------

Start Factor. It is case-sensitive.

Type: USE: arrayfactor

To look at the arrayfactor api type:
&&about
&&about-console

You can run tests with |chip and words like it in arrayfactor/core/utils.

Auto-use is handy (alt-u). In auto-use-mode you do not have to tell Factor which vocabulary to use, unless there are several possibilities.

PARPI
-----

You can run "f parpi-test" now. It takes a while.

To run parpi slowly, stepping through it:
USE: arrayfactor.sessions.parpi

You can change the limit under which prime numbers are counted in:
arrayfactor/projects/parpi/cfs/218.cfs

SAVING FACTOR IMAGE
-------------------

The current image can be saved with "save", but make a backup of the factor.image file first. The original image will be overwritten by the save action.

