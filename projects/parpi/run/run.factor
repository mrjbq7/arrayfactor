USING: kernel
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.smart-integer
arrayfactor.projects.parpi
arrayfactor.ui
arrayfactor.ui.console
arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.nodes
arrayfactor.ui.chip.step ;
IN: arrayfactor.projects.parpi.run

: &result ( limit -- )
    700 swap step-until-active ;

: run-parpi ( limit -- )
    parpi
    hardcoded-ram
    init-ui

    start-stepping
    6 steps
    #! Replace tos with limit in node 0,
    #!   using console words.
    0 {node /drop /#
    
    100 steps
    12345678 &result
    100 steps
    12345678 &result

    nodes-window
    navigation-window ;

100,000 run-parpi

! Change limit in 218.cfs!

! Primes under 1,000 = 0x3e8
! Result: 1229 = 0xa8

! Primes under 10,000 = 0x2710
! Result: 1229 = 0x4cd

! Primes under 100,000 = 0x186a0
! Result: 9592 = 0x2578

! USE: arrayfactor.projects.parpi.run
