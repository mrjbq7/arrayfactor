arrayFactor is a simulator written in Factor, 
that simulates the GA144 chip from GreenArrays, Inc.

The programming language Factor can be found at www.factorcode.org

Information about the GA144 chip and the programming language 
arrayForth can be found at www.greenarraychips.com

This repository has a companion web site, that can be found at www.arrayfactor.org

At the moment I do not have very much documentation to offer you, 
nor are there many examples of how to write a program.

There is only one simple example program for now. 
It can be found in projects/circular-stack. 
I used it in my presentation, that can be found in talks/german-fig-talk.

The other two projects, parpi and sha2, are translations from older programs, 
using a special format called "cfs".

In sessions I keep records of working with the simulator. 
The directory sessions/console/first is an interesting example.

More information will also be available at www.arrayfactor.org

Best regards,

Leon Konings
