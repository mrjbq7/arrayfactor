! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrayfactor.ui.console arrayfactor.ui.console.access
arrayfactor.ui.console.instructions
arrayfactor.ui.console.windows kernel sequences ;
IN: arrayfactor.sessions.console.first

&&chip

0 {node

&&ram
&&stack
&&return-stack

"0 org : add . + ;" &
&&
": double dup add ;" &
&&

{ 4 5 } [ /# ] each
/.s
/swap
/push
/pop


": 2dup over over ;" &
&&

": tst 2dup add double ;" & &&

node}

&&editor

! USE: arrayfactor.sessions.console.first

