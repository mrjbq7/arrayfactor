USING: accessors generalizations kernel locals math namespaces sequences
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.nodes
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.library.macros.basic
arrayfactor.projects.parpi
arrayfactor.ui
arrayfactor.ui.chip.nodes
arrayfactor.ui.chip.nodes.navigation
arrayfactor.ui.chip.step ;
IN: arrayfactor.sessions.parpi-duo

: tos-equal? ( node1 node2 -- ? )
    stacks>> [ t> n> ] bi@ = ; inline

: duo-step ( -- )
    0 >current-chip inc-step this-chip timed-executes
    1 >current-chip inc-step this-chip timed-executes ; inline

: stepping? ( limit -- ? )
    this-chip step>> > ; inline

: (step-until-tos-different) ( node1 node2 limit -- node1 node2 limit )
    dup stepping? dup
    [ drop
      duo-step
      pick pick tos-equal?
    ] when
    [ (step-until-tos-different) ] when 
; recursive

:: step-until-t-different ( chip1 chip2 node# limit -- )
!   90,000 [ duo-step ] times
    node# node#>nd
    [ chip1 get-node ] [ chip2 get-node ] bi
    limit
    (step-until-tos-different)
    3drop ;

: parpi-no-limit ( -- chip )
    <ga144> <chip>
    define-macros
    control-node
    generator-node
    generator1-node
    early-sieve-nodes  
    early-sieve1-nodes  
    merge-node
    late-sieve-nodes
    result-node ;

: control-node-limit ( -- )
    "projects/parpi/hex-stuff/control.txt"
    "projects/parpi/cfs/218-limit.cfs"
    "control" 0 "init" { -1 -1 1 100 } compile+assemble-one ;

: parpi-limit ( -- chip )
    <ga144> <chip>
    define-macros
    control-node-limit
    generator-node
    generator1-node
    early-sieve-nodes  
    early-sieve1-nodes  
    merge-node
    late-sieve-nodes
    result-node ;

: run-parpi-duo ( -- )
    no-chips
    parpi-limit
    hardcoded-ram
    init-ui
    start-stepping
    parpi-no-limit
    hardcoded-ram
    init-ui
    start-stepping
!   2drop 84,530 [ duo-step ] times
    0 1,000,000 step-until-t-different
    0 >current-chip 
    nodes-window
    navigation-window
    1 >current-chip 
    nodes-window
    navigation-window ;

ALIAS: sr steps-refresh
ALIAS: ndw nodes-window

: rn ( -- )
    nodes@ refresh-nodes ;

run-parpi-duo

! USE: arrayfactor.sessions.parpi-duo
