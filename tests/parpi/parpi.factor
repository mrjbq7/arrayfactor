#! Copyright (C) 2013 Leon Konings.
#! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs combinators fry grouping kernel locals logging
math math.parser math.ranges namespaces sequences shuffle words words.symbol
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.connection
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.status
arrayfactor.chips
arrayfactor.core.library.macros.basic
arrayfactor.core.macros
arrayfactor.chip.port
arrayfactor.core.utils ;
IN: arrayfactor.tests.parpi

CONSTANT: MERGE-NAMES
H{
    { "0@"         HEX:  1 }
    { "1@"         HEX:  3 }
    { "main"       HEX:  5 }
    { "finish"     HEX: 13 }
    { "init"       HEX: 19 }
}

: ld ( hex-path cfs-path node# -- )
  {node}
  this-compilation get-cfs
  [ swap read-source this-compilation get-cfs translate-source ] [ swap read-hex ] bi ;

: (multiple-connections) ( nd1 nd2 -- seq/f )
    [ nd>rc 2array ] bi@
    2dup [ first ] bi@ =
    [ dup first 100 * '[ second _ + ] bi@ [a,b] [ ] map ]
    [ 2dup [ second ] bi@ =
      [ dup second '[ first 100 * _ + ] bi@ 100 <range> [ ] map ]
      [ 2drop f ]
      if
    ] if ;

ERROR: multiple-connections-error ;

: multiple-connections ( node#1 node#2 -- seq/f )
    [ node#>nd ] bi@
    2dup < [ (multiple-connections) ] [ swap (multiple-connections) reverse ] if
    dup empty? [ multiple-connections-error ] when ;

: set-connected-nodes ( connection seq -- )
    { [ first prvnd ] [ second prvnd1 ] [ third nxtnd ] [ fourth nxtnd1 ] } 2cleave ;

ERROR: start-at-word-error ;

: fix-cnp ( addr compilation -- )
    [ connection>> ,cnp >hex swap ] [ cfs>> binary>> set-nth ] bi ;

: fix-cnn ( addr compilation -- )
    [ connection>> ,cnn >hex swap ] [ cfs>> binary>> set-nth ] bi ;

: fix-early-sieve ( compilation -- )
    43 over fix-cnp
    44 swap fix-cnn ;

: fix-late-sieve ( compilation -- )
    50 over fix-cnp
    51 swap fix-cnn ;

: fix-sieves ( compilation -- )
    dup cfs>> binary>> length 47 =
    [ fix-early-sieve ] [ fix-late-sieve ] if ;

! : replace-ram ( ram node -- )
!    memory>> ram<< ;

! : replace-all-ram ( -- )
!     this-chip nodes>> sift [ [ compilation>> cfs>> binary>> <hex-ram> ] [ replace-ram ] bi ] each ;

: binary-import ( memory cfs -- )
    [ ram-hardcoded>> ] [ binary>> ] bi* hex>ram ;

: hardcoded-ram ( -- )
    this-chip nodes>> sift
    [ [ memory>> ] [ compilation>> cfs>> ] bi binary-import ] each ;

:: (lds)* ( start-at-word hex-path cfs-path 2nodes -- )
    this-node node#>>
    #! "2nodes first" makes sure the last node in ldss is not included.
    hex-path cfs-path 2nodes first ld
    this-compilation connection>> [ swap prvnd ] [ 2nodes second nxtnd ] bi
    this-compilation fix-sieves
!   this-node
!     [ compilation>>
!         [ connection>> [ swap prvnd ] [ 2nodes second nxtnd ] bi ]
!         [ fix-sieves ]
!         [ cfs>> binary>> <hex-ram> ]
!         tri
!     ] [ replace-ram ]
!   bi
    this-compilation dup cfs>> new-source>> " " join compile-source
    this-compilation get-source this-assembly assemble-source
    this-assembly end-assembly
    this-node memory>> this-compilation cfs>> binary-import
    start-at-word this-ram name>addr [ start-at-word-error ] unless this-assembly replace-a9-code ;

: lds ( start-at-word hex-path cfs-path node#1 node#2 -- )
    multiple-connections 2 clump [ 4dup (lds)* drop ] each 3drop ;

: ldss ( start-at-word hex-path cfs-path route -- )
    2 clump [ 4dup [ first ] [ second ] bi lds drop ] each 3drop ;

:: compile+assemble-one ( hex-path cfs-path node-name node# start-at-word seq -- )
    hex-path cfs-path node# ld
    this-node node-name set-node-name
    this-compilation connection>> seq set-connected-nodes
    this-compilation dup cfs>> new-source>> " " join compile-source
    this-compilation get-source this-assembly assemble-source
    this-assembly end-assembly
    this-node memory>> this-compilation cfs>> binary-import
    start-at-word this-ram name>addr [ start-at-word-error ] unless this-assembly replace-a9-code ;

:: compile+assemble-many ( hex-path cfs-path route start-at-word -- )
    start-at-word hex-path cfs-path route ldss ;

: control-node ( -- )
    "tests/parpi/hex-stuff/control.txt"
    "tests/parpi/cfs/218-limit.cfs"
    "control" 0 "init" { -1 -1 1 100 } compile+assemble-one ;

: generator-node ( -- )
    "tests/parpi/hex-stuff/generator.txt"
    "tests/parpi/cfs/220.cfs"
    "generator0" 1 "init" { 0 -1 2 101 } compile+assemble-one ;

: generator1-node ( -- )
    "tests/parpi/hex-stuff/generator_100.txt"
    "tests/parpi/cfs/220a.cfs"
    "generator1" 100 "init" { 0 -1 101 200 } compile+assemble-one ;

: early-sieve-nodes ( -- )
    1 >current-node
    "tests/parpi/hex-stuff/early-sieve.txt"
    "tests/parpi/cfs/222.cfs"
    { 107 7 2 } reverse "init" compile+assemble-many ;

: early-sieve1-nodes ( -- )
    100 >current-node
    "tests/parpi/hex-stuff/early-sieve.txt"
    "tests/parpi/cfs/222.cfs"
    { 107 101 } reverse "init" compile+assemble-many ;

: merge-node ( -- )
    "tests/parpi/hex-stuff/merge.txt"
    "tests/parpi/cfs/228.cfs"
    "merge" 107 "init" { 106 7 108 -1 } compile+assemble-one ;

: late-sieve-nodes ( -- )
    107 >current-node
    "tests/parpi/hex-stuff/late-sieve.txt"
    "tests/parpi/cfs/224.cfs"
    { 700 717 617 600 500 517 417 400 300 317 217 210 110 117 17 8 108 }
    reverse "init" compile+assemble-many ;

: result-node ( -- )
    "tests/parpi/hex-stuff/result.txt"
    "tests/parpi/cfs/226.cfs"
    "result" 700 "init" { 701 -1 -1 -1 } compile+assemble-one ;

: parpi ( -- )
    <ga144> <chip> drop
    define-macros
    control-node
    generator-node
    generator1-node
    early-sieve-nodes  
    early-sieve1-nodes  
    merge-node
    late-sieve-nodes
    result-node ;

: show-blocking ( -- seq )
    this-chip nodes>> [ dup [ dup status>> blocking-> [ opcode-> first ] [ drop f ] if ] when ] map ;

! USE: arrayfactor.tests.parpi
