! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: arrays checksums.sha math tools.test namespaces make
sequences kernel quotations
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.connection
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation.cfs
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.core.library.macros.basic
arrayfactor.projects.parpi
arrayfactor.chip.smart-integer ;
IN: arrayfactor.projects.sha2.tests

: n#  ( n -- ) <smart-integer> this-node # ;
: n#> ( -- n ) this-node #> n> ;

<ga144> <chip> drop

[ "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad" ]
[ "abc" sha-256 checksum-bytes hex-string  ]
unit-test

remove-chip

#! "arrayfactor.projects.sha2" test
