! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays kernel
math.parser models.arrow models.product sequences
ui ui.gadgets ui.gadgets.borders
ui.gadgets.labels ui.gadgets.tracks
arrayfactor.chip
arrayfactor.chip.node
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.core.utils
arrayfactor.ui
arrayfactor.ui.chip.node ;
IN: arrayfactor.ui.chip.node.f18a.instructions

TUPLE: instructions-gadget < track ;

: (the-instructions) ( vectors -- string )
    concat [ visf>string ] map f's>surround " " join ; inline

: (instruction-pane-control) ( vectors -- )
    (the-instructions) normal-string>pane ;

: <instruction-pane-control> ( model -- bordered-control )
    [ (instruction-pane-control) ]
    <normal-pane-control> solid-gray-boundary ;

: <the-instructions> ( node -- the-instructions )
    [ instruction-stack-model-> ] [ opcode-model-> ] bi 2vector <product>
    <instruction-pane-control>
    { 380 20 } >>pref-dim ;

: <instructions-gadget> ( node# -- instructions-gadget node )
    horizontal instructions-gadget new-track
    1 >>fill
    { 10 10 } >>gap
    swap node#>node
    [ <the-instructions> f track-add ] keep ;

: instructions-title ( node -- string )
    "Instructions - Node: " swap
    [ node#>> number>string append " " append ]
    [ get-node-name append ] bi ;

: instructions-window ( node# -- )
    dup is-real-node#? 
    [ <instructions-gadget>
      dup this-chip ui>> instructions-gadget<<
      instructions-title open-window
    ] [ drop ] if ;

! USE: arrayfactor.ui.chip.node.f18a.instructions
