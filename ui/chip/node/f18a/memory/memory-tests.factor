! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors
arrayfactor.chip arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.smart-integer arrayfactor.ui
arrayfactor.ui.chip.node.f18a.memory
arrayfactor.ui.chip.node.f18a.stacks kernel models
quotations.private sequences tools.test ;
IN: arrayfactor.ui.chip.node.f18a.memory-tests 

<ga144> <chip> drop

200 {node}
this-compilation
"0 org 
: -swap - . + - ; 
: *, a push dup a! dup or 17
;"
compile-source
this-compilation get-source
this-assembly assemble-source
this-assembly end-assembly
 
! this-node memory>> ram>>
! V{
!   V{ "-" "." "+" "." }
!   V{ "-" ";" }
!   V{ "a" "push" "dup" "." }
!   V{ "a!" "dup" "or" "@p" }
!   V{ 17 }
! }
! >code drop 

[ V{
    V{ "-" "." "+" "." }
    V{ "-" ";" }
    V{ "a" "push" "dup" "." }
    V{ "a!" "dup" "or" "@p" }
    V{ 17 }
    V{ ";" }
} V{
    "329f2"
    "33555"
    "22892"
    "2ade7"
    "11"
    "15555"
} H{ { "-swap" 0 } { "*," 2 } } ]
[ this-ram
  [ code-trimmed> ]
  [ binary-trimmed> [ >hex ] map ]
  [ names> ]
  tri
]
unit-test

[ "  0\n> 1\n  2\n  3\n  4" ]
[ 1 this-node >p
  this-node p-model>
  0 4 memory-addr-arrow
  children>> first model>> quot>>
  this-node p> swap call
]
unit-test

[ { "- . + ." "- ;" "a push dup ." "a! dup or @p" "11" ";" } ]
[ this-node ram-code-arrow
  first children>> first model>>
  [ model>> value>> ] [ quot>> ] bi call
  "\n" split [ "a9 call" = ] trim-tail
]
unit-test

[ { "329f2" "33555" "22892" "2ade7" "00011" "15555" } ]
[ this-node ram-binary-arrow
  first children>> first model>>
  [ model>> value>> ] [ quot>> ] bi call
  "\n" split [ "134a9" = ] trim-tail
]
unit-test

[ { "-swap" "" "*," } ]
[ this-node ram-names-arrow
  first children>> first model>>
  [ model>> value>> ] [ quot>> ] bi call
  "\n" split [ "" = ] trim-tail
]
unit-test

remove-chip

! "arrayfactor.ui.chip.node.f18a.memory" test
