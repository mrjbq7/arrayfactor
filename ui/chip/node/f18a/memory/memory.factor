! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrays assocs colors
colors.constants combinators fry generalizations
grouping io kernel locals math math.parser math.ranges
models models.arrow namespaces shuffle
sequences sorting.insertion splitting ui ui.gadgets
ui.gadgets.borders ui.gadgets.grids ui.gadgets.grid-lines
ui.gadgets.labels ui.gadgets.packs ui.gadgets.scrollers ui.pens
unicode.normalize vectors
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.core.preferences
arrayfactor.ui ;
IN: arrayfactor.ui.chip.node.f18a.memory

: (memory-addresses>label) ( n p -- string )
    over >hex " " prepend 2 tail*
    [ = [ ">" ] [ " " ] if ] dip append ;

: memory-addresses>string ( p a1 a2 -- label )
    [a,b] swap [ (memory-addresses>label) ] curry map
    "\n" join ;

: memory-code>string ( code -- string )
    code>text "\n" join ;

! : memory-timed-code>string ( memory -- string )
!     get-timed-code "\n" join ;

: memory-binary>string ( binary -- string )
    [ >hex "0000" prepend 5 tail* ] map
    "\n" join ;

: memory-names>string ( names ram -- string )
    nip
    0 63 [a,b] swap [ addr>name [ drop "" ] unless ] curry map
    "\n" join ;

: memory-addr-arrow ( p-model a1 a2 -- label-control )
    [ memory-addresses>string ] 2curry <arrow> 12 width>label-control ;

: memory-code-arrow ( code-model -- label-control )
    [ memory-code>string ] <arrow> 30 width>label-control ;

: memory-binary-arrow ( binary-model -- label-control )
    [ memory-binary>string ] <arrow> 30 width>label-control ;

: memory-names-arrow ( ram names-model -- label-control )
    swap [ memory-names>string ] curry
    <arrow> 30 width>label-control ;

! USE: arrayfactor.ui.chip.node.f18a.memory
