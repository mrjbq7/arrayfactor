! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays colors.constants combinators kernel
math.parser math.ranges models models.arrow namespaces
sequences ui ui.gadgets.borders ui.gadgets.grid-lines
ui.gadgets.grids ui.gadgets.scrollers vectors
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.core.preferences
arrayfactor.core.utils
arrayfactor.chips
arrayfactor.ui
arrayfactor.ui.chip.node.f18a.memory ;
IN: arrayfactor.ui.chip.node.f18a.memory.ram

: ram-addr-arrow ( node -- label-control-vector )
    p-model> 0 HEX: 3f memory-addr-arrow
    dup monospace-font>label 1vector ; inline

: ram-code-arrow ( node -- label-control-vector )
    memory>> ram>> code-model> memory-code-arrow
    dup monospace-font>label 1vector ; inline

: ram-binary-arrow ( node -- label-control-vector )
    memory>> ram>> binary-model> memory-binary-arrow
    dup monospace-font>label 1vector ; inline

: ram-names-arrow ( node -- label-control-vector )
    memory>> ram>> dup names-model> memory-names-arrow
    dup serif-font>label 1vector ; inline

: ram-create-arrows ( nd -- 4array )
    this-chip get-node
    { [ ram-addr-arrow ]
      [ ram-code-arrow ]
      [ ram-binary-arrow ]
      [ ram-names-arrow ]
    } cleave 4array ;

SYMBOL: Ram-Nd
SYMBOL: Ram-Debugging-Window

: ram-title ( -- string )
    "Ram - Node: " Ram-Nd get-global this-chip get-node
    [ node#>> number>string append " " append ]
    [ get-node-name append ] bi ;

MAIN-WINDOW: (ram-window) { { title "Ram" } }
    dup Ram-Debugging-Window set
    { 300 450 } >>pref-dim
    ram-title >>title
    Ram-Nd get-global ram-create-arrows
    flip <grid>
    COLOR: cyan <grid-lines> >>boundary
    { 5 5 } <border>
    <scroller>
    >>gadgets ;

! Work in progress...
MAIN-WINDOW: (ram-window-timed) { { title "Ram" } }
    dup Ram-Debugging-Window set
    ram-title >>title
    Ram-Nd get-global ram-create-arrows
    flip <grid>
    COLOR: cyan <grid-lines> >>boundary
    { 10 10 } <border>
    <scroller>
    { 20 420 } <border>
    >>gadgets ;

: ram-window ( node# -- )
    dup is-real-node#? 
    [ node#>nd Ram-Nd set-global (ram-window) ] [ drop ] if ;
