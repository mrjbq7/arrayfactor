! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays colors.constants combinators kernel 
math.parser math.ranges models models.arrow namespaces
sequences ui ui.gadgets.borders ui.gadgets.grid-lines
ui.gadgets.grids ui.gadgets.scrollers vectors
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.memory.rom
arrayfactor.chips
arrayfactor.core.preferences
arrayfactor.core.utils
arrayfactor.ui
arrayfactor.ui.chip.node.f18a.memory ;
IN: arrayfactor.ui.chip.node.f18a.memory.rom

: rom-addr-arrow ( node -- label-control )
    p-model> HEX: 80 HEX: bf memory-addr-arrow
    dup monospace-font>label 1vector ; inline

: rom-code-arrow ( node -- label-control )
    memory>> rom>> code-model> memory-code-arrow
    dup monospace-font>label 1vector ; inline

: rom-binary-arrow ( node -- label-control )
    memory>> rom>> binary-model> memory-binary-arrow
    dup monospace-font>label 1vector ; inline

: rom-names-arrow ( node -- label-control )
    memory>> rom>> dup names-model> memory-names-arrow
    dup serif-font>label 1vector ; inline

: rom-create-arrows ( nd -- 4array )
    this-chip get-node
    { [ rom-addr-arrow ]
      [ rom-code-arrow ]
      [ rom-binary-arrow ]
      [ rom-names-arrow ]
    } cleave 4array ;

SYMBOL: Rom-Nd
SYMBOL: Rom-Debugging-Window

: rom-title ( -- string )
    "Rom - Node: " Rom-Nd get this-chip get-node
    [ node#>> number>string append " " append ]
    [ get-node-name append ] bi ;

MAIN-WINDOW: (rom-window) { { title "Rom" } }
    dup Rom-Debugging-Window set
    rom-title >>title 
    Rom-Nd get rom-create-arrows
    flip <grid>
    COLOR: cyan <grid-lines> >>boundary
    { 10 10 } <border>
    <scroller>
    { 120 450 } <border>
    >>gadgets ;

: rom-window ( node# -- )
    dup is-real-node#? 
    [ node#>nd Rom-Nd set (rom-window) ] [ drop ] if ;
