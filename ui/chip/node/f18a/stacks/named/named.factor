! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip.node
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.smart-integer arrayfactor.ui.console.private
assocs hashtables kernel math math.parser models models.arrow
namespaces sequences strings vectors ;
IN: arrayfactor.ui.chip.node.f18a.stacks.named

TUPLE: named-collections
    { collection hashtable }
    { collections vector }
    { max integer } ;

: <named-collections> ( -- named-collections )
    H{ } clone      #! collection
    V{ } clone      #! collections
    200             #! max
    named-collections boa ;

<PRIVATE

: keep-collection ( named-collections -- named-collections )
    dup [ collection>> ] [ collections>> ] bi
    [ push ] keep dup length pick max>> >
    [ rest ] when >>collections ;

: add-collection ( named-collections -- named-collections )
    keep-collection
    H{ } clone >>collection ;

: n=/name-exists? ( smart-integer name named-collections -- n=? name? )
    [ n> >hex ] 2dip
    collection>> at*
    [ = ] dip ;

: (add-to-collection) ( smart-integer name/f named-collections -- )
    over [ [ n> >hex ] 2dip collection>> set-at ] [ 3drop ] if ;

PRIVATE>

: add-to-collection ( smart-integer named-collections -- )
    [ dup name/f>> ] dip over
    [ [ >string ] dip 3dup n=/name-exists?
      [ [ [ add-collection ] unless ] [ drop ] if ]
      [ and [ 3drop ] [ (add-to-collection) ] if ] 2bi
    ] [ 3drop ] if ;

: node>stack-model ( node -- model )
    stacks>> stack-model-> ; inline

: (collect-named-smart-integers) ( stack-seq named-collections -- named-collections )
    [ [ last ] dip add-to-collection ]
    [ [ but-last last ] dip [ add-to-collection ] keep ] 2bi ;

: collect-named-smart-integers ( node -- arrow )
    node>stack-model <named-collections>
    [ (collect-named-smart-integers) ] curry <arrow> ;

: &&named ( -- arrow )
    check-for-current-node
    this-node collect-named-smart-integers
    dup activate-model ;

! Usage:
! ... {node
! &&named
! ..... &&steps
! ( arrow ) dup value>>
! unparse write

! USE: arrayfactor.ui.chip.node.f18a.stacks.named
