! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors kernel models
quotations.private sequences tools.test
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.smart-integer
arrayfactor.ui
arrayfactor.ui.chip.node.f18a.stacks ;
IN: arrayfactor.ui.chip.node.f18a.stacks-tests 

<ga144> <chip> drop

[ "a" t t ]
[ 0 {node}
  this-node stacks>>
  { 1 2 3 4 5 6 7 8 9 10 } [ <smart-integer> ] map
  [ >stack ] keep swap
  stack-model-> this-node stack-vectors
  last last children>> first
  model>> quot>>
  [ call ]
  [ uncompose
    [ this-node = ] [ [ 9 rot* nth swap smart-integer&node>string ] = ]
    bi* ]
  bi
]
unit-test

remove-chip

! "arrayfactor.ui.chip.node.f18a.stacks" test
