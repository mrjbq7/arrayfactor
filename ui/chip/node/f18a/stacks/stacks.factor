! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrays
colors.constants combinators kernel math.parser models.arrow
models.product sequences ui ui.gadgets ui.gadgets.borders
ui.gadgets.grid-lines ui.gadgets.grids ui.gadgets.panes
ui.gadgets.scrollers ui.gadgets.tracks ui.pens.solid vectors
arrayfactor.chip
arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.smart-integer
arrayfactor.chips
arrayfactor.ui
arrayfactor.ui.chip.node ;
IN: arrayfactor.ui.chip.node.f18a.stacks

TUPLE: stack-gadget < grid ;

: empty-control ( -- label-control )
    "" string>label ; inline

: empty-controls ( -- label-controls )
    empty-control dup clone 2array ;

: (stack-pane-control) ( stack-seq node index -- )
    rot nth swap smart-integer&node>pane ;

: stack-pane-control ( model node index -- bordered-control )
    [ (stack-pane-control) ] 2curry
    <normal-pane-control> solid-gray-boundary ;

: (stack-vectors) ( model node indices -- seq )
    [ [ 2dup ] dip stack-pane-control ] map 2nip ;

#!
#! Stack
#!

: stack-vectors ( model node -- v )
    { [ { 3 2 1 0 } (stack-vectors)
        empty-controls append ]
      [ { 4 5 6 7 8 9 } (stack-vectors) ]
    } 2cleave 2array ;

: node>stack-model ( node -- model )
    stacks>> stack-model-> ; inline

: <stack-gadget> ( node# -- node stack-gadget )
    node#>nd this-chip get-node dup
    [ node>stack-model ] [ stack-vectors ] bi <grid>
    { 2 2 } >>gap { 120 1 } <border> ;

: stack-title ( node -- string )
    "Stack - Node: " swap
    [ node#>> number>string append " " append ]
    [ get-node-name append ] bi ;

: stack-window ( node# -- )
    dup is-real-node#? 
    [ <stack-gadget>
      dup this-chip ui>> stack-gadget<<
      swap stack-title open-window
    ] [ drop ] if ;

#!
#! Return-stack
#!

: return-stack-vectors ( arrow node -- v )
    { [ { 3 2 1 0 } (stack-vectors)
        empty-controls append ]
      [ { 4 5 6 7 8 } (stack-vectors) ]
    } 2cleave 2array ;

: the-return-stack-model ( node -- model )
    stacks>> return-stack-model-> ; inline

: <return-stack-gadget> ( node# -- node return-stack-gadget )
    node#>nd this-chip get-node dup
    [ the-return-stack-model ] [ return-stack-vectors ]
    bi <grid> { 2 2 } >>gap { 120 1 } <border> ;

: return-stack-title ( node -- string )
    "Return-stack - Node: " swap
    [ node#>> number>string append " " append ]
    [ get-node-name append ] bi ;

: return-stack-window ( node# -- )
    dup is-real-node#? 
    [ <return-stack-gadget>
      dup this-chip ui>> return-stack-gadget<<
      swap return-stack-title open-window
    ] [ drop ] if ;

! USE: arrayfactor.ui.chip.node.f18a.stacks
