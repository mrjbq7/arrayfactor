! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.ui ascii io kernel models
namespaces sequences strings ui ui.gadgets ui.gadgets.borders
ui.gadgets.panes ui.gadgets.tracks ;
IN: arrayfactor.ui.chip.node.node-editor.colorforth

TUPLE: cf-gadget < track
    { source string }
    { word-model model } ;

SYMBOL: Cf-gadget

: edit-source ( cf-gadget word -- )
    [ append ] curry
    change-source source>> write ;

: source-pane-control ( cf-gadget model -- pane-control )
    swap [ swap edit-source ] curry <pane-control>
    { 280 140 } >>pref-dim
    { 3 3 } <border> solid-gray-boundary  ;

: gadget&model ( cf-gadget -- cf-gadget model )
    "" [ >>source ] [ <model> ] bi ;

: <cf-gadget> ( -- gadget )
    vertical cf-gadget new-track
    dup Cf-gadget set gadget&model 
    [ >>word-model ] [ source-pane-control f track-add ] 2bi
    { 300 200 } >>pref-dim ;

: add-word ( word -- )
    " " append Cf-gadget get word-model>> set-model ;

: colorforth-window ( -- )
    <cf-gadget>
        { 5 5 } <border>
    "colorForth" open-window ;

! USE: arrayfactor.ui.chip.node.node-editor.colorforth
