! Copyright (C) 2011 John Benediktsson
! See http://factorcode.org/license.txt for BSD license
USING: accessors arrayfactor.chip arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.assembly
arrayfactor.chip.node.f18a.compilation
arrayfactor.chips arrays colors.constants kernel locals
math.parser namespaces sequences smtp splitting ui ui.commands
ui.gadgets ui.gadgets.borders ui.gadgets.buttons
ui.gadgets.editors ui.gadgets.labels ui.gadgets.scrollers
ui.gadgets.tracks ui.gadgets.worlds ui.gestures ui.pens.solid ;
IN: arrayfactor.ui.chip.node.node-editor

TUPLE: node-editor < track the-editor node ;

: <the-editor> ( node-editor -- gadget )
    the-editor>> <scroller> COLOR: gray <solid> >>boundary ;

<PRIVATE

: maybe-close-window ( gadget -- )
    dup parents 3 head [ world? ] find nip
    [ close-window ] [ drop ] if ;

PRIVATE>

: node-label ( node -- label )
    node#>> number>string "node: " prepend <label> ;

: get-node-source ( editor node -- )
    compilation>> get-source " " join
    format-source swap set-editor-string ;

: compile-editor-string ( node-editor -- )
    [ the-editor>> editor-string ] [ node>> compilation>> ]
    bi
    [ swap compile-source ]
    [ get-source ]
    [ assembly>>
      [ assemble-source ]
      [ end-assembly ] bi ]
    tri ;

: com-compile ( node-editor -- )
    compile-editor-string ;

: com-close ( node-editor -- )
    maybe-close-window ;

: com-refresh ( node-editor -- )
    [ the-editor>> ] [ node>> ] bi get-node-source ;

node-editor "toolbar" f {
    { f com-compile }
    { f com-refresh }
    { f com-close }
} define-command-map

SYMBOL: Node-Editor-Track

:: <node-editor> ( node -- gadget )
    vertical node-editor new-track
        dup Node-Editor-Track set
        1 >>fill
        { 10 10 } >>gap

        <multiline-editor>
            dup node get-node-source
            10 >>min-rows
            60 >>min-cols
            >>the-editor
        
        node >>node

        node node-label   f track-add
        dup <the-editor>  1 track-add
        dup <toolbar>     f track-add ;

: editor-window ( node# -- )
    dup is-real-node#? 
    [ node#>nd this-chip get-node <node-editor>
      { 5 5 } <border> { 1 1 } >>fill
      "Node editor" open-window
    ] [ drop ] if ;

! USE: arrayfactor.ui.chip.node.node-editor
