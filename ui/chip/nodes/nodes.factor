! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip
arrayfactor.chip.node arrayfactor.chip.node.f18a
arrayfactor.chip.port arrayfactor.chip.smart-integer
arrayfactor.chips arrayfactor.ui.chip.node
arrayfactor.ui.chip.node.node-editor
arrayfactor.ui.chip.node.f18a.instructions
arrayfactor.ui.chip.node.f18a.memory.ram
arrayfactor.ui.chip.node.f18a.memory.rom
arrayfactor.ui.chip.node.f18a.registers
arrayfactor.ui.chip.node.f18a.stacks
arrayfactor.ui.chip.port arrays colors.constants combinators
kernel math math.parser math.ranges namespaces sequences
splitting ui ui.gadgets.borders ui.gadgets.grid-lines
ui.gadgets.grids ui.gadgets.menus ui.gadgets.scrollers
ui.gestures ;
IN: arrayfactor.ui.chip.nodes

#!
#! NODE POPUP-MENU
#!

: gadget-node ( gadget -- node# )
    node#>> ;

: com-instructions ( gadget -- )
    gadget-node instructions-window ;

: com-node ( gadget -- )
    gadget-node node-window ;

: com-ram ( gadget -- )
    gadget-node ram-window ;

: com-registers ( gadget -- )
    gadget-node registers-window ;

: com-rom ( gadget -- )
    gadget-node rom-window ;

: com-stack ( gadget -- )
    gadget-node stack-window ;

: com-return-stack ( gadget -- )
    gadget-node return-stack-window ;

: com-port-a ( gadget -- )
    gadget-node dup is-real-node#? 
    [ node#>nd
      dup this-chip get-node dup
      [ a> n>
        dup port#?
        [ port#>nd-pairs [ [ first ] [ second ] bi port-window ]
          each ]
        [ 2drop beep ] if
      ] [ 2drop ] if
    ] [ drop ] if ;

: com-port-b ( gadget -- )
   gadget-node dup is-real-node#? 
   [ node#>nd
     dup this-chip get-node dup
     [ b> n>
       dup port#?
       [ port#>nd-pairs [ [ first ] [ second ] bi port-window ]
         each ]
       [ 2drop beep ] if
     ] [ 2drop ] if
   ] [ drop ] if ;

: com-editor ( gadget -- )
    gadget-node editor-window ;

: node-label-menu ( node-label-control -- )
    { com-ram com-rom ----
      com-stack com-return-stack ----
      com-instructions com-registers ----
      com-port-a com-port-b ----
      com-node com-editor }
    show-commands-menu ;

node-pane-control H{
    { T{ button-down f f 3 } [ node-label-menu ] }
} set-gestures

#!
#! NODES
#!

: node-grids-array ( nodes node-info -- grids )
    [ swap <node-gadget> ] curry map 1array ;

: node-pane-row ( nodes -- panes )
    [ node-pane ] map ;

: registers-pane-row ( nodes -- panes )
    [ registers-pane ] map ;

: stack-pane-row ( nodes node-info -- panes )
    [ swap stack-pane ] curry map ;

: return-stack-pane-row ( nodes node-info -- panes )
    [ swap return-stack-pane ] curry map ;

: node-panes-array ( nodes node-info -- array )
    { [ drop node-pane-row ]
      [ drop registers-pane-row ]
      [ stack-pane-row ]
      [ return-stack-pane-row ]
    } 2cleave
    4array ;

: (nodes-matrix) ( nodes -- arrays )
    V{ } clone swap
    [ dup 17 + [a,b] [ ] map over push ] each ;

: nodes-matrix ( -- arrays )
    0 700 100 <range> reverse (nodes-matrix) ;

: all-nodes-array ( -- array )
    !TODO Vector with push?
    { } clone
    nodes-matrix { 5 3 } <node-info>
    [ node-panes-array append ] curry each ;

SYMBOL: Nodes-Window

MAIN-WINDOW: nodes-window { { title "Nodes" } }
    dup Nodes-Window set-global
    set-standard-node
    all-nodes-array <grid>
    COLOR: cyan <grid-lines> >>boundary
    { 10 10 } <border>
    <scroller>
    >>gadgets ;

: 10-cols ( nodes -- arrays )
    V{ } clone swap
    [ dup 9 + [a,b] [ ] map over push ] each ;

: 40-nodes-matrix ( -- arrays )
    0 300 100 <range> reverse 10-cols ;

: 40-nodes-array ( -- array )
    !TODO Vector with push?
    { } clone
    40-nodes-matrix { 5 3 } <node-info>
    [ node-panes-array append ] curry each ;

MAIN-WINDOW: 40-nodes-window { { title "40 Nodes" } }
    dup Nodes-Window set-global
    set-standard-node
    40-nodes-array <grid>
    COLOR: cyan <grid-lines> >>boundary
    { 10 10 } <border>
    <scroller>
    >>gadgets ;

: 4-cols ( nodes -- arrays )
    V{ } clone swap
    [ dup 1 + [a,b] [ ] map over push ] each ;

: 4-nodes-matrix ( -- arrays )
    100 200 100 <range> reverse 4-cols ;

: 4-nodes-array ( -- array )
    !TODO Vector with push?
    { } clone
    4-nodes-matrix { 5 3 } <node-info>
    [ node-panes-array append ] curry each ;

MAIN-WINDOW: 4-nodes-window { { title "4 Nodes" } }
    { 300 600 } >>pref-dim
    dup Nodes-Window set-global
    set-standard-node
    4-nodes-array <grid>
    COLOR: cyan <grid-lines> >>boundary
    { 10 10 } <border>
    <scroller>
    >>gadgets ;

! USE: arrayfactor.ui.chip.nodes
