! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip
arrayfactor.chip.chip-type arrayfactor.chip.node
arrayfactor.chip.node.f18a.execution
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.memory
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.port arrayfactor.chip.smart-integer
arrayfactor.chips arrayfactor.core.preferences
arrayfactor.core.utils arrayfactor.ui arrays assocs calendar
colors colors.constants combinators fonts grouping hashtables
io io.styles kernel locals
math math.order math.parser math.ranges models
models.arrow namespaces sequences splitting strings timers ui
ui.gadgets ui.gadgets.borders ui.gadgets.grid-lines
ui.gadgets.grids ui.gadgets.labels ui.gadgets.packs
ui.gadgets.panes ui.gadgets.scrollers ui.pens ui.pens.solid
unicode.normalize vectors ;
IN: arrayfactor.ui.chip.port

TUPLE: port-gadget < grid ;

: from-perspective ( port-integers -- nd )
    [ active-node>> ] map 10000 [ min ] reduce ;

#! read or write depends on perspective of nd
: r/w ( port-integer nd -- ? )
    swap
    [ active-node>> = ]
    [ read-request>> ]
    bi swap [ not ] unless ;

: (values-history>pane) ( port-integer nd -- )
    [ r/w [ "r" ] [ "w" ] if normal-string>pane bl ]
    [ drop smart-integer>> smart-integer>pane nl ]
    2bi ; inline

: node-perspective>pane ( nd -- )
    "Node: " normal-string>pane
    nd>node# number>string normal-string>pane
    nl nl ;

: values-history>pane ( port-integers -- )
    reverse
    dup from-perspective
    dup node-perspective>pane 
    [ (values-history>pane) ] curry each ;

: values-history-pane ( port -- pane )
    values-history-model-> [ values-history>pane ]
    <normal-pane-control> ;

: (port-gadget) ( port -- array )
    values-history-pane
    1array [ 1array ] map ;

: <port-gadget> ( port -- gadget )
    (port-gadget) port-gadget new-grid
    { 300 170 } >>pref-dim ;

: port-title ( port -- string )
    "Port: " swap
    [ nd1>> nd>node# number>string append "-" append ]
    [ nd2>> nd>node# number>string append ] bi ;

: (port-window) ( port -- )
    [ <port-gadget> ] [ port-title ] bi open-window ;

: port-window ( node#1 node#2 -- )
    [ node#>nd ] bi@
    nds>prt this-chip prt>port dup
    [ (port-window) ] [ drop ] if ;

! USE: arrayfactor.ui.chip.port
