! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: calendar help.markup help.syntax math prettyprint
quotations sequences strings ;
IN: arrayfactor.ui.console.access

HELP: /#
{ $values { "n" integer } }
{ $description "Move integer from Factor stack to arrayForth stack." } ;

HELP: .r
{ $description "Display return stack." } ;

HELP: .s
{ $description "Display stack." } ;

HELP: /a
{ $values { "n" integer } }
{ $description "Move integer from Factor stack to a register." } ;

HELP: /b
{ $values { "n" integer } }
{ $description "Move integer from Factor stack to b register." } ;

HELP: /break
{ $values { "addr" integer } }
{ $description "Put a breakpoint at addr." } ;

HELP: /color>stack-item
{ $values { "color-name" string } { "item" integer } }
{ $description "Change color of item on stack.\nAlias: /clr>s" } ;

HELP: /clr>s
{ $values { "color-name" string } { "item" integer } }
{ $description "Change color of item on stack.\nAlias for: /color>stack-item" } ;

HELP: /color>return-stack-item
{ $values { "color-name" string } { "item" integer } }
{ $description "Change color of item on return-stack.\nAlias: /clr>rs" } ;

HELP: /clr>rs
{ $values { "color-name" string } { "item" integer } }
{ $description "Change color of item on return-stack.\nAlias for: /color>return-stack-item" } ;

HELP: /drop
{ $description "Remove first item from the data stack." } ;

HELP: /dup
{ $description "Duplicate item on the data stack." } ;

HELP: /name>stack-item
{ $values { "name" string } { "item" integer } }
{ $description "Change name of item on stack.\nAlias: /nm>s" } ;

HELP: /nm>s
{ $values { "name" string } { "item" integer } }
{ $description "Change name of item on stack.\nAlias for: /name>stack-item" } ;

HELP: /name>return-stack-item
{ $values { "name" string } { "item" integer } }
{ $description "Change name of item on return-stack.\nAlias: /nm>rs" } ;

HELP: /nm>rs
{ $values { "name" string } { "item" integer } }
{ $description "Change name of item on return-stack.\nAlias for: /name>return-stack-item" } ;

HELP: /over
{ $description "Duplicate second item on the data stack." } ;

HELP: /p
{ $values { "n" integer } }
{ $description "Move integer from Factor stack to p register." } ;

HELP: /pop
{ $description "Move item from return stack to data stack." } ;

HELP: /push
{ $description "Move item from data stack to return stack." } ;

HELP: /remove-break
{ $values { "addr" integer } }
{ $description "Remove breakpoint from addr." } ;

HELP: /return-stack
{ $values { "seq" sequence } }
{ $description "Reset the contents of the return-stack to seq.\nThe sequence should consist of 9 integers!" } ;

HELP: /s@
{ $values { "s" integer } }
{ $description "Get the contents of the second item on the stack." } ;

HELP: /stack
{ $values { "seq" sequence } }
{ $description "Reset the contents of the stack to seq.\nThe sequence should consist of 10 integers!" } ;

HELP: /stack->
{ $values { "seq" sequence } }
{ $description "Get the stack-object." } ;

HELP: /stack@
{ $values { "seq" sequence } }
{ $description "Get a sequence containing the integers on the stack." } ;

HELP: /t@
{ $values { "s" integer } }
{ $description "Get the contents of the first (top) item on the stack." } ;

ARTICLE: "arrayfactor.ui.console.access" "Console access"
{ $link "arrayfactor.ui.console" } $nl
"The " { $vocab-link "arrayfactor.ui.console.access" } " gives access to stacks and memory of the current node."
{ $heading "Display stacks" }
{ $subsections .r .s }
{ $heading "Change contents of stacks" }
{ $subsections /# /return-stack /stack }
{ $heading "Get a stack-item" }
{ $subsections /t@ /s@ }
{ $heading "Get a stack" }
{ $subsections /stack-> /stack@ }
{ $heading "Change contents of registers" }
{ $subsections /a /b /p }
{ $heading "Debugging" }
{ $subsections /break /remove-break }
{ $heading "Change attribute of a stack-item" }
"You can change attributes of a stack-item to follow it, when it goes from one node to another. A stack-item is an " { $vocab-link "arrayfactor.chip.smart-integer" } "."
{ $subsections /color>stack-item /clr>s /color>return-stack-item /clr>rs /name>stack-item /nm>s /name>return-stack-item /nm>rs }
{ $heading "Stack manipulations" }
{ $subsections /drop /dup /over }
{ $heading "Move to or from stack and return-stack" }
{ $subsections /pop /push }
;

ABOUT: "arrayfactor.ui.console.access"

! "Related vocabularies: " { $vocab-link "arrayfactor.ui.console.windows" }

#! help-lint-all
