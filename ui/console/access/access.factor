! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors annotations arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.memory.ram
arrayfactor.chip.node.f18a.stacks
arrayfactor.chip.smart-integer arrayfactor.ui.console.private
combinators kernel sequences ;
QUALIFIED-WITH:
arrayfactor.chip.node.f18a.memory.ram r
IN: arrayfactor.ui.console.access

#!
#! REGISTERS AND STACKS
#!

: /# ( n -- )
    check-for-integer
    check-for-current-node
    <smart-integer> this-node stacks>> (#) ;

: /a ( n -- )
    check-for-current-node
    check-for-integer
    <smart-integer> this-node a< ;

: /b ( n -- )
    check-for-current-node
    check-for-integer
    <smart-integer> this-node b< ;

: /p ( n -- )
    check-for-current-node
    !TODO Check for valid addr
    check-for-integer
    this-node p< ;

: /drop ( -- )
    check-for-current-node
    this-node stacks>> ,,drop ;

: /dup ( -- )
    check-for-current-node
    this-node stacks>> ,,dup ;

: /over ( -- )
    check-for-current-node
    this-node stacks>> ,,over ;

: /push ( -- )
    check-for-current-node
    this-node stacks>> ,,push ;

: /pop ( -- )
    check-for-current-node
    this-node stacks>> ,,pop ;

<PRIVATE

ERROR: lenght-of-stack-should-be-10-error ;

: check-stack-sequence ( seq -- seq )
    check-for-current-node
    dup length 10 =
    [ lenght-of-stack-should-be-10-error ] unless ;

PRIVATE>

: /stack ( seq -- )
    check-for-current-node
    check-stack-sequence
    dup [ check-for-integer drop ] each
    [ <smart-integer> ] map
    this-node stacks>> stack<- ;

: /stack-> ( -- seq )
    check-for-current-node
    this-node stacks>> stack-> ;

: /stack@ ( -- seq )
    /stack-> [ n> ] map ;

: /t@ ( -- t )
    /stack-> clone pop n> ;

: /s@ ( -- s )
    /stack-> clone [ pop drop ] keep pop n> ;

: /color>stack-item ( color-name item -- )
    check-for-current-node
    this-node stacks>> change-stack-item-color ;

: /color>return-stack-item ( color-name item -- )
    check-for-current-node
    this-node stacks>> change-return-stack-item-color ;

: /name>stack-item ( name item -- )
    check-for-current-node
    this-node stacks>> change-stack-item-name ;

: /name>return-stack-item ( name item -- )
    check-for-current-node
    this-node stacks>> change-return-stack-item-name ;

ALIAS: /clr>s /color>stack-item
ALIAS: /clr>rs /color>return-stack-item
ALIAS: /nm>s /name>stack-item
ALIAS: /nm>rs /name>return-stack-item

<PRIVATE

ERROR: lenght-of-stack-should-be-9-error ;

: check-return-stack-sequence ( seq -- seq )
    check-for-current-node
    dup length 9 =
    [ lenght-of-stack-should-be-9-error ] unless ;

PRIVATE>

: /return-stack ( seq -- )
    check-for-current-node
    check-return-stack-sequence
    dup [ check-for-integer drop ] each
    [ <smart-integer> ] map
    this-node stacks>> return-stack<- ;

: /.s ( -- )
    check-for-current-node
    this-node stacks>> .ds ;

: /.r ( -- )
    check-for-current-node
    this-node stacks>> .rs ;

<PRIVATE

: (/swap) ( stack -- stack' )
    clone dup
    { [ pop ]
      [ pop swap ]
      [ push ]
      [ push ]
    } cleave ;

PRIVATE>

: /swap ( -- )
    check-for-current-node
    this-node stacks>>
    [ stack-> (/swap) ]
    [ stack<- ]
    bi ;

#!
#! RAM
#!

: /break ( addr -- )
    check-for-current-node
    this-ram r:break ;

: /remove-break ( addr -- )
    check-for-current-node
    this-ram r:remove-break ;
