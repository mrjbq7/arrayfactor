! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: calendar help.markup help.syntax math prettyprint
quotations sequences strings ;
IN: arrayfactor.ui.console

ARTICLE: "arrayfactor.ui.console" "Console"
"---" ;
