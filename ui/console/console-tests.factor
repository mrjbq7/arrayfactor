! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors kernel math.parser sequences tools.test
arrayfactor.chip
arrayfactor.chip.chip-type
arrayfactor.chip.node.f18a.ga144
arrayfactor.chip.node
arrayfactor.chip.node.f18a.compilation
arrayfactor.ui.console ;
IN: arrayfactor.ui.console-tests

<ga144> <chip> drop

[ V{ "0" "org" ":" "-swap" "-" "." "+" "-" ";" ":" "tst" "+" ";" } ]
[ 4 {node}
  0 &# &org " : -swap - . + - ; \n  : tst + ;  " &
  this-compilation source>>
] unit-test 

remove-chip

#! "arrayfactor.ui.console" test
