! Copyright (C) 2013 Leon Konings.
! See http://factorcode.org/license.txt for BSD license.
USING: accessors arrayfactor.chip.node
arrayfactor.chip.node.f18a
arrayfactor.chip.node.f18a.compilation
arrayfactor.ui.console.private math.parser sequences ;
IN: arrayfactor.ui.console.instructions

<PRIVATE

: source-push ( string -- )
    this-compilation source>> push ; inline

: add-word ( string -- )
    check-for-current-node
    source-push ;

PRIVATE>

: &# ( n -- )
    number>string add-word ;

: & ( source-string -- )
    check-for-current-node
    split-source
    add-expanded-to-source ;

: &; ( -- )
    ";" add-word ;

: &ex ( -- )
    "ex" add-word ;

: &unext ( -- )
    "unext" add-word ;

: &next ( -- )
    "next" add-word ;

: &if ( -- )
    "if" add-word ;

: &-if ( -- )
    "-if" add-word ;

: &@p ( -- )
    "@p" add-word ;

: &@+ ( -- )
    "@+" add-word ;

: &@b ( -- )
    "@b" add-word ;

: &@ ( -- )
    "@" add-word ;

: &!p ( -- )
    "!p" add-word ;

: &!+ ( -- )
    "!+" add-word ;

: &!b ( -- )
    "!b" add-word ;

: &! ( -- )
    "!" add-word ;

: &+* ( -- )
    "+*" add-word ;

: &2* ( -- )
    "2*" add-word ;

: &2/ ( -- )
    "2/" add-word ;

: &- ( -- )
    "-" add-word ;

: &+ ( -- )
    "+" add-word ;

: &and ( -- )
    "and" add-word ;

: &or ( -- )
    "or" add-word ;

: &drop ( -- )
    "drop" add-word ;

: &dup ( -- )
    "dup" add-word ;

: &pop ( -- )
    "pop" add-word ;

: &over ( -- )
    "over" add-word ;

: &a ( -- )
    "a" add-word ;

: &. ( -- )
    "." add-word ;

: &push ( -- )
    "push" add-word ;

: &b! ( -- )
    "b!" add-word ;

: &a! ( -- )
    "a!" add-word ;

: &, ( -- )
    "," add-word ;

: &lit ( -- )
    "lit" add-word ;

: &org ( -- )
    "org" add-word ;

: &: ( string -- )
    ":" add-word add-word ;

: &] ( -- )
    "]]" add-word ;

: &[ ( -- )
    "[[" add-word ;

: &then ( -- )
    "then" add-word ;

: &for ( -- )
    "for" add-word ;

: &begin ( -- )
    "begin" add-word ;

: &while ( -- )
    "while" add-word ;

: &-while ( -- )
    "-while" add-word ;

: &end ( -- )
    "end" add-word ;

: &until ( -- )
    "until" add-word ;

: &-until ( -- )
    "-until" add-word ;

: &nm>t ( -- )
    "nm>t" add-word ;

ALIAS: &xor &or
ALIAS: &@a &@
ALIAS: &@+a &@+
ALIAS: &!a &!
ALIAS: &!a+ &!+
ALIAS: &a@ &a
